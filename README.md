# Accounts

A web application for managing accounts and transactions using ASP.NET Core with F# and the Giraffe framework.

This is a personal project that I'm using to learn F# and to track personal spending.

## To Do

- [x] Sharing accounts with other people
- [x] Edit transfers and accounts
- [ ] Add visualisations for transfers