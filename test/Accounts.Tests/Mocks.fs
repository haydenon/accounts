[<AutoOpen>]
module Accounts.Tests.Mocks

open System
open System.Linq
open System.Threading.Tasks

open NSubstitute

open Microsoft.AspNetCore.Http
open Microsoft.Extensions.DependencyInjection;

open Accounts.Data.Models
open Accounts.Repository

open Accounts.Tests.Data
open Microsoft.AspNetCore.Identity
open Microsoft.Extensions.Options
open Microsoft.Extensions.Logging

let contextWithRefData =
    let ctx = Substitute.For<HttpContext>() 
    let periodRepo = Substitute.For<IRepository<Period>>()
    periodRepo.Query.Returns(periods.AsQueryable()) |> ignore
    let serviceBuilder = ServiceCollection()
    serviceBuilder.AddSingleton<IRepository<Period>>(periodRepo) |> ignore
    ctx.RequestServices <- (serviceBuilder.BuildServiceProvider() :> IServiceProvider)
    ctx

let addServiceToContext<'T> (ctx : HttpContext) (service : 'T) (serviceType : Type) =
    let serviceProvider = Substitute.For<IServiceProvider>()
    serviceProvider.GetService(serviceType).Returns(service) |> ignore
    ctx.RequestServices.Returns(serviceProvider) |> ignore

let contextWithService service (serviceType : Type) =
    let ctx = Substitute.For<HttpContext>() 
    addServiceToContext ctx service serviceType 
    ctx 

type FakeUserManager(current : User) =
    inherit UserManager<User>(
        Substitute.For<IUserStore<User>>(),
        Substitute.For<IOptions<IdentityOptions>>(),
        Substitute.For<IPasswordHasher<User>>(),
        Seq.ofList [],
        Seq.ofList [],
        Substitute.For<ILookupNormalizer>(),
        Substitute.For<IdentityErrorDescriber>(),
        Substitute.For<IServiceProvider>(),
        Substitute.For<ILogger<UserManager<User>>>())
    override _this.GetUserId _ = current.Id
    override _this.GetUserAsync _ = Task.FromResult current

let userManager current _users =
    new FakeUserManager(current)

let contextWithUser (user : User) =
    let userMan = userManager user []
    contextWithService userMan typeof<UserManager<User>>

type IMockObject =
    abstract member Arg0 : unit -> unit
    abstract member Arg1 : 'a -> unit
    abstract member Arg2 : 'a * 'b -> unit
    abstract member Arg3 : 'a * 'b * 'c -> unit
    abstract member Arg4 : 'a * 'b * 'c * 'd -> unit

type Mock = {
    Mock   : IMockObject
    mutable Value0 : obj
    mutable Value1 : obj
    mutable Value2 : obj
    mutable Value3 : obj
    mutable Value4 : obj
}

let mockForFuncs () = 
    {
        Mock = Substitute.For<IMockObject>()
        Value0 = null
        Value1 = null
        Value2 = null
        Value3 = null
        Value4 = null
    }


let any<'a> () = Arg.Any<'a>()

let private returnValue<'a> value : 'a =
    if isNull value then Unchecked.defaultof<'a> else unbox value

let fun0<'a> funcs =
    fun () ->
        funcs.Mock.Arg0() |> ignore
        returnValue<'a> funcs.Value0

let fun1<'a> funcs =
    fun a ->
        funcs.Mock.Arg1 a
        returnValue<'a> funcs.Value1

let fun2<'a> funcs =
    fun a b ->
        funcs.Mock.Arg2(a, b)
        returnValue<'a> funcs.Value2

let fun3<'a> funcs =
    fun a b c->
        funcs.Mock.Arg3(a, b, c)
        returnValue<'a> funcs.Value3

let fun4<'a> funcs =
    fun a b c d ->
        funcs.Mock.Arg4(a, b, c, d)
        returnValue<'a> funcs.Value4

let verify0 func =
    func.Mock.Received().Arg0()

let returnFor0 func (object : 'a) =
    func.Value0 <- (box object)

let verify1 func (arg1 : 'a) times =
    func.Mock.Received(times).Arg1 arg1

let verifyAny1 func times =
    func.Mock.Received(times).Arg1 Arg.Any

let returnFor1 func (object : 'a) =
    func.Value1 <- (box object)

let verify2 func (arg1 : 'a) (arg2 : 'b) times =
    func.Mock.Received(times).Arg2(arg1, arg2)

let verifyAny2 func times =
    func.Mock.Received(times).Arg2(Arg.Any, Arg.Any)

let returnFor2 func (object : 'a) =
    func.Value2 <- (box object)

let verify3 func (arg1 : 'a) (arg2 : 'b) (arg3 : 'c) times =
    func.Mock.Received(times).Arg3(arg1, arg2, arg3)

let verifyAny3 func times =
    func.Mock.Received(times).Arg3(Arg.Any, Arg.Any, Arg.Any)

let returnFor3 func (object : 'a) =
    func.Value3 <- (box object)

let verify4 func (arg1 : 'a) (arg2 : 'b) (arg3 : 'c) (arg4 : 'd) times =
    func.Mock.Received(times).Arg4(arg1, arg2, arg3, arg4)

let verifyAny4 func times =
    func.Mock.Received(times).Arg4(Arg.Any, Arg.Any, Arg.Any, Arg.Any)

let returnFor4<'a> func (object : 'a) =
    func.Value4 <- (box object)