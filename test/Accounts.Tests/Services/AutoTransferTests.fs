namespace Accounts.Tests

open System

open FsCheck
open FsCheck.Xunit

open Accounts.Data.Models
open Accounts.Services.AutoTransfer
open Accounts.Helpers.DateTimeHelpers
open System.Collections.Generic
open Accounts.Services
open Accounts.ViewModels

open Accounts.Tests.Data

module AutoTransferTests =
    let rand = System.Random()

    let [<Literal>] Currency = "NZD"

    let correctAmount (rates : RatesView) target (auto : AutoTransfer) (trans : Transfer) =
            if not(isNull trans.OriginalCurrency) && trans.OriginalAmount.HasValue
            then
                let exchanged = ExchangeService.Exchange(rates)(auto.Currency, target, auto.Amount)
                trans.Amount               = exchanged &&
                trans.OriginalAmount.Value = auto.Amount &&
                trans.OriginalCurrency     = auto.Currency
            else
                trans.Amount = auto.Amount

    [<Property(Arbitrary=[| typeof<AutoTransferGenerator> |])>]
    let ``AutoTransfer service creates transfers`` (autos: AutoTransfer list) (size : NonNegativeInt) =
        let expectedAutos = if autos.Length = 0 then 0 else (int size) % autos.Length

        let autoSet = HashSet<AutoTransfer>()
        let updatedAutos = HashSet<AutoTransfer>()

        let mostRecent = fun start per numPer -> mostRecentDaysForPeriod start (DateTime.Now.AddYears(2)) per numPer

        let toCreate =
            let autos = Seq.take expectedAutos autos |> List.ofSeq
            List.iter (autoSet.Add >> ignore) autos
            autos

        let transfersForAuto =
            List.map (fun auto ->
                let num = rand.Next(1, 10)
                let transfers = [1..num] |> List.map(fun _ -> Transfer())
                (auto, transfers)) toCreate

        createTransfersForAutoTransfers (updatedAutos.Add >> ignore) ignore transfersForAuto

        autoSet.SetEquals updatedAutos

    [<Property(Arbitrary=[| typeof<AutoTransferGenerator> |])>]
    let ``AutoTransfer service creates all transfers`` (autos : AutoTransfer list) (size : NonNegativeInt) =
        let expectedAutos = if autos.Length = 0 then 0 else (int size) % autos.Length

        let transferSet = HashSet<Transfer>()
        let createdTransfers = HashSet<Transfer>()

        let makeTransfers =
            List.iter (createdTransfers.Add >> ignore)

        let mostRecent = fun start per numPer -> mostRecentDaysForPeriod start (DateTime.Now.AddYears(2)) per numPer

        let toCreate = Seq.take expectedAutos autos |> List.ofSeq

        let transfersForAuto =
            List.map (fun auto ->
                let num = rand.Next(1, 10)
                let transfers = [1..num] |> List.map(fun _ -> Transfer())
                List.iter (transferSet.Add >> ignore) transfers
                (auto, transfers)) toCreate

        createTransfersForAutoTransfers ignore makeTransfers transfersForAuto

        transferSet.SetEquals createdTransfers

    [<Property(Arbitrary=[| typeof<AutoTransferGenerator> |])>]
    let ``Maps transfer correctly for auto transfer`` (auto : AutoTransfer) =
        let accountForAuto = fun (a : AutoTransfer)->
            let account = new Account()
            account.Id <- a.AccountId
            account.Currency  <- Currency
            account
        let timeZoneDate = dateForTimeZone defaultTimeZone
        let mostRecent = fun _ _ _ -> [timeZoneDate.AddDays(-1.0)]

        let transfer =
            getTransfersForAutoTransfers mostRecent accountForAuto rates [auto]
            |> List.collect snd
            |> List.head

        correctAmount rates Currency auto transfer &&
        transfer.Description      = auto.Description &&
        transfer.AutoTransfer

    [<Property(Arbitrary=[| typeof<AutoTransferGenerator> |])>]
    let ``Generates transfers for each date`` (autos : AutoTransfer list) =
        let accountForAuto = fun (a : AutoTransfer)->
            let account = new Account()
            account.Id <- a.AccountId
            account.Currency  <- Currency
            account
        let timeZoneDate = dateForTimeZone defaultTimeZone
        let mostRecent = fun _ _ _ -> [timeZoneDate.AddDays(-3.0); timeZoneDate.AddDays(-2.0); timeZoneDate.AddDays(-1.0)]

        let transfers =
            getTransfersForAutoTransfers mostRecent accountForAuto rates autos
            |> List.collect snd

        List.length transfers = (List.length autos) * 3