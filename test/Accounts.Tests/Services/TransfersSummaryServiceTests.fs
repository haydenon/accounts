namespace Accounts.Tests

open System

open FsCheck.Xunit
open Xunit
open FsUnit

open Accounts.Errors
open Accounts.CommonTypes
open Accounts.Models
open Accounts.Flow2
open Accounts.ReferenceData.Data
open Accounts.Data
open Accounts.Services.TransfersSummary

type SummaryModelList = Models.TransfersSummary list
type SummaryList = TransfersSummary list

[<Properties()>]
module TransfersSummaryServiceTests =
  let anyPeriod = Day
  let anyStart = new DateTime()
  let anyEnd = new DateTime()

  let errorResult error : AppResult<SummaryList> = Error error

  let successResult value : AppResult<SummaryList> = Ok value 

  [<Fact>]
  let ``Transfers summary service returns unxpected error when not logged in`` () =
    // Arrange
    let getUserId = fun () -> None
    let fetchSummaries = mockForFuncs()

    // Act
    let result = getSummaries getUserId (fun4 fetchSummaries) anyPeriod anyStart anyEnd

    // Assert
    verifyAny4 fetchSummaries 0
    result |> should equal (errorResult [ UnexpectedError ])

  [<Fact>]
  let ``Transfers summary service returns transfers when logged in`` () =
    // Arrange
    let userId = UserId "user_id"
    let getUserId = fun () -> Some userId
    let fetchSummaries = mockForFuncs()

    let summaries: SummaryModelList = []
    returnFor4 fetchSummaries summaries

    // Act
    let result = getSummaries getUserId (fun4<Models.TransfersSummary list> fetchSummaries) anyPeriod (Some anyStart) (Some anyEnd)

    // Assert
    result |> should equal (successResult [])