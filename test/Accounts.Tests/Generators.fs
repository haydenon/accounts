[<AutoOpen>]
module Accounts.Tests.Generator

open System
open System.Globalization

open FsCheck

open Accounts.ViewModels
open Accounts.Data.Models

open Accounts.Tests.Data

let createAccount name currency =
    let a = Account()
    a.Name <- name
    a.Currency <- currency
    a

let createAutoTransfer amount currency accountId period numPeriods lastTransfer description =
    let a = AutoTransfer()
    a.Amount <- amount
    a.Currency <- currency
    a.AccountId <- accountId
    a.Period <- period
    a.PeriodId <- period.Id
    a.PeriodsPerOccurence <- numPeriods
    a.LastTransfer <- lastTransfer
    a.Description <- description
    a

let createAutoTransferView amount currency accountId period numPeriods date description =
    {
        Id = 0L;
        CreatedAt = DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
        AccountId = accountId;
        Amount = amount;
        Currency = currency;
        Period = period;
        PeriodsPerOccurence = numPeriods;
        NextDate = date;
        Description = description;
        Date = "2000-01-01";
    }

let createTransferView amount curr origCurrency accountId date description : TransferView =
    let origCurrency =
        match origCurrency with
        | None -> None
        | Some c -> if c = curr then None else Some c
    {
        Id = 0L;
        CreatedAt = DateTime.Now.ToString("yyyy-MM-dd_HH:mm:ss")
        AccountId = accountId;
        Amount = if origCurrency.IsNone then amount else 0.0m;
        Currency = curr
        OriginalAmount = if origCurrency.IsNone then Nullable() else Nullable(amount);
        OriginalCurrency =
            match origCurrency with
            | None   -> null
            | Some c -> c;
        Date = date;
        Description = description;
        Recurring = false;
    }

let generateAmount =
    Arb.Default.NonZeroInt >> Arb.toGen >> (Gen.map int) >> (Gen.map decimal)

let generateNumberOfPeriods =
    Arb.Default.PositiveInt >>
    Arb.filter(fun i -> i < PositiveInt 1000) >>
    Arb.toGen >>
    (Gen.map int)

let generateCurrency() =
    Gen.frequency [
        (1, Gen.constant <| baseCurrency);
        (1, gen {
            let! curr = Gen.elements rates.Rates
            return curr.Key
        })
    ]

let generateCurrencyOption() =
    Gen.frequency [
        (2, Gen.constant None);
        (1, Gen.constant <| Some baseCurrency);
        (1, gen {
            let! curr = Gen.elements rates.Rates
            return Some curr.Key
        })
    ]

let generatePeriod() =
    Gen.elements periods

let generatePeriodCase() =
    Gen.elements periodCases

let generateLastTransfer =
    Arb.Default.Nullable<DateTime> >>
    Arb.filter (fun dt ->
        match Option.ofNullable dt with
        | None -> true
        | Some d -> d < DateTime.UtcNow) >>
    Arb.toGen

let generateDateString =
    Arb.Default.DateTime >>
    Arb.toGen >>
    Gen.map (fun d -> d.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture))

let generateAccount =
    createAccount
    <!> (Arb.Default.String() |> Arb.toGen)
    <*> generateCurrency()

let generateAutoTransfer =
    createAutoTransfer
    <!> generateAmount()
    <*> generateCurrency()
    <*> (Arb.Default.Int64() |>  Arb.toGen)
    <*> generatePeriod()
    <*> generateNumberOfPeriods()
    <*> generateLastTransfer()
    <*> (Arb.Default.String() |> Arb.toGen)

let generateAutoTransferView =
    createAutoTransferView
    <!> generateAmount()
    <*> generateCurrency()
    <*> (Arb.Default.Int64() |>  Arb.toGen)
    <*> generatePeriodCase()
    <*> generateNumberOfPeriods()
    <*> generateDateString()
    <*> (Arb.Default.String() |> Arb.toGen)

let generateTransferView =
    createTransferView
    <!> generateAmount()
    <*> generateCurrency()
    <*> generateCurrencyOption()
    <*> (Arb.Default.Int64() |>  Arb.toGen)
    <*> generateDateString()
    <*> (Arb.Default.String() |> Arb.toGen)

type AccountGenerator() =
    static member Account() =
        generateAccount |> Arb.fromGen

type AutoTransferGenerator() =
    static member AutoTransfer() =
        generateAutoTransfer |> Arb.fromGen

type AutoTransferViewGenerator() =
    static member AutoTransferView() =
        generateAutoTransferView |> Arb.fromGen

type TransferViewGenerator() =
    static member TransferView() =
        generateTransferView |> Arb.fromGen