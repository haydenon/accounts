module Accounts.Tests.Data

open System
open Accounts.ViewModels
open Accounts.Data.Models
open Accounts.ReferenceData

let baseCurrency = "NZD"

let rates = {
    Base = "EUR";
    Date = "2018-01-16"
    Rates =
        [   "AUD", 1.5379m;
            "DKK", 7.4492m;
            "GBP", 0.8886m;
            "JPY", 135.4m;
            "KRW", 1302.4m;
            "NZD", 1.6808m;
            "USD", 1.223m; ]
        |> Map.ofSeq 
}

let defaultTimeZone = TimeZoneInfo.GetSystemTimeZones() |> Seq.head

let mutable id = 0L
let period name =
    id <- id + 1L
    Period(Name = name, Description = name, Id = id)

let periods = [period "Day"; period "Week"; period "Month"; period "Year"]
let periodCases = [Data.Period.Day; Data.Period.Week; Data.Period.Month; Data.Period.Year]

let defaultUser id =
    let user = User()
    let id = id.ToString()
    user.Id <- id
    user.Email <- sprintf "%s@test.com" id
    user.DisplayName <- id
    user