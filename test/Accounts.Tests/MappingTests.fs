namespace Accounts.Tests

open System

open FsCheck.Xunit
open NSubstitute
open FsUnit

open Microsoft.AspNetCore.Http
open Accounts.ViewModels
open Accounts.Mapper
open Accounts.Tests.Mocks

[<Properties(Arbitrary=[| typeof<Overrides>; typeof<AutoTransferViewGenerator>; typeof<TransferViewGenerator> |])>]
module MappingTests =
    [<Property>]
    let ``Account view model is the same after mapping to model and back`` (start: AccountView) =
        let withoutViewOnly = { start with Shares = []; Owner = null; Readonly = Nullable(); CreatedAt = "" }
        let user = Data.defaultUser (Guid.NewGuid())
        let ctx = contextWithUser user
        let model = Mapper.Map(ctx, withoutViewOnly)
        model.UserId <- user.Id
        let transfer = new Accounts.Data.Models.Transfer()
        transfer.AccountBalance <- start.Balance
        model.Transfers <- [| transfer |]
        model.Currency <- start.Currency
        let view = { Mapper.Map(ctx, model) with CreatedAt = "" }
        view |> should equal withoutViewOnly

    [<Property>]
    let ``Transfer view model is the same after mapping to model and back`` (start: TransferView) =
        let ctx = Substitute.For<HttpContext>()
        let model = Mapper.Map(ctx, start)
        model.DisplayAmount <- start.Amount
        let view = { Mapper.Map(ctx, model) with CreatedAt = ""; Currency = null }
        let start = { start with CreatedAt = ""; Currency = null; }
        view |> should equal start

    [<Property>]
    let ``AutoTransfer view model is the same after mapping to model and back`` (start : AutoTransferView) =
        let ctx = contextWithRefData
        let model = Mapper.Map(ctx, start)
        let view = Mapper.Map(ctx, model)

        // NextDate is calculated from LastTransfer, which isn't mapped
        let view = { view with NextDate = dateToString(DateTime.MinValue); CreatedAt = "" }
        let start = { start with NextDate = dateToString(DateTime.MinValue); CreatedAt = "" }

        view |> should equal start
