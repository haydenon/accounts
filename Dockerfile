FROM microsoft/dotnet:2.1-aspnetcore-runtime
ENTRYPOINT ["dotnet", "Accounts.dll"]
ARG source=./publish/
WORKDIR /app
ENV ASPNETCORE_URLS http://+:8080
ENV ASPNETCORE_ENVIRONMENT=Production
COPY $source/ .