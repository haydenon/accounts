#!/bin/bash

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

cd "$SCRIPTPATH/"

rm -rf ./release
mkdir ./release

cd "$SCRIPTPATH/src/Accounts/"

./node_modules/.bin/rimraf ./dist/**/*
./node_modules/.bin/webpack --config ./webpack.prod.js
dotnet publish -c Release && cp -a bin/Release/netcoreapp2.0/publish/. ../../publish/

cd $SCRIPTPATH

docker build -t registry.gitlab.com/haydenon/accounts/accounts-web:latest .