module Accounts.Request.Flow

open Giraffe
open System.Threading.Tasks
open System.Net
open Accounts.Errors

type Bypass = 
    | ReturnModel of obj
    | NoContent
    | Failure of Error list * HttpStatusCode

type Result<'T> =
    | Success of 'T
    | Bypass of Bypass

let failure error statusCode =
    Failure ([ error ], statusCode) |> Bypass

let failures errors statusCode =
    Failure (errors, statusCode) |> Bypass

let unexpectedFailure () : Result<'a> =
    Failure ([UnexpectedError], HttpStatusCode.InternalServerError) |> Bypass

let successResult value =
    value
    |> Success
    |> Task.FromResult

let bind func input =
    match input with
    | Success s -> func s
    | Bypass s -> Bypass s

let bindToTask func input =
    match input with
    | Success s -> func s
    | Bypass b -> Task.FromResult <| Bypass b

let bindFromTask (func : ('a -> Result<'b>)) (input : 'a Result Task) : 'b Result Task =
    task {
        let! inp = input
        return
            match inp with
            | Bypass b -> Bypass b
            | Success s -> func s
    }

let bindFromTaskToTask (func : ('a -> Result<'b> Task)) (input : 'a Result Task) : 'b Result Task =
    task {
        let! inp = input
        return!
            match inp with
            | Bypass b -> Task.FromResult <| Bypass b
            | Success s -> func s
    }

let onError (func : 'a Result -> 'a Result ) (input : 'a Result Task) : 'a Result Task =
    task {
        let! inp = input
        return
            match inp with
            | Bypass b ->
                match b with
                | Failure(err, c) -> func (Failure (err, c) |> Bypass)
                | partial -> Bypass partial
            | other -> other
    }

let (>>=) input func =
    bindFromTask func input

let (>>=!) input func =
    bindFromTaskToTask func input

let (!>>!) func =
    bindFromTaskToTask func

let (!>>) func =
    bindFromTask func

let endWith (func : 'a Result -> HttpFuncResult) (input : 'a Result Task) : HttpFuncResult =
    task {
        let! inp = input
        return! func inp
    }

let tryRun (func : 'a -> 'b Result Task) (failure : Error list * HttpStatusCode) input =
    try 
        func input
    with
    | _ -> Failure failure |> Bypass |> Task.FromResult

let teeAsync (func : 'a -> Task<unit>) input : 'a Result Task =
    task {
        do! func input
        return Success input 
    }

let tee (func : 'a -> unit) input : 'a Result =
    func input
    Success input 
 