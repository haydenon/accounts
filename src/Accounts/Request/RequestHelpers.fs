module Accounts.Request.RequestHelpers

open Accounts
open Accounts.Errors
open Mapper
open Flow
open Giraffe
open Microsoft.AspNetCore.Http
open System.Threading.Tasks
open System.Net
open Accounts.Data.Models
open Microsoft.AspNetCore.Identity

let noCache handler = setHttpHeader "Cache-Control" "no-store,no-cache" >=> handler

let returnMessage next ctx result : HttpFuncResult =
    let toJson res = 
        match res with
        | Success model -> noCache (json model) next ctx
        | Bypass bypass ->
            match bypass with
            | ReturnModel object -> noCache (json object) next ctx
            | NoContent -> noCache(setStatusCode 204) next ctx
            | Failure (errors, code) -> noCache(setStatusCode (unbox code) >=> json (toErrorModel errors)) next ctx
    endWith toJson result

let noContent next ctx result : HttpFuncResult =
    let toStatus res =
        match res with
        | Success _ -> noCache(setStatusCode 204) next ctx
        | Bypass bypass ->
            match bypass with
            | ReturnModel object -> noCache (json object) next ctx
            | NoContent -> noCache(setStatusCode 204) next ctx
            | Failure (errors, code) -> noCache(setStatusCode (unbox code) >=> json (toErrorModel errors)) next ctx
    endWith toStatus result

let getModel<'T> (ctx : HttpContext) : 'T Result Task =
    task {
        let! model = ctx.BindJsonAsync<'T>()
        return 
            if obj.ReferenceEquals (model, null) then
                failure BadModel HttpStatusCode.BadRequest
            else
                Success model
    }

let mapModel (ctx : HttpContext) (map : HttpContext * 'a -> 'b) input : 'b Result Task =
    let mapTo model : 'b Result =
        try
            map(ctx, model)
            |> Success
        with
        | err -> failure (MappingError err.Message) HttpStatusCode.BadRequest
    bindFromTask mapTo input

let mapModels (ctx : HttpContext) (map : HttpContext * 'a -> 'b) input : 'b list Result Task =
    let mapTo models : 'b list Result =
        try
            List.map (fun i -> map(ctx, i)) models
            |> Success
        with
        | err -> failure (MappingError err.Message) HttpStatusCode.BadRequest
    bindFromTask mapTo input

let mapViewModel<'a, 'b when 'b :> IEntity> ctx (map : HttpContext *'a -> 'b) eraseId input: 'b Result Task =
    let mapTo model : 'b Result =
        try
            let model = map(ctx, model)
            if (eraseId) then
                model.Id <- 0L
            Success <| model
        with
        | err -> failure (MappingError err.Message) HttpStatusCode.BadRequest
    bindFromTask mapTo input

let mapUserModel<'a, 'b when 'b :> IUserOwned> (ctx : HttpContext) (map : HttpContext * 'a -> 'b) eraseId input: 'b Result Task =
    let mapTo (model : 'b) : 'b Result =
        let userManager = ctx.GetService<UserManager<User>>()
        let userId = userManager.GetUserId(ctx.User)
        model.UserId <- userId
        Success <| model
    mapViewModel ctx map eraseId >> bindFromTask mapTo <| input


let tryGetModel<'T> = (tryRun getModel<'T> ([BadModel], HttpStatusCode.BadRequest))

type Validator<'a> = 'a -> (ValidationError * string) list

let validate (func : Validator<'a>) input = 
    let validateModel validator model : 'a Result =
        let validations = validator model |> List.map ValidationFailure
        match validations with
        | [] -> Success model
        | _::_ -> failures validations HttpStatusCode.BadRequest
    bindFromTask (validateModel func) input

let exitWithNoContent (pred : 'a -> bool) (input : 'a Result Task) : 'a Result Task =
    task {
        let! inp = input
        return
            match inp with
            | Success s ->
                if pred s then
                    Bypass NoContent
                else
                    Success s
            | other -> other
    }

let exitWithMessage (pred : 'a -> bool) (exit : 'a -> 'b Result Task) (input : 'a Result Task) : 'a Result Task =
    let onSuccess success = 
        if pred success then
            task {
                let! result = exit success
                return
                    match result with
                    | Success value ->
                        box value |> ReturnModel |> Bypass
                    | Bypass bypass ->
                        Bypass bypass
            }
        else
            Task.FromResult <| Success success
    task {
        let! inp = input
        return!
            match inp with
            | Success success -> onSuccess success
            | other -> Task.FromResult other
    }