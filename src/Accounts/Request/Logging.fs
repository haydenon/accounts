module Accounts.Request.Logging

open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Logging
open Flow
open Accounts.Errors
open System.Threading.Tasks
open Microsoft.FSharp.Quotations.Patterns
open Microsoft.FSharp.Reflection
open Giraffe

let log (ctx : HttpContext) (success : 'a -> string) input =
    let logMsg model =
        let logger = ctx.GetLogger()
        success model
        |> sprintf "%s; %s" ctx.Request.Path.Value
        |> logger.LogInformation
    bindFromTask (tee logMsg) input

let private logForLevel (logger : ILogger) level : string -> unit =
    match level with
    | LogLevel.Warning -> logger.LogWarning
    | LogLevel.Error   -> logger.LogError
    | _                -> logger.LogInformation


let rec isUnionCase = function
| Lambda (_, expr) | Let (_, _, expr) -> isUnionCase expr
| NewTuple exprs -> 
    let iucs = List.map isUnionCase exprs
    fun value -> List.exists ((|>) value) iucs
| NewUnionCase (uci, _) ->
    let utr = FSharpValue.PreComputeUnionTagReader uci.DeclaringType
    box >> utr >> (=) uci.Tag
| _ -> failwith "Expression is no union case."

let (|ContainsError|_|) (errorType : Quotations.Expr) value =
    if List.exists (fun v -> isUnionCase errorType v) value then Some() else None

let (|FailureWithError|_|) (errorType : Quotations.Expr) value =
    match value with
    | Bypass bypass -> 
        match bypass with
        | Failure (errors, code) ->
            match errors with
            | ContainsError errorType -> Some (errors, code)
            | _ -> None
        | _ -> None
    | _ -> None

let private logOnIssue level = 
    fun (ctx : HttpContext) (errorType : Quotations.Expr) (failure : Error -> string) (input : 'a Result Task) -> 
        let logMsg inp =
            match inp with
            | FailureWithError errorType (errors, s) ->
                let logger = ctx.GetLogger()
                failure (List.filter (fun err -> isUnionCase errorType err) errors).Head 
                |> sprintf "%s; %s" ctx.Request.Path.Value
                |> logForLevel logger level
                Failure (errors, s) |> Bypass
            | other -> other
        onError logMsg input

let logError (ctx : HttpContext) errorType (failure : Error -> string) input =
    logOnIssue LogLevel.Error ctx errorType failure input

let logWarning (ctx : HttpContext) errorType (failure : Error -> string) input =
    logOnIssue LogLevel.Warning ctx errorType failure input