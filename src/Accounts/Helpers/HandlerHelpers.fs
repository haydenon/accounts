// module Accounts.Helpers.HandlerHelpers

// open Giraffe
// open Microsoft.AspNetCore.Http
// open Accounts.Mapper
// open Accounts.Errors

// let fallback : HttpHandler =
//     fun (next : HttpFunc) (ctx : HttpContext) ->
//         let action = 
//             if ctx.Request.Path.Value.StartsWith("/api")
//             then 
//                 (setStatusCode 404 >=> json (toErrorModel [NotFound]))
//             else
//                 htmlFile "dist/index.html"
//         action next ctx

// let notAuthenticated : HttpHandler = setStatusCode 401 >=> json (toErrorModel [NotAuthenticated])
// let mustBeAuthenticated : HttpHandler = requiresAuthentication notAuthenticated
// let internalServerError : HttpHandler = setStatusCode 500 >=> json (toErrorModel [UnexpectedError])

// let authentication handler =
//     (mustBeAuthenticated >=> handler)

// let authenticationWithId handler =
//     (fun (id : int64)-> mustBeAuthenticated >=> (handler id))

// let authenticationWithValue handler =
//     (fun value -> mustBeAuthenticated >=> (handler value))