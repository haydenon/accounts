module Accounts.Helpers.DateTimeHelpers

open System
open Accounts.ReferenceData.Data

let private dayOfMonthGreaterThanEqualTo (from : DateTime) (upTo : DateTime ) =
    upTo.Day = DateTime.DaysInMonth(upTo.Year, upTo.Month) || upTo.Day >= from.Day;

let private monthDifference (from : DateTime) (upTo : DateTime) =
    let extraMonth =
        if dayOfMonthGreaterThanEqualTo from upTo then 1 else 0
    extraMonth + (upTo.Month - from.Month - 1) + ((upTo.Year - from.Year) * 12)

let private yearDifference (from : DateTime) (upTo : DateTime) =
    (monthDifference from upTo) / 12

let private numberOfTimeframes (from : DateTime) (upTo : DateTime) (period : Period) numOfPeriods =
    match period with
    | Day ->
        let days = (upTo - from).Days
        days / numOfPeriods
    | Week ->
        let days = (upTo - from).Days
        days / (7 * numOfPeriods)
    | Month ->
        (monthDifference from upTo) / numOfPeriods
    | Year ->
        (yearDifference from upTo) / numOfPeriods

let addTimeframe (original : DateTime) (period : Period) (numOfPeriods : int) (numOfTimeframes : int) =
    try
        match period with
        | Day   -> original.AddDays(float(numOfTimeframes * numOfPeriods))
        | Week  -> original.AddDays(float(numOfTimeframes * (7 * numOfPeriods)))
        | Month -> original.AddMonths(numOfTimeframes * numOfPeriods)
        | Year  -> original.AddYears(numOfTimeframes * numOfPeriods)
    with 
    | _ ->
        if numOfTimeframes > 1
        then DateTime.MaxValue
        else DateTime.MinValue

let dateForTimeZone timeZone = 
    TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.Utc, timeZone).Date;

let mostRecentDaysForPeriod (from : DateTime) (upTo : DateTime) (period : Period) numOfPeriods =
    if numOfPeriods < 1
    then failwith "Number of periods must be greater than 1"

    if from > upTo
    then
        []
    else
        let numberBetween = numberOfTimeframes from upTo period numOfPeriods
        List.fold (fun acc i -> 
            let num = (numberBetween - (i - 1))
            let date = addTimeframe from period numOfPeriods num
            date::acc
        ) [] [1..numberBetween]