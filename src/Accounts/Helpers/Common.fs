[<AutoOpen>]
module Accounts.Common

open System
open System.Linq.Expressions
open Microsoft.FSharp.Linq.RuntimeHelpers
open System.Threading.Tasks
open FSharp.Control.Tasks.ContextInsensitive
open Microsoft.EntityFrameworkCore
open Giraffe

module Task =
  let map f (tsk : 'a Task) = task {
    let! value = tsk
    return f value
  }
  let value = Task.FromResult
  let result (tsk : 'a Task) = tsk.Result

module Option =
  let fromResult res =
    match res with
    | Ok value -> Some value
    | _        -> None

  let toNullable opt =
    match opt with
    | Some v -> Nullable(v)
    | None -> Nullable()

  let fromNullable (nullable : Nullable<'a>) =
    match nullable.HasValue with
    | true -> Some nullable.Value
    | false -> None

  let possibleNull possiblyNull =
    if isNull possiblyNull then
      None
    else
      Some possiblyNull

  let apply f opt =
    match opt with
    | Some _ -> f opt
    | _ -> opt

module Result =
  let get result =
    match result with
    | Ok value -> value
    | _ -> failwith "Expected result to contain value"

  let isOk = function
    | Ok _ -> true
    | _ -> false

  let fromOption opt ifErr =
    match opt with
    | Some value -> Ok value
    | None -> Error ifErr

  let apply f result =
    match result with
    | Ok _ -> f result
    | _ -> result

  let mapError f result =
    match result with
    | Ok _ -> result
    | Error err -> f err

let (|NotNull|_|) value =
  if obj.ReferenceEquals(value, null) then None
  else Some()

let (|StartsWith|_|) (arg : string) (value : string) =
  if not(isNull value) && value.StartsWith(arg) then Some ()
  else None

[<AutoOpen>]
module Giraffe =
  /// Matches the resource with or without a trailing slash
  let routefslash (path : string) (routeHandler : 'T -> HttpHandler) : HttpHandler =
    let path = if path.EndsWith '/' then path.Substring(0, path.Length - 1) else path
    let pathWithout : PrintfFormat<_,_,_,_, 'T> = new PrintfFormat<_,_,_,_, 'T>(value = path)
    let pathWith : PrintfFormat<_,_,_,_, 'T> = new PrintfFormat<_,_,_,_, 'T>(value = sprintf "%s/" path)
    choose [
      routef pathWithout routeHandler
      routef pathWith routeHandler
    ]

type ActionHelper() =
  static member GetAction<'T>(func: 'T -> unit): Expression<Action<'T>> =
    <@ System.Action<'T>(func) @>
    |> LeafExpressionConverter.QuotationToExpression
    |> unbox<Expression<Action<'T>>>