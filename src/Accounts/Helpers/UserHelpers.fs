module Accounts.Helpers.UserHelpers

open Microsoft.AspNetCore.Http
open Giraffe
open Accounts.Data.Models
open Microsoft.AspNetCore.Identity
open FSharp.Control.Tasks.ContextInsensitive

let userEmail (ctx : HttpContext) = task {
    let userManager = ctx.GetService<UserManager<User>>()
    let! user = userManager.GetUserAsync(ctx.User)
    return user.Email
}