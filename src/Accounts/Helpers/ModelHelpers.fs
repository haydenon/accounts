module Accounts.Helpers.Models

open System.Net
open System.Linq
open System.Threading.Tasks

open FSharp.Control.Tasks.ContextInsensitive

open Microsoft.AspNetCore.Http

open Giraffe

open Accounts.Data.Models
open Accounts.Repository
open Accounts.Errors
open Accounts.Request.Flow

let createModel<'T when 'T :not struct and 'T :null and 'T :> IEntity> (ctx : HttpContext) =
    !>>! (fun (inp : 'T) -> task {
        let repo = ctx.GetService<IRepository<'T>>()
        do repo.Create inp
        let! created = repo.SaveChangesAsync()
        return
            match created with
            | true  -> Success inp
            | false -> failure UnexpectedError HttpStatusCode.InternalServerError
    })

let updateModel<'T when 'T :not struct and 'T :null and 'T :> IEntity> (updatesToOriginal : HttpContext -> 'T -> 'T -> unit) (ctx : HttpContext) =
    !>>! (fun (inp : 'T) -> task {
        let repo = ctx.GetService<IRepository<'T>>()
        let id = inp.Id
        let original = repo.FindById(id).Value
        updatesToOriginal ctx inp original
        do repo.Update original
        let! updated = repo.SaveChangesAsync()
        return
            match updated with
            | true  -> Success original
            | false -> failure UnexpectedError HttpStatusCode.InternalServerError
    })

let deleteRelated<'T when 'T :not struct and 'T :null and 'T :> IEntity> (ctx : HttpContext) (getRelated : HttpContext -> int64 -> IQueryable<'T>) =
    !>>! (fun (id : int64) -> task {
        let related = getRelated ctx id
        let repo = ctx.GetService<IRepository<'T>>()
        do repo.DeleteRange related
        return Success id
    })

let deleteTransfers (ctx : HttpContext) =
  !>>! (fun (id : int64) -> task{
      let repo = ctx.GetService<ITransferRepository>()
      do repo.DeleteAllForAccount id
      return Success id
  })

let delete<'T when 'T :not struct and 'T :null and 'T :> IEntity> (ctx : HttpContext) =
    !>>! (fun (id : int64) -> task {
        let repo = ctx.GetService<IRepository<'T>>()
        let item = repo.FindById(id)
        let task =
            match item with
            | None -> Task.FromResult false
            | Some i ->
                repo.Delete i
                repo.SaveChangesAsync()
        let! deleted = task
        return
            match deleted with
            | true  -> Success id
            | false -> failure UnexpectedError HttpStatusCode.InternalServerError
    })