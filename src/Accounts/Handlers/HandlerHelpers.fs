[<AutoOpen>]
module Accounts.Handlers.Helpers

open System;
open System.Net;
open System.Threading.Tasks;

open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Http

open Giraffe

open Accounts
open Accounts.Flow2
open Accounts.Services
open Accounts.Mapper
open Accounts.Errors

let fallback : HttpHandler =
  fun (next : HttpFunc) (ctx : HttpContext) ->
    let action = 
      if ctx.Request.Path.Value.StartsWith("/api")
      then 
        (setStatusCode 404 >=> json (toErrorModel [NotFound]))
      else
        htmlFile "dist/index.html"
    action next ctx

let notAuthenticated : HttpHandler = setStatusCode 401 >=> json (toErrorModel [NotAuthenticated])
let mustBeAuthenticated : HttpHandler = requiresAuthentication notAuthenticated
let internalServerError : HttpHandler = setStatusCode 500 >=> json (toErrorModel [UnexpectedError])

let authentication handler =
    (mustBeAuthenticated >=> handler)

let authenticationWithId handler =
    (fun (id : int64)-> mustBeAuthenticated >=> (handler id))

let authenticationWithValue handler =
    (fun value -> mustBeAuthenticated >=> (handler value))
  
let defaultStatusForError err =
  match err with
  // Not found
  | NotFound -> HttpStatusCode.NotFound
  // Unauthorized
  | NotAuthenticated -> HttpStatusCode.Unauthorized
  // Bad request
  | ValidationFailure(_) 
  | InvalidToken(_) | DuplicateEmailAddress(_) | InvalidPasswordReset
  | PasswordMustContain | NoUserWithEmail(_) | IncorrectUserOrPassword(_)
  | LockedOut(_) | BadModel | MappingError(_) -> HttpStatusCode.BadRequest
  // Internal error
  | UnexpectedError | _ -> HttpStatusCode.InternalServerError

let defaultApiStatuses result =
  match result with
  | Ok  value -> Success value
  | Error err -> Failure(err, defaultStatusForError(List.head err))

let withDefaultApiStatuses next ctx result =
  returnResult defaultApiStatuses result next ctx;

let getUserId (services : IServiceProvider) =
  let usrMng = services.GetService<UserService>()
  usrMng.CurrentUserId()

let getModel<'T> (ctx : HttpContext) : 'T AppResult Task =
  task {
    let! model = ctx.BindJsonAsync<'T>()
    return
      match model with
      | NotNull -> Ok model 
      | _       -> Error [BadModel]
  }