namespace Accounts.Handlers
    open System
    open System.Threading.Tasks
    open System.Linq

    open Microsoft.AspNetCore.Identity
    open Microsoft.AspNetCore.Http

    open Giraffe

    open Accounts
    open Accounts.Services
    open Accounts.Request.RequestHelpers
    open Accounts.Request.Flow
    open Accounts.Validation.TransferValidation
    open Accounts.Repository
    open Mapper
    open Accounts.Data
    open Accounts.ViewModels

    module TransferHandler =
        let private userReadable (ctx : HttpContext) (queryable : IQueryable<Models.Transfer> ) =
            let userRepo = ctx.GetService<UserManager<Models.User>>()
            let userId = userRepo.GetUserId(ctx.User)
            query {
                for item in queryable do
                where (item.Account.UserId = userId
                    || item.Account.AccountShares.Any(fun accShare -> accShare.SharedUserId = userId))
                select item
            }

        let private withAccountId (id: int64 option) (queryable : IQueryable<Models.Transfer>) =
            match id with
            | None -> queryable
            | Some id -> query {
                    for item in queryable do
                    where (item.AccountId = id)
                    select item
                }

        let private page (skip: int option) (take: int option) map (queryable : IQueryable<Models.Transfer>) =
            let total = queryable.Count()
            let transfers =
                match skip, take with
                | None, None -> queryable
                | Some skip, None -> queryable |> Query.Skip skip
                | None, Some take -> queryable |> Query.Take take
                | Some skip, Some take -> queryable |> Query.Skip skip |> Query.Take take
                |> List.ofSeq
                |> List.map map
            Map.empty<string, obj>
            |> Map.add "total" (box total)
            |> Map.add "resources" (box transfers)

        let transferListQuery (ctx : HttpContext) (id: int64 option) =
            let repo = ctx.GetService<ITransferRepository>()
            repo.Query
            |> userReadable ctx
            |> withAccountId id
            |> Query.OrderByDescending(fun t -> t.Date)
            |> Query.ThenByDescending(fun t -> t.CreatedOn)

        let listTransfersWithPage (ctx : HttpContext) (id: int64 option) (skip: int option) (limit: int option) =
            transferListQuery ctx id
            |> page skip limit (fun t -> Mapper.Map(ctx, t))
            |> Success
            |> Task.FromResult

        let listTransfers (ctx : HttpContext) (id: int64 option) =
            transferListQuery ctx id
            |> List.ofSeq
            |> List.map (fun t -> Mapper.Map(ctx, t))
            |> Success
            |> Task.FromResult

        let getSkipTake (ctx : HttpContext) =
            let skip =
                ctx.TryGetQueryStringValue "skip"
                |> Option.bind (Int32.TryParse >> function (true, i) -> Some i | _ -> None)
            let take =
                ctx.TryGetQueryStringValue "limit"
                |> Option.bind (Int32.TryParse >> function (true, i) -> Some i | _ -> None)
            (skip, take)

        let accountListHandler : int64 -> HttpHandler =
            fun (id: int64) (next : HttpFunc) (ctx : HttpContext) ->
                let (skip, take) = getSkipTake ctx
                match skip, take with
                | None, None -> listTransfers ctx (Some id) |> returnMessage next ctx
                | _ -> listTransfersWithPage ctx (Some id) skip take |> returnMessage next ctx

        let listHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let (skip, take) = getSkipTake ctx
                match skip, take with
                | None, None -> listTransfers ctx None |> returnMessage next ctx
                | _ -> listTransfersWithPage ctx None skip take |> returnMessage next ctx

        let private createTransferAndBalance (ctx : HttpContext) input =
            let saveData (transfer : Models.Transfer) = task {
                let service = ctx.GetService<TransferService>()
                return Success (service.CreateTransfer(transfer))
            }
            bindFromTaskToTask saveData input

        type DeleteTransfer = {
            TransferId           : int64
            AccountBalanceChange : decimal
        }

        let private deleteTransfer (ctx : HttpContext) =
            !>>! (fun (id : int64) -> task {
                let service = ctx.GetService<TransferService>()
                let amount = service.DeleteTransfer(id)
                return
                    match amount with
                    | Some amount  -> Success {
                            TransferId = id
                            AccountBalanceChange = amount
                        }
                    | None -> unexpectedFailure()
            })

        let private updateTransfer (ctx : HttpContext) =
            !>>! (fun trans -> task {
                let service = ctx.GetService<TransferService>()
                let updated = service.UpdateTransfer trans
                return
                    match updated with
                    | Some update ->
                        Success update
                    | None ->
                        unexpectedFailure()
            })

        type TransferUpdate = {
            Transfer : TransferView
            AccountBalanceChange   : Decimal
        }

        let mapTransfer ctx =
            function
            | Success { Transfer = trans; BalanceChange = change } ->
                { Transfer = Mapper.Map(ctx, trans); AccountBalanceChange = change} |> Success
            | Bypass b -> Bypass b
            |> Task.map

        let createHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<TransferView> ctx
                |> validate (validateTransfer ctx)
                |> mapViewModel ctx Mapper.Map true
                |> createTransferAndBalance ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let deleteHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                successResult id
                |> validate (validateDelete ctx)
                |> deleteTransfer ctx
                |> returnMessage next ctx

        let updateHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<TransferView> ctx
                |> validate (validateTransferUpdate id ctx)
                |> mapViewModel ctx Mapper.Map false
                |> updateTransfer ctx
                |> mapTransfer ctx
                |> returnMessage next ctx

        let transferHandler : HttpHandler =
            choose [
                GET >=> choose [
                    routex "(/?)" >=> authentication listHandler
                    routex "/summary(/?)" >=> authentication TransfersSummaryHandler.summariesForAllAccounts
                ]
                POST >=> choose [
                    routex "(/?)" >=> authentication createHandler
                ]
                PUT >=> choose [
                    routefslash "/%d" (authenticationWithId updateHandler)
                ]
                DELETE >=> choose [
                    routefslash "/%d" (authenticationWithId deleteHandler)
                ]
            ]

