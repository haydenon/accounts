namespace Accounts.Handlers
    open System.Threading.Tasks
    open System.Linq
    open System.Net

    open Giraffe
    open FSharp.Control.Tasks.ContextInsensitive
    open Microsoft.AspNetCore.Identity
    open Microsoft.AspNetCore.Http

    open Accounts
    open Accounts.ReferenceData
    open Accounts.Helpers.Models
    open Accounts.Request.RequestHelpers
    open Accounts.Request.Flow
    open Accounts.Validation.AccountValidation
    open Accounts.Repository
    open Accounts.Errors
    open Accounts.Data
    open Accounts.Data.Models
    open Accounts.ViewModels
    open Accounts.Services.Accounts
    open Mapper

    module AccountHandler =
        let private shareHasAccount (ctx : HttpContext) (accountId : int64) =
            let repo = ctx.GetService<IRepository<AccountShare>>()
            query {
                for share in repo.Query do
                where (share.AccountId = accountId)
                select share
            }

        let private transferHasAccount (ctx : HttpContext) (accountId : int64) =
            let repo = ctx.GetService<ITransferRepository>()
            query {
                for trans in repo.Query do
                where (trans.AccountId = accountId)
                select trans
            }

        let private autoTransferHasAccount (ctx : HttpContext) (accountId : int64) =
            let repo = ctx.GetService<IRepository<AutoTransfer>>()
            query {
                for auto in repo.Query do
                where (auto.AccountId = accountId)
                select auto
            }

        let private userReadable (ctx : HttpContext) (accounts : IQueryable<Account> ) =
            let userRepo = ctx.GetService<UserManager<User>>()
            let userId = userRepo.GetUserId(ctx.User)
            query {
                for item in accounts do
                where (item.UserId = userId
                    || item.AccountShares.Any(fun ac -> ac.SharedUserId = userId))
                select item
            }

        let includeMostRecentTransfer (q : IQueryable<Account>) =
            query {
                for acc in q do
                select (acc, acc.Transfers.OrderByDescending(fun t -> t.Date).ThenByDescending(fun t -> t.CreatedOn).Take(1))
            }
            |> Seq.map (fun (acc, t) -> acc.Transfers <- Array.ofSeq t; acc)

        let includeMostRecentTransferForSingle (ctx : HttpContext) =
            bindFromTask (fun (acc : Account) ->
                let repo = ctx.GetService<ITransferRepository>()
                let transfer =
                    query {
                        for t in repo.Query do
                        where (t.AccountId = acc.Id)
                        select t
                    }
                    |> Query.OrderByDescending(fun t -> t.Date)
                    |> Query.ThenByDescending(fun t -> t.CreatedOn) :> IQueryable<Transfer>
                    |> Query.Take 1
                acc.Transfers <- Array.ofSeq transfer
                Success acc
            )

        let listAccounts (ctx : HttpContext) =
            let repo = ctx.GetService<IRepository<Account>>()
            Query.IncludeRelation (fun (a : Models.Account) -> a.AccountShares) repo.Query :> IQueryable<Account>
            |> Query.IncludeRelation (fun (a : Models.Account) -> a.User) :> IQueryable<Account>
            |> Query.OrderBy(fun a -> a.CreatedOn)
            |> userReadable ctx
            |> includeMostRecentTransfer
            |> List.ofSeq
            |> List.map (fun v -> Mapper.Map(ctx, v))
            |> Success
            |> Task.FromResult

        let private listHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                listAccounts ctx
                |> returnMessage next ctx

        let private accountUpdate _ (update : Account) (original : Account) =
            original.Name <- update.Name

        let private accountShares (ctx : HttpContext) accountId userId  =
           let repo = ctx.GetService<IRepository<AccountShare>>()
           List.ofSeq <| query {
               for share in repo.Query do
               where (share.SharedUserId = userId && share.AccountId = accountId)
               select share
           }

        let private shareUpdate _ (update : AccountShare) (original : AccountShare) =
            original.Readonly <- update.Readonly

        let checkEmail (ctx : HttpContext) =
            !>>! (fun (share : AccountShareView) -> task {
                let userManager = ctx.GetService<UserManager<Models.User>>()
                let normalised = userManager.NormalizeKey share.ShareUser
                let exists = not <| (Seq.isEmpty <| query {
                    for user in userManager.Users do
                    where (user.NormalizedEmail = normalised)
                    select user
                })

                return!
                    match exists with
                    | true -> Success share |> Task.FromResult
                    | false -> task {
                        do! Task.Delay(1000)
                        return failure (NoUserWithEmail share.ShareUser) HttpStatusCode.BadRequest
                    }
            })

        let private createHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AccountView> ctx
                |> validate (validateAccount ctx)
                |> mapUserModel ctx Mapper.Map true
                |> createModel ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let deleteHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                successResult id
                |> validate (validateDelete ctx)
                |> deleteRelated<Models.AccountShare> ctx shareHasAccount
                |> deleteTransfers ctx
                |> deleteRelated<Models.AutoTransfer> ctx autoTransferHasAccount
                |> delete<Models.Account> ctx
                |> noContent next ctx

        let updateAccount (ctx : HttpContext) =
            bindFromTaskToTask (fun acc -> task {
                let accountService = ctx.GetService<AccountService>()
                let! updated = accountService.UpdateAccount acc
                return
                    match updated with
                    | Some updates  -> Success updates
                    | None -> failure UnexpectedError HttpStatusCode.InternalServerError
            })

        let updateHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AccountView> ctx
                |> validate (validateAccountUpdate id ctx)
                |> mapUserModel ctx Mapper.Map false
                |> updateAccount ctx
                |> includeMostRecentTransferForSingle ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let createShareHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AccountShareView> ctx
                |> checkEmail ctx
                |> validate (validateAccountShare id ctx)
                |> mapUserModel ctx Mapper.Map true
                |> createModel ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let updateShareHandler (ids: int64 * int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AccountShareView> ctx
                |> validate (validateAccountShareUpdate (fst ids) (snd ids) ctx)
                |> mapUserModel ctx Mapper.Map false
                |> updateModel shareUpdate ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let deleteShareHandler (ids: int64 * int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                successResult (snd ids)
                |> validate (validateAccountShareDelete ctx)
                |> delete<Models.AccountShare> ctx
                |> noContent next ctx

        let accountHandler : HttpHandler =
            choose [
                GET >=> choose [
                    routefslash "/%d/transfer" (authenticationWithValue TransferHandler.accountListHandler)
                    routefslash "/%d/transfer/summary" (authenticationWithValue TransfersSummaryHandler.summariesForAccount)
                    routefslash "/%d/autotransfer" (authenticationWithValue AutoTransferHandler.accountListHandler)
                    routex "(/?)" >=> authentication listHandler
                ]
                POST >=> choose [
                    routex "(/?)" >=> authentication createHandler
                    routefslash "/%d/share" (authenticationWithValue createShareHandler)
                ]
                PUT >=> choose [
                    routefslash "/%d" (authenticationWithValue updateHandler)
                    routefslash "/%d/share/%d" (authenticationWithValue updateShareHandler)
                ]
                DELETE >=> choose [
                    routefslash "/%d" (authenticationWithValue deleteHandler)
                    routefslash "/%d/share/%d" (authenticationWithValue deleteShareHandler)
                ]
            ]