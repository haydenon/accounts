namespace Accounts.Handlers
    open Giraffe
    open FSharp.Control.Tasks.ContextInsensitive
    open Microsoft.Extensions.DependencyInjection
    open Microsoft.AspNetCore.Http
    open Accounts
    open Accounts.Errors
    open Accounts.Services.Rates
    open Request.Flow
    open Request.RequestHelpers
    open System.Net

    module RateHandler =
        let listHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let getRates () = task {
                    let client = ctx.GetService<IRateClient>()
                    let! rates = client.GetRates()
                    return Success rates
                }
                ()
                |> tryRun getRates ([UnexpectedError], HttpStatusCode.BadRequest)
                |> returnMessage next ctx

        let rateHandler : HttpHandler = 
            choose [
                GET >=> choose [
                    route "/" >=> listHandler
                ]
            ]