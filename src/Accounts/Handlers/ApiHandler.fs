namespace Accounts.Handlers
    module ApiHandler =
        open Giraffe
        open AuthHandler
        open SettingsHandler
        open AccountHandler
        open TransferHandler
        open AutoTransferHandler
        open RateHandler

        let apiHandler : HttpHandler = 
            choose [
                subRoute "/auth" authHandler
                subRoute "/settings" settingsHandler
                subRoute "/account" accountHandler
                subRoute "/transfer" transferHandler
                subRoute "/autotransfer" autoTransferHandler
                subRoute "/rate" rateHandler
            ]