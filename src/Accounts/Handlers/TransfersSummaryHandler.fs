module Accounts.Handlers.TransfersSummaryHandler

open System
open System.Net

open Microsoft.AspNetCore.Http

open Giraffe

open Accounts
open Accounts.Errors
open Accounts.ReferenceData
open Accounts.Mapper2
open Accounts.Services.TransfersSummary
open Accounts.Handlers.Helpers

type SummaryDetails = {
  StartDate : DateTime option
  EndDate   : DateTime option
  Period    : Data.Period option
}

let private getSummaryDetails (ctx : HttpContext) =
  let start = ctx.TryGetQueryStringValue "startDate" |> Option.map dateFromString
  let endDate = ctx.TryGetQueryStringValue "endDate"|> Option.map dateFromString
  let period =
    ctx.TryGetQueryStringValue "period"
    |> Option.bind (fromString typeof<Data.Period>)
  {
    StartDate = start
    EndDate = endDate
    Period = period
  }

let summariesForAccount accId : HttpHandler =
  fun next (ctx : HttpContext)  ->
    let summaryService = ctx.GetService<TransfersSummaryService>()
    let { StartDate = start; EndDate = endDate ; Period = period } = getSummaryDetails ctx
    match period with
    | Some p -> 
      summaryService.GetSummariesForAccount (RecordId accId) p start endDate
      |> Result.map (List.map TransfersSummary.mapToView)
      |> withDefaultApiStatuses next ctx
    | None ->
      Flow2.returnError ([ValidationFailure(MustExist, "period")], HttpStatusCode.BadRequest) next ctx

let summariesForAllAccounts : HttpHandler =
  fun next (ctx : HttpContext)  ->
    let summaryService = ctx.GetService<TransfersSummaryService>()
    let { StartDate = start; EndDate = endDate ; Period = period } = getSummaryDetails ctx
    match period with
    | Some p -> 
      summaryService.GetSummariesForAllAccounts p start endDate
      |> Result.map (List.map TransfersSummary.mapToView)
      |> withDefaultApiStatuses next ctx
    | None ->
      Flow2.returnError ([ValidationFailure(MustExist, "period")], HttpStatusCode.BadRequest) next ctx