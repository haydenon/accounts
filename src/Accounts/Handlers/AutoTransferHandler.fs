namespace Accounts.Handlers
    open Accounts.Services.AutoTransfer
    open Giraffe
    open System.Net
    open Microsoft.AspNetCore.Identity
    open Accounts.Request.RequestHelpers
    open Accounts.Request.Flow
    open Giraffe
    open Accounts.Validation.AutoTransferValidation
    open Microsoft.AspNetCore.Http
    open Accounts
    open Accounts.Repository
    open Accounts.Helpers.Models
    open Accounts.Errors
    open Mapper
    open Accounts.Data
    open System.Threading.Tasks
    open System.Linq
    open Accounts.ViewModels
    open Accounts.Data.Models
    module AutoTransferHandler =
        let private userReadable (ctx : HttpContext) (queryable : IQueryable<Models.AutoTransfer> ) =
            let userRepo = ctx.GetService<UserManager<Models.User>>()
            let userId = userRepo.GetUserId(ctx.User)
            query {
                for item in queryable do
                where (item.Account.UserId = userId
                    || item.Account.AccountShares.Any(fun accShare -> accShare.SharedUserId = userId))
                select item
            }

        let private withAccountId (id: int64 option) (queryable : IQueryable<Models.AutoTransfer>) =
            match id with
            | None -> queryable
            | Some id -> query {
                    for item in queryable do
                    where (item.AccountId = id)
                    select item
            }

        let private withPeriod (queryable : IQueryable<Models.AutoTransfer>) =
            Query.IncludeRelation (fun (a : Models.AutoTransfer) -> a.Period) queryable

        let listAutoTransfers (ctx : HttpContext) (id: int64 option) =
            let repo = ctx.GetService<IRepository<Models.AutoTransfer>>()
            repo.Query
            |> withPeriod
            |> userReadable ctx
            |> withAccountId id
            |> Query.OrderBy(fun a -> a.CreatedOn)
            |> List.ofSeq
            |> List.map (fun t -> Mapper.Map(ctx, t))
            |> Success
            |> Task.FromResult

        let accountListHandler : int64 -> HttpHandler =
            fun (id: int64) (next : HttpFunc) (ctx : HttpContext) ->
                listAutoTransfers ctx (Some id)
                |> returnMessage next ctx

        let listHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                listAutoTransfers ctx None
                |> returnMessage next ctx

        let private createAutoTransfer (ctx : HttpContext) =
            !>>! (fun model -> task {
                let repo = ctx.GetService<IRepository<Models.AutoTransfer>>()
                let service = ctx.GetService<AutoTransferService>()
                do repo.Create(model)
                let! created = repo.SaveChangesAsync()

                if created then service.AddToCreateJob model

                return
                    match created with
                    | true -> Success model
                    | false -> failure UnexpectedError HttpStatusCode.InternalServerError
            })

        let private autoTransferUpdate _ (update : AutoTransfer) (original : AutoTransfer) =
            original.Description         <- update.Description
            original.Amount              <- update.Amount
            original.Currency            <- update.Currency
            original.Period              <- update.Period
            original.PeriodsPerOccurence <- update.PeriodsPerOccurence

        let createHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AutoTransferView> ctx
                |> validate (validateAutoTransfer ctx)
                |> mapViewModel ctx Mapper.Map true
                |> createAutoTransfer ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let deleteHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                successResult id
                |> validate (validateDelete ctx)
                |> delete<Models.AutoTransfer> ctx
                |> noContent next ctx

        let updateHandler (id : int64) : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<AutoTransferView> ctx
                |> validate (validateAutoTransferUpdate id ctx)
                |> mapViewModel ctx Mapper.Map false
                |> updateModel autoTransferUpdate ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx

        let autoTransferHandler : HttpHandler =
            choose [
                GET >=> choose [
                    route "/" >=> (authentication listHandler)
                ]
                POST >=> choose [
                    route "/" >=> (authentication createHandler)
                ]
                PUT >=> choose [
                    routef "/%d/" (authenticationWithId updateHandler)
                ]
                DELETE >=> choose [
                    routef "/%d/" (authenticationWithId deleteHandler)
                ]
            ]

