namespace Accounts.Handlers
    open Microsoft.Extensions.Configuration
    module AuthHandler =
        open System
        open Accounts.Request.Logging
        open Accounts
        open Accounts.Repository
        open Accounts.Errors
        open Accounts.Services
        open ReferenceData.Data
        open Accounts.Data.Models
        open Mapper
        open Request.Flow
        open Request.RequestHelpers
        open Accounts.Validation.AuthValidation
        open Accounts.ViewModels
        open FSharp.Control.Tasks.ContextInsensitive
        open Giraffe
        open Microsoft.AspNetCore.Http
        open Microsoft.AspNetCore.Identity
        open System.Threading.Tasks
        open System.Net

        let private tokens (ctx : HttpContext) token =
            let registerRef = refData<TokenType> ctx Registration
            let queryable = ctx.GetService<IRepository<Token>>().Query
            query {
                for item in queryable do
                where (item.TokenTypeId = registerRef.Id
                    && item.TokenText = token
                    && not(item.Used))
                select item
            }
        
        let private (|StartsWith|_|) (arg : string) (value : string) =
            if value.StartsWith(arg) then Some ()
            else None
        
        let private errorForCreateResult (error : IdentityError) email =
            match error.Code with
            | "DuplicateUserName" -> DuplicateEmailAddress email
            | StartsWith "Password" -> PasswordMustContain
            | _ -> UnexpectedError

        let createUser (ctx : HttpContext) =
            !>>! (fun (model : RegisterView) -> task {
                let  user        =
                    User(UserName = model.Email,
                        Email = model.Email,
                        DisplayName = model.DisplayName,
                        TimeZoneId = model.Timezone)
                let  userManager = ctx.GetService<UserManager<User>>()
                let! result      = userManager.CreateAsync(user, model.Password)
                return
                    match result.Succeeded with
                    | true  -> Success (model, user)
                    | false -> failure (errorForCreateResult (Seq.head result.Errors) model.Email) HttpStatusCode.BadRequest
            })

        let useToken (ctx : HttpContext) =
            !>>! (fun (inputs : RegisterView * User) -> task {
                let useUpToken () = task {
                    let (model, user) = inputs
                    let tokenRepo = ctx.GetService<IRepository<Token>>()
                    let token =
                        tokens ctx model.Token
                        |> Seq.head                    
                    token.Used <- true
                    token.UserId <- user.Id
                    tokenRepo.Update token
                    let! updated = tokenRepo.SaveChangesAsync()
                    return
                        match updated with
                        | true -> Success user
                        | false -> failure UnexpectedError HttpStatusCode.BadRequest
                }

                let config = ctx.GetService<IConfiguration>()
                let registerTokenEnabled = config.GetValue<bool>("RegistrationTokensRequired")
                return!
                    match registerTokenEnabled with
                    | false -> Success (snd inputs) |> Task.FromResult
                    | true  -> useUpToken()
            })

        let private wait (time : int) (input : 'a Result Task) = task {
            let! inp = input
            do! Task.Delay(time)
            return inp
        }
        
        let private checkToken (ctx : HttpContext) input : RegisterView Result Task =
            let config = ctx.GetService<IConfiguration>()
            let registerTokenEnabled = config.GetValue<bool>("RegistrationTokensRequired")
            let waitIfEnabled =
                match registerTokenEnabled with
                | true  -> wait 1000
                | false -> id

            let check (model : RegisterView) =
                let exists =
                    (not registerTokenEnabled) ||
                        tokens ctx model.Token |> Seq.isEmpty |> not
                match exists with
                | true  -> Success model
                | false -> failure (InvalidToken model.Token) HttpStatusCode.BadRequest
            (input >>= check) |> waitIfEnabled

        let private signIn (ctx: HttpContext) =
            !>>! (teeAsync (fun user -> task {
                let signInManager = ctx.GetService<SignInManager<User>>()
                do! signInManager.SignInAsync(user, false)
            }))

        let private getUser (ctx: HttpContext) (_ : 'a Result Task) = task {
            let userManager = ctx.GetService<UserManager<User>>()
            let! user = userManager.GetUserAsync(ctx.User)
            return Success user
        }

        let private getUserFromEmail (ctx: HttpContext) =
            !>>! (fun (model : SignInView) -> task {
                let userManager = ctx.GetService<UserManager<User>>()
                let! user = userManager.FindByEmailAsync(model.Email)
                return Success user
            })
        
        let private exitIfAuthenticated (ctx : HttpContext) input : 'a Result Task =
            let getAndMap _ =
                getUser ctx input
                |> mapModel ctx Mapper.Map
            exitWithMessage (fun _ -> ctx.User.Identity.IsAuthenticated) getAndMap input
        
        let private checkLockout (ctx : HttpContext) =
            !>>! (fun (model : SignInView) -> task {
                let userManager = ctx.GetService<UserManager<User>>()
                let! user = userManager.FindByEmailAsync(model.Email)
                if isNull user
                then
                    return failure (IncorrectUserOrPassword model.Email) HttpStatusCode.BadRequest
                else
                    let! lockedOut = userManager.IsLockedOutAsync(user)
                    return
                        match lockedOut with
                        | true -> failure (LockedOut model.Email) HttpStatusCode.BadRequest
                        | false -> Success model
            })

        let private passwordSignIn (ctx: HttpContext) =
            !>>! (fun (model : SignInView) -> task {
                let signInManager = ctx.GetService<SignInManager<User>>()
                let userManager = ctx.GetService<UserManager<User>>()
                let! result = signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false)
                if result.Succeeded
                then
                    let! user = userManager.FindByEmailAsync(model.Email)
                    let! _ = userManager.ResetAccessFailedCountAsync(user)
                    return Success model
                else 
                    let! user = userManager.FindByEmailAsync(model.Email)
                    let! _ = userManager.AccessFailedAsync(user)
                    return failure (IncorrectUserOrPassword model.Email) HttpStatusCode.BadRequest
            })

        let private registerHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<RegisterView> ctx
                |> exitIfAuthenticated ctx
                |> validate validateRegister
                |> checkToken ctx
                |> (createUser ctx >> useToken ctx)
                |> signIn ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx
            

        let private signInHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<SignInView> ctx
                |> exitIfAuthenticated ctx
                |> validate validateSignIn
                |> checkLockout ctx
                |> passwordSignIn ctx
                |> log ctx (fun _ -> "Successfully signed in")
                |> getUserFromEmail ctx
                |> mapModel ctx Mapper.Map
                |> returnMessage next ctx
            
        let private signOutHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let signOut () =
                    task {
                        do! ctx.GetService<SignInManager<User>>().SignOutAsync()
                        return Success ()
                    } 
                signOut()
                |> noContent next ctx
        
        let private validTokenHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let tryToken () = task {
                        let token = ctx.BindQueryString<TokenView>()
                        return
                            match tokens ctx token.Token |> Seq.isEmpty with
                            | true -> failure (InvalidToken token.Token) HttpStatusCode.BadRequest
                            | false -> Success (new obj())
                    }  
                tryToken()
                |> wait 1000
                |> noContent next ctx
        
        let forgotPasswordHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let send (model : ForgotView) = task {
                    let! _ = ResetPasswordService.SendReset ctx model.Email
                    return Success model
                }
                tryGetModel<ForgotView> ctx
                |> bindFromTaskToTask send
                |> noContent next ctx
        
        let private resetPassword ctx =
            !>>! (fun (model : ResetView) -> task {
                let email = model.Email
                let token = model.Token
                let password = model.Password
                let! result = ResetPasswordService.ResetPassword ctx email token password
                return 
                    match result with
                    | None -> Success model
                    | Some err -> failure err HttpStatusCode.BadRequest
            })
        
        let resetPasswordHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<ResetView> ctx
                |> resetPassword ctx
                |> noContent next ctx
        

        let authHandler : HttpHandler =
            choose [
                POST >=> choose [
                    route "/register" >=> registerHandler
                    route "/signin" >=> signInHandler
                    route "/signout" >=> signOutHandler
                    route "/forgotPassword" >=> forgotPasswordHandler
                    route "/resetPassword" >=> resetPasswordHandler
                ]
                GET >=> choose [
                    route "/validRegisterToken" >=> validTokenHandler
                ]
            ]
