namespace Accounts.Handlers
    open System.Threading.Tasks
    open System.Net
    open Accounts
    open Accounts.ViewModels
    open Accounts.Validation.SettingsValidation
    open Accounts.Errors
    open Request.Flow
    open Request.RequestHelpers
    open Microsoft.AspNetCore.Http
    open Giraffe
    open Microsoft.Extensions.DependencyInjection
    open Microsoft.AspNetCore.Identity
    open Accounts.Data.Models
    open Accounts.Services
    open Accounts.Services.AutoTransfer
    open Accounts.Services.Configuration

    module SettingsHandler =
        let private updateCurrencies (ctx : HttpContext) =
            !>>! (fun currencies -> task {
                let userManager = ctx.GetService<UserManager<User>>()
                let! user = userManager.GetUserAsync(ctx.User)
                user.CommonCurrencies <- String.concat ";" currencies.Currencies
                let! res = userManager.UpdateAsync(user)
                return
                    match res.Succeeded with
                    | true -> Success user
                    | false -> failure UnexpectedError HttpStatusCode.InternalServerError
            })

        let currenciesHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<SetCurrencyView> ctx
                |> validate (validateCurrencies ctx)
                |> updateCurrencies ctx
                |> noContent next ctx

        let timezonesHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let timeZoneService = ctx.GetService<TimeZoneService>()
                timeZoneService.TimeZones
                |> Success
                |> Task.FromResult
                |> returnMessage next ctx

        let private updateTimezone (ctx : HttpContext) =
            !>>! (fun (timezone : SetTimezoneView) -> task {
                let userManager = ctx.GetService<UserManager<User>>()
                let! user = userManager.GetUserAsync(ctx.User)
                user.TimeZoneId <- timezone.Timezone
                let! res = userManager.UpdateAsync(user)
                let autoService = ctx.GetService<AutoTransferService>()
                autoService.AddCreateJobIfAnyAutos timezone.Timezone
                return
                    match res.Succeeded with
                    | true -> Success user
                    | false -> failure UnexpectedError HttpStatusCode.InternalServerError
            })

        let updateTimezoneHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<SetTimezoneView> ctx
                |> validate (validateTimezone ctx)
                |> updateTimezone ctx
                |> noContent next ctx

        let private updateUser (ctx : HttpContext) =
            !>>! (fun (userView : UserView) -> task {
                let userManager = ctx.GetService<UserManager<User>>()
                let! user = userManager.GetUserAsync(ctx.User)
                user.DisplayName <- userView.DisplayName
                let! res = userManager.UpdateAsync(user)
                return
                    match res.Succeeded with
                    | true -> Success user
                    | false -> failure UnexpectedError HttpStatusCode.InternalServerError
            })

        let updateUserHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                tryGetModel<UserView> ctx
                |> validate (validateUser ctx)
                |> updateUser ctx
                |> returnMessage next ctx

        let configurationHandler : HttpHandler =
            fun (next : HttpFunc) (ctx : HttpContext) ->
                let configService = ctx.GetService<ConfigurationService>()
                configService.GetFrontEndConfiguration()
                |> Success
                |> Task.value
                |> returnMessage next ctx

        let settingsHandler : HttpHandler=
            choose [
                POST >=> choose [
                    route "/currencies" >=> authentication currenciesHandler
                    route "/timezone" >=> authentication updateTimezoneHandler
                ]
                PUT >=> choose [
                    route "/user" >=> authentication updateUserHandler
                ]
                GET >=> choose [
                    route "/timezone" >=> timezonesHandler
                    routex "/configuration(/?)" >=> configurationHandler
                ]
            ]