namespace Accounts.Setup

open Microsoft.Extensions.DependencyInjection
open Accounts.Repository
open Accounts.Data.Models

module Repositories =
    let configureRepositories (services : IServiceCollection) =
        services.AddTransient<RepositoryFactory>() |> ignore

        services.AddScoped<IRepository<Token>, Repository<Token>>() |> ignore
        services.AddScoped<IRepository<TokenType>, Repository<TokenType>>() |> ignore
        services.AddScoped<IRepository<Period>, Repository<Period>>() |> ignore
        services.AddScoped<IRepository<Account>, Repository<Account>>() |> ignore
        services.AddScoped<IRepository<AutoTransfer>, Repository<AutoTransfer>>() |> ignore
        services.AddScoped<IRepository<AccountShare>, Repository<AccountShare>>() |> ignore
        services.AddScoped<ITransferRepository, TransferRepository>() |> ignore
        services.AddScoped<ITransfersSummaryRepository, TransfersSummaryRepository>() |> ignore