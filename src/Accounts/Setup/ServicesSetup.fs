namespace Accounts.Setup

open Microsoft.AspNetCore.Hosting
open Microsoft.Extensions.DependencyInjection
open Accounts.Services
open Accounts.Services.Rates
open Accounts.Services.Accounts
open Accounts.Services.AutoTransfer
open Accounts.Services.TransfersSummary
open Accounts.Services.Configuration

module Services =
    let configureServices (ctx : WebHostBuilderContext) (services : IServiceCollection) =
        if ctx.HostingEnvironment.IsDevelopment() then
            services.AddSingleton<IRateClient, MockRateClient>() |> ignore
        else
            services.AddSingleton<IRateClient, RateClient>() |> ignore

        services.AddSingleton<TimeZoneService>() |> ignore
        services.AddTransient<TransferService>() |> ignore
        services.AddTransient<AutoTransferService>() |> ignore
        services.AddTransient<AccountService>() |> ignore
        services.AddTransient<TransfersSummaryService>() |> ignore
        services.AddTransient<UserService>() |> ignore
        services.AddTransient<ConfigurationService>() |> ignore