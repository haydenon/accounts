module Accounts.Setup.Auth

open System
open Accounts
open Accounts.AuthRoles
open Accounts.Data.Models
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Identity
open Microsoft.AspNetCore.Authentication.Cookies
open Microsoft.Extensions.DependencyInjection

let cookieAuth (options : CookieAuthenticationOptions) =
    options.Cookie.HttpOnly     <- true
    options.Cookie.SecurePolicy <- CookieSecurePolicy.SameAsRequest
    options.SlidingExpiration   <- true
    options.ExpireTimeSpan <- TimeSpan.FromDays 14.0

let configureIdentity (options : IdentityOptions) =
    let pass = options.Password
    pass.RequiredLength <- 8
    pass.RequireDigit <- false
    pass.RequireUppercase <- false
    pass.RequireNonAlphanumeric <- false
    let lockout = options.Lockout
    lockout.DefaultLockoutTimeSpan <- TimeSpan.FromMinutes(5.0);
    lockout.MaxFailedAccessAttempts <- 5; 
    lockout.AllowedForNewUsers <- true;

let configureAuthServices (services : IServiceCollection) =
    services.AddAuthentication()
        .AddCookie(cookieAuth) |> ignore
    services.AddIdentity<User, IdentityRole>(Action<IdentityOptions> configureIdentity)
        .AddEntityFrameworkStores<Data.AccountsContext>()
        .AddDefaultTokenProviders() |> ignore

let addAuthRoles (services : IServiceProvider) =
  let roleManager = services.GetService<RoleManager<IdentityRole>>();
  let createRole role = 
    let exists = Seq.exists (fun (r : IdentityRole) -> r.Name = role) roleManager.Roles
    match exists with
    | true -> ()
    | false -> (roleManager.CreateAsync(new IdentityRole(role))).Result |> ignore

  [ AdminRole; HangfireRole ]
  |> List.iter createRole