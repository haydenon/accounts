namespace Accounts.Setup

open System
open System.Threading.Tasks

open FSharp.Control.Tasks.ContextInsensitive

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Identity
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.DependencyInjection

open Hangfire;
open Hangfire.Dashboard;
open Hangfire.PostgreSql;

open Accounts.Data.Models
open Accounts.Services.AutoTransfer
open Accounts.AuthRoles
open Accounts.Repository.Helpers

type HangfireDashboardAuthFilter() =
    interface IDashboardAuthorizationFilter with
        member __.Authorize(context : DashboardContext) =
            let ctx = context.GetHttpContext()
            let userManager = ctx.RequestServices.GetService<UserManager<User>>()
            let isAdmin = task {
                let! user = userManager.GetUserAsync(ctx.User)
                return!
                    if isNull user then Task.FromResult false
                    else userManager.IsInRoleAsync(user, HangfireRole)
            }
            isAdmin.Result

module Hangfire =
    let config (configuration : IConfiguration) (hangfireConfig : IGlobalConfiguration) =
        let connection = configuration.GetSection("Data").GetValue<string>("HangfireDatabaseConnection")
        let postgreSqlStorage = PostgreSqlStorage(connection)
        hangfireConfig.UseStorage(postgreSqlStorage) |> ignore

    let configureHangfireServices (services : IServiceCollection) (configuration : IConfiguration) =
        services.AddHangfire(Action<IGlobalConfiguration>(config configuration)) |> ignore

    let configureHangfire (app : IApplicationBuilder) =
        let options = BackgroundJobServerOptions();
        options.ServerTimeout <- TimeSpan.FromDays(1.0);
        app.UseHangfireServer(options) |> ignore
        let dashOpts = new DashboardOptions()
        dashOpts.Authorization <- [| new HangfireDashboardAuthFilter() |]
        app.UseHangfireDashboard("/hangfire", dashOpts) |> ignore

    let private updateAutoTransferJobs (services : IServiceProvider) =
        let userManager = services.GetService<UserManager<User>>()
        let updateJob (timeZone : TimeZoneInfo) =
            let id = jobId timeZone.Id
            let timeZoneId = timeZone.Id
            let act = AutoTransferService.GetAction(timeZoneId)
            RecurringJob.AddOrUpdate<AutoTransferService>(
                id,
                act,
                Cron.Daily(0, 1),
                timeZone)
        let timezones =
            userManager.Users
            |> Array.ofSeq
            |> Array.map ((fun u -> u.TimeZoneId) >> timeZoneFromId)
            |> Array.distinct
        Array.iter updateJob timezones

    let createHangfireJobs (services : IServiceProvider) =
        updateAutoTransferJobs services
