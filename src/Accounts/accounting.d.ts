declare interface IAccounting {
  formatMoney: (amount: number, opts: any) => string;
}

declare const def: IAccounting;

export default def;