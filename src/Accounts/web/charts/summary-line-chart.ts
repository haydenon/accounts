import moment, { Moment } from 'moment';

import { ITransfersSummary } from '../transfers/transfersSummary';
import { currencyPrecision, formatMoney, sort } from '../util';

export type ChartItem = number | string | { meta: string, value: number | string } | null;

export const getChartItemValue = (item: ChartItem): number => {
    if (typeof (item) === 'number') {
        return item;
    } else if (typeof (item) === 'string') {
        return parseFloat(item);
    } else if (item && typeof (item.value) === 'number') {
        return item.value;
    } else if (item && typeof (item.value) === 'string') {
        return parseFloat(item.value);
    } else {
        return 0;
    }
};

export interface IChartData {
    labels: string[];
    series: ChartItem[][];
}

export const getSummaryLineData = (
    summaries: ITransfersSummary[],
    start: Moment,
    end: Moment,
    currency: string,
    defaultBalance: number,
    showAllDates: boolean): IChartData => {
    summaries = sort(summaries, (s1, s2) => s1.startDate.localeCompare(s2.startDate));

    const series = [];
    const labels = [];

    const current = moment(start);

    let currentBalance = summaries.length ? summaries[0].startBalance : defaultBalance;
    while (current <= end) {
        if (showAllDates || current.day() === 1) {
            labels.push(current.format('D MMM'));
        } else {
            labels.push('');
        }

        const entryIndex = summaries.findIndex((s) => moment(s.startDate).isSame(current, 'day'));
        const entry = entryIndex !== -1 ? summaries[entryIndex] : undefined;

        if (entry) {
            currentBalance = parseFloat((entry.startBalance + entry.amount).toFixed(currencyPrecision(currency)));
        }

        if (!entry && current.isSame(start)) {
            series.push({ meta: 'Start balance', value: currentBalance });
        } else if (!entry && current.isSame(end)) {
            series.push({ meta: 'End balance', value: currentBalance });
        } else {
            series.push(entry
                ? {
                    meta: `${current.format('D MMM')} ${formatMoney(currency, entry.amount)}`,
                    value: currentBalance
                }
                : null);
        }

        if (entryIndex !== -1) {
            summaries.splice(0, entryIndex + 1);
        }
        current.add(1, 'day');
    }

    return {
        labels,
        series: [series]
    };
};
