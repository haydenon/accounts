import React from 'react';
import { connect } from 'react-redux';

import { Moment } from 'moment';

import Chartist from 'chartist';
import ChartistGraph from 'react-chartist';
(window as any).Chartist = Chartist;

import { ITransfersSummary } from 'web/transfers/transfersSummary';
import { IAccount } from '../accounts/accounts';
import { IScreenState, sizes } from '../screen';
import { formatMoney, max, min } from '../util';
import { ChartItem, getChartItemValue, getSummaryLineData } from './summary-line-chart';

const chartistThreshold: any = require('chartist-plugin-threshold');
require('chartist-plugin-tooltips');

import '../styles/chartist-custom.scss';
import '../styles/chartist-plugin-tooltip.scss';
import '../styles/chartist.min.css';

interface IProps {
    summaries: ITransfersSummary[];
    account: IAccount;
    start: Moment;
    end: Moment;
    screenWidth: IScreenState;
}

const showGridX = (width: number): boolean =>
    width > sizes.SMALL;

const showAllDates = (width: number): boolean =>
    width > sizes.XLARGE;

const getMax = (data: ChartItem[]) => {
    const items = data.filter((i) => i).map(getChartItemValue);
    if (!items.length) {
        return 10;
    }
    const m = max(items);
    return Math.max(m >= 0 ? m * 1.1 : m * 0.9, 10);
};

const getMin = (data: ChartItem[]) => {
    const items = data.filter((i) => i).map(getChartItemValue);
    if (!items.length) {
        return -10;
    }
    const m = min(items);
    return Math.min(m <= 0 ? m * 1.1 : m * 0.9, -10);
};

class SummaryLineChart extends React.Component<IProps> {
    public render() {
        const chartData = getSummaryLineData(
            this.props.summaries,
            this.props.start,
            this.props.end,
            this.props.account.currency,
            this.props.account.balance,
            showAllDates(this.props.screenWidth.width)
        );
        const opts = this.options(chartData.series[0]);
        return (
            <div className="summary-line-chart" >
                <ChartistGraph data={chartData} type={'Line'} options={opts} />
            </div>
        );
    }

    private options = (data: ChartItem[]) => ({
        axisX: {
            showGrid: showGridX(this.props.screenWidth.width)
        },
        axisY: {
            offset: 30
        },
        showArea: true,
        plugins: [
            chartistThreshold(),
            Chartist.plugins.tooltip({
                transformTooltipTextFnc: (num: number) => `Bal: ${formatMoney('NZD', num)}`
            })
        ],
        chartPadding: {
            top: 25,
            right: 20,
            bottom: 5,
            left: 25
        },
        lineSmooth: (Chartist.Interpolation as any).monotoneCubic({
            fillHoles: true,
        }),
        high: getMax(data),
        low: getMin(data),
    })
}

const mapStateToProps = (state: any) => ({
    screenWidth: state.screenWidth
});

export default connect(mapStateToProps)(SummaryLineChart);
