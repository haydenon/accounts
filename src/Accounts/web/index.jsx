import * as e6p from 'es6-promise';
e6p.polyfill();

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { routerMiddleware } from 'react-router-redux'
import { createStore, applyMiddleware, compose } from 'redux';
import Routes from './routes'
import { reducer } from './reducer';
import thunk from 'redux-thunk';

import { updateScreenWidth } from './screen';

const history = createHistory()

function createAppStore() {
  if (process.env.NODE_ENV === 'production') {
    return createStore(
      reducer,
      applyMiddleware(...[thunk, routerMiddleware(history)])
    );
  } else {
    const middleware = [applyMiddleware(...[thunk, routerMiddleware(history)])]
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
      middleware.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    return (module.hot && module.hot.data && module.hot.data.store)
      ? module.hot.data.store
      : createStore(
        reducer,
        compose.apply(null, middleware)
      );
  }
}

const store = createAppStore();

ReactDOM.render(
  <Provider store={store}>
    <AppContainer>
      <Routes history={history} />
    </AppContainer>
  </Provider>,
  document.getElementById('app')
);

window.addEventListener('resize', () => {
  store.dispatch(updateScreenWidth())
});

// Hot Module Replacement API
if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept();

  module.hot.dispose((data) => {
    data.store = store;
  });
}
