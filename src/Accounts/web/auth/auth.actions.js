import { push } from 'react-router-redux'
import { post } from '../util';
import { message } from 'antd';

export const SIGN_IN = 'SIGN_IN'
export const SIGNED_IN = 'SIGNED_IN'
export const SIGN_IN_FAILED = 'FAILED_SIGN_IN'

export const SIGN_OUT = 'SIGN_OUT'
export const SIGNED_OUT = 'SIGNED_OUT'
export const SIGN_OUT_FAILED = 'FAILED_SIGN_OUT'

export const signIn = userDetails => ({
    type: SIGN_IN,
    userDetails
})

export const signInFailed = () => ({
    type: SIGN_IN_FAILED,
})

export const signedIn = user => ({
    type: SIGNED_IN,
    user
})

export const signOut = () => ({
    type: SIGN_OUT
})

export const signOutFailed = () => ({
    type: SIGN_OUT_FAILED
})

export const signedOut = () => ({
    type: SIGNED_OUT
})

const signInBase = (userDetails, dispatch, getState, onError) => {
    dispatch(signIn(userDetails))
    post(`/api/auth/signin`, userDetails)
        .then(json => {
            dispatch(signedIn(json))
            const pathname = getState().routing.location.pathname;
            if (pathname.startsWith('/signin') || pathname.startsWith('/register')){
                dispatch(push('/'))
            }
        })
        .catch((err) => {
            onError && onError(dispatch, err)
            dispatch(signInFailed(err.message))
        })
}

export const checkIfSignedIn = (onError) => (dispatch, getState) => {
    if (getState().auth.user) return;

    const userDetails = { email: "", password: "" }
    signInBase(userDetails, dispatch, getState, onError)
}

export const fetchSignIn = (userDetails = { email: "", password: "" }) => (dispatch, getState) =>
    signInBase(userDetails, dispatch, getState, (d, err) => message.error(err.message, 4));

export const fetchSignOut = () => dispatch => {
    dispatch(signOut())
    post(`/api/auth/signout`)
        .then(() => {
            dispatch(signedOut())
            dispatch(push('/signin'))
        })
        .catch(() => dispatch(signOutFailed()))
}
