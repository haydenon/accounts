import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Steps, Select, Spin } from 'antd';
import { fetchCheckToken, fetchRegister, finishRegistration, currenciesSet } from './register.actions';
import { checkIfSignedIn } from './auth.actions';
import { loadTimezones, updateUserCurrencies } from '../settings/settings.actions';
import { loadRates } from '../rates/rates.actions';
import { validateSubmit } from '../util';
import { withRouter } from 'react-router-dom';
import SetCurrenciesForm from '../settings/CurrenciesForm';
import config from '../config';

import './register.scss';
import { createTimezoneHelper } from '../util/stateHelpers';

const FormItem = Form.Item;
const Step = Steps.Step;
const Option = Select.Option;

class Register extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.dispatch(checkIfSignedIn())
        if (!this.props.rates.data) {
            this.props.dispatch(loadRates())
        }
        if (!this.props.settings.timezones.length) {
            this.props.dispatch(loadTimezones())
        }
    }

    currentStep = () => {
        if (config.tokensEnabled && !this.props.register.validToken) return (<WrappedTokenForm />)
        else if (!this.props.register.registered) return (<WrappedDetailForm />)
        else if (!this.props.register.setCurrencies) return (<WrappedCurrenciesForm />)
        else return (<ConnectedSuccessBanner />)
    }

    current = () => {
        let value = 0;
        if (config.tokensEnabled && !this.props.register.validToken)
            value = 0
        else if (!this.props.register.registered)
            value = 1;
        else if (!this.props.register.setCurrencies)
            value = 2;
        else
            value = 3;

        if (!config.tokensEnabled)
            value -= 1;

        return value;
    }

    render() {
        return (
            <div>
                <Steps className="register-steps" current={this.current()}>
                    {config.tokensEnabled ? <Step title="Unlock" icon={<Icon type="key" />} /> : null}
                    <Step title="Details" icon={<Icon type="solution" />} />
                    <Step title="Currencies" icon={<Icon type="pay-circle-o" />} />
                    <Step title="Done" icon={<Icon type="check-circle-o" />} />
                </Steps>
                {this.currentStep()}
            </div>
        );
    }
}
Register.propTypes = {
    rates: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
    register: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
};

class SuccessBanner extends React.Component {
    componentDidMount() {
        this.props.dispatch(finishRegistration());
    }

    render() {
        return (
            <div className="register-welcome">
                <p>
                    Welcome<br />
                    Thanks for registering!
                </p>
            </div>
        )
    }
}
SuccessBanner.propTypes = {
    dispatch: PropTypes.func.isRequired
}

const ConnectedSuccessBanner = connect()(SuccessBanner);

class TokenForm extends React.Component {
    handleSubmit = validateSubmit(this.props.form, () => {
        this.props.dispatch(fetchCheckToken(this.props.form.getFieldsValue()))
    })

    render() {
        const { getFieldDecorator } = this.props.form;
        const fetching = this.props.register.isFetching;
        return (
            <Form onSubmit={this.handleSubmit} className="auth-form">
                <FormItem>
                    {getFieldDecorator('token', {
                        validateTrigger: 'onBlur',
                        rules: [{ required: true, message: 'Please input your registration token!' }],
                    })(
                        <Input prefix={<Icon type="key" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Registration token" disabled={fetching} />
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" className="register-form-button" loading={fetching}>
                        Check {fetching || <Icon type="check-circle" />}
                    </Button>
                </FormItem>
            </Form>
        )
    }
}
TokenForm.propTypes = {
    form: PropTypes.object.isRequired,
    register: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
}

class DetailForm extends React.Component {
    handleSubmit = validateSubmit(this.props.form, () => {
        this.props.dispatch(fetchRegister(this.props.form.getFieldsValue()))
    })

    checkPassword = (rule, value, callback) => {
        const password = this.props.form.getFieldValue('password');
        if (password === value) callback()
        else callback(true);
    }

    checkConfirm = (rule, value, callback) => {
        const form = this.props.form
        if (value && form.getFieldValue('passwordConfirmation')) {
            form.validateFields(['passwordConfirmation'], { force: true });
        }
        callback();
    }

    render() {
        if (this.props.settings.timezonesFetching)
            return <div className="spin-center"><Spin size="large" /></div>;
        const { getFieldDecorator } = this.props.form;
        const fetching = this.props.register.isFetching;
        return (
            <Form onSubmit={this.handleSubmit} className="auth-form">
                <FormItem>
                    {getFieldDecorator('displayName', {
                        validateTrigger: 'onBlur',
                        rules: [{ required: true, message: 'Please input your display name' }],
                    })(
                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Display Name" disabled={fetching} />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('email', {
                        validateTrigger: 'onBlur',
                        rules: [{ type: 'email', required: true, message: 'Please input a valid email address' }],
                    })(
                        <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email Address" disabled={fetching} />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('timezone', {
                        initialValue: this.timezoneHelper.userTimezone().value,
                        validateTrigger: 'onBlur',
                        rules: [{ required: true, message: 'Please select a timezone' }],
                    })(
                        <Select
                            placeholder="Select timezone"
                            disabled={fetching}>
                            {this.timezoneHelper.timezones().map((timezone) => <Option key={timezone.value} value={timezone.value}>{timezone.display}</Option>)}
                        </Select>
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('password', {
                        validateTrigger: 'onBlur',
                        rules: [
                            { required: true, message: 'Please input your password' },
                            { message: '', validator: this.checkConfirm }
                        ],
                    })(
                        <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" disabled={fetching} />
                    )}
                </FormItem>
                <FormItem>
                    {getFieldDecorator('passwordConfirmation', {
                        validateTrigger: 'onBlur',
                        rules: [
                            { required: true, message: 'Please confirm your password' },
                            { message: 'The passwords must match!', validator: this.checkPassword }
                        ],
                    })(
                        <Input prefix={<Icon type="lock"
                            style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password" placeholder="Password Confirmation"
                            disabled={fetching} />
                    )}
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" className="register-form-button" loading={this.props.register.isFetching}>
                        Register {fetching || <Icon type="user-add" />}
                    </Button>
                </FormItem>
            </Form>
        )
    }

    get timezoneHelper() { return createTimezoneHelper(this.props.settings); }
}
DetailForm.propTypes = {
    form: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired,
    register: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
}

class CurrenciesForm extends React.Component {
    render() {
        return (
            <div className="auth-form">
                <p>Choosing your most common currencies here will help make them easier to select when creating accounts or transfers</p>
                <SetCurrenciesForm onSubmit={this.handleSubmit} />
            </div>
        );
    }

    handleSubmit = (currencies) => {
        this.props.dispatch(updateUserCurrencies({ currencies }))
            .then(() => {
                if (!this.props.settings.currenciesError) {
                    this.props.dispatch(currenciesSet())
                }
            });
    }
}
CurrenciesForm.propTypes = {
    settings: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    settings: state.settings,
    rates: state.rates,
    register: state.register
});

const WrappedTokenForm = withRouter(connect(mapStateToProps)(Form.create()(TokenForm)));
const WrappedDetailForm = withRouter(connect(mapStateToProps)(Form.create()(DetailForm)));
const WrappedCurrenciesForm = withRouter(connect(mapStateToProps)(CurrenciesForm));

export default withRouter(connect(mapStateToProps)(Register));
