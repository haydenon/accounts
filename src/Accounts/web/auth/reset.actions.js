import {post} from '../util';
import { message } from 'antd'

export const PASSWORD_RESET_SEND = "SEND_PASSWORD_RESET";
export const PASSWORD_RESET_SENT = "SENT_PASSWORD_RESET";
export const PASSWORD_RESET_SEND_FAILED = "SEND_PASSWORD_RESET_FAILED";
export const PASSWORD_RESET_SEND_RESET = "SEND_PASSWORD_RESET_RESET";

export const PASSWORD_RESET = "PASSWORD_RESET";
export const PASSWORD_RESET_DONE = "PASSWORD_RESET_DONE";
export const PASSWORD_RESET_FAILED = "PASSWORD_RESET_FAILED";

export const passwordResetSend = () => ({
    type: PASSWORD_RESET_SEND
})

export const passwordResetSent = () => ({
    type: PASSWORD_RESET_SENT
})

export const passwordResetSendFailed = (error) => ({
    type: PASSWORD_RESET_SEND_FAILED,
    error
})

export const passwordResetSendReset = () => ({
    type: PASSWORD_RESET_SEND_RESET,
})

export const passwordReset = () => ({
    type: PASSWORD_RESET
})

export const passwordResetDone = () => ({
    type: PASSWORD_RESET_DONE
})

export const passwordResetFailed = (error) => ({
    type: PASSWORD_RESET_FAILED,
    error
})

export const sendPasswordReset = (reset) => (dispatch) => {
    dispatch(passwordResetSend());
    post(`/api/auth/forgotPassword`, reset)
        .then(() => dispatch(passwordResetSent()))
        .catch((err) => {
            dispatch(passwordResetSendFailed(err.message))
            message.error(err.message, 6)
        })
}

export const resetPassword = (reset) => (dispatch) => {
    dispatch(passwordReset());
    return post(`api/auth/resetPassword`, reset)
        .then(() => {
            dispatch(passwordResetDone())
            message.success("Successfully reset password", 3)
        })
        .catch((err) => {
            dispatch(passwordResetFailed(err.message))
            message.error(err.message, 6)
        })
}
