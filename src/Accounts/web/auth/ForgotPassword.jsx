import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Input } from 'antd';
import { sendPasswordReset, passwordResetSendReset } from './reset.actions';
import { validateSubmit } from '../util';
const FormItem = Form.Item;

class ForgotPasswordForm extends React.Component {
    handleSubmit = validateSubmit(this.props.form, (reset) => {
        this.props.dispatch(sendPasswordReset(reset))
    });

    resetForgot = () => {
        this.props.dispatch(passwordResetSendReset())
    }

    render() {
        const isFetching = this.props.reset.isFetching;
        const { getFieldDecorator } = this.props.form;
        const formItems = [
            <FormItem key="email">
                {getFieldDecorator('email', {
                    validateTrigger: 'onBlur',
                    rules: [{ type: 'email', required: true, message: 'Please input a valid email address!' }],
                })(
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email Address" disabled={isFetching} />
                    )}
            </FormItem>,
            <FormItem key="reset">
                <Button type="primary" icon="check" htmlType="submit" className="full-width" loading={isFetching}>
                    Send password reset
                    </Button>
            </FormItem>
        ];
        const sentItem = [
            <p key="sent-paragraph" className="result-text">A password reset has been sent to your email address</p>,
            <Link key="back" to="/signin" className="result-text" onClick={this.resetForgot}>Back to sign in</Link>
        ]
        return (
            <Form onSubmit={this.handleSubmit} className="auth-form">
                <h1>Forgot password</h1>
                {this.props.reset.sentReset ? sentItem : formItems}
            </Form>);
    }
}
ForgotPasswordForm.propTypes = {
    reset: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

const WrappedForgotPasswordForm = Form.create()(ForgotPasswordForm);

const mapStateToProps = (state) => ({
    reset: state.reset
});

export default withRouter(connect(mapStateToProps)(WrappedForgotPasswordForm))
