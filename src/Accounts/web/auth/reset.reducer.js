import {
    PASSWORD_RESET_SEND,
    PASSWORD_RESET_SENT,
    PASSWORD_RESET_SEND_FAILED,
    PASSWORD_RESET_SEND_RESET,
    PASSWORD_RESET,
    PASSWORD_RESET_DONE,
    PASSWORD_RESET_FAILED
} from './reset.actions';

export function reset(state = {
    isFetching: false,
    sendResetError: null,
    sentReset: false,
    resetError: null,
    resetPassword: false
}, action) {
    switch (action.type) {
        case PASSWORD_RESET_SEND:
            return {
                ...state,
                isFetching: true,
                sendResetError: null,
                sentReset: false
            }
        case PASSWORD_RESET_SENT:
            return {
                ...state,
                isFetching: false,
                sentReset: true
            }
        case PASSWORD_RESET_SEND_FAILED:
            return {
                ...state,
                isFetching: false,
                sendResetError: action.error,
            }
        case PASSWORD_RESET_SEND_RESET:
            return {
                ...state,
                sentReset: false,
                sendResetError: null
            }
        case PASSWORD_RESET:
            return {
                ...state,
                isFetching: true,
                resetPassword: false,
                resetError: null
            }
        case PASSWORD_RESET_DONE:
            return {
                ...state,
                isFetching: false,
                resetPassword: true
            }
        case PASSWORD_RESET_FAILED:
            return {
                ...state,
                isFetching: false,
                resetError: action.error
            }
        default:
            return state;
    }
}
