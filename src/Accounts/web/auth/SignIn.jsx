import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
const FormItem = Form.Item;
import { validateSubmit } from '../util';
import { fetchSignIn, checkIfSignedIn } from './auth.actions';
import '../styles/signin.scss';

class SignInForm extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit = validateSubmit(this.props.form, () => {
    this.props.dispatch(fetchSignIn(this.props.form.getFieldsValue()))
  });

  componentDidMount() {
    this.props.dispatch(checkIfSignedIn())
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <div>
        <Form onSubmit={this.handleSubmit} className="login-form auth-form">
          <FormItem>
            {getFieldDecorator('email', {
              validateTrigger: 'onBlur',
              rules: [{ type: 'email', required: true, message: 'Please input a valid email address!' }],
            })(
              <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email Address" disabled={this.props.isFetching} />
              )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('password', {
              validateTrigger: 'onBlur',
              rules: [{ required: true, message: 'Please input your password!' }],
            })(
              <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" disabled={this.props.isFetching} />
              )}
          </FormItem>
          <FormItem>
            {getFieldDecorator('rememberMe', {
              valuePropName: 'checked',
              initialValue: true,
            })(
              <Checkbox disabled={this.props.isFetching}>Remember me</Checkbox>
              )}
            <Link className="login-form-forgot" to="/forgotPassword">Forgot password</Link>
            <Button type="primary" htmlType="submit" className="login-form-button" loading={this.props.isFetching}>
              Sign in {this.props.isFetching || <Icon type="login" />}
            </Button>
            Or <Link to="/register">register now!</Link>
          </FormItem>
        </Form>
      </div>
    );
  }
}
SignInForm.propTypes = {
  form: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  dispatch: PropTypes.func.isRequired,
};

const WrappedSignInForm = Form.create()(SignInForm);

const mapStateToProps = (state) => {
  return ({
    isFetching: state.auth.isFetching
  })
}

export default connect(mapStateToProps)(WrappedSignInForm);
