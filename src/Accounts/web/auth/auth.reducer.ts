import { Action } from 'redux';

import {
    ICurrenciesUpdateStart,
    ITimezonesUpdateSuccess,
    IUserUpdateSuccess,
    TIMEZONE_UPDATED,
    UPDATE_CURRENCIES,
    USER_UPDATED
} from '../settings/settings.actions';
import { IStringErrorAction } from '../util/reducerHelpers';
import {
    SIGN_IN,
    SIGN_IN_FAILED,
    SIGN_OUT,
    SIGNED_IN,
    SIGNED_OUT,
} from './auth.actions';

export interface IUser {
    displayName: string;
    email: string;
    commonCurrencies: string[];
    timezone: string;
}

export interface IAuthState {
    attempted: boolean;
    user?: IUser;
    isFetching: boolean;
    signInError?: string;
    updateCurrencies: string[];
    currenciesError?: string;
    currenciesFetching: boolean;
}

const ORIGINAL_STATE: IAuthState = {
    attempted: false,
    isFetching: false,
    updateCurrencies: [],
    currenciesFetching: false
};

interface IUserAction extends Action {
    user: IUser;
}

export function auth(state = ORIGINAL_STATE, action: Action) {
    switch (action.type) {
        case SIGN_IN:
            return {
                ...state,
                isFetching: true,
                signInError: null,
                user: null
            };
        case SIGNED_IN:
            return {
                ...state,
                attempted: true,
                isFetching: false,
                user: (action as IUserAction).user
            };
        case SIGN_IN_FAILED:
            return {
                ...state,
                attempted: true,
                isFetching: false,
                signInError: (action as IStringErrorAction).error
            };
        case SIGN_OUT:
            return {
                ...state,
                isFetching: true
            };
        case SIGNED_OUT:
            return {
                ...state,
                user: null,
                isFetching: false,
            };
        case UPDATE_CURRENCIES:
            if (!state.user) return state;

            return {
                ...state,
                user: {
                    ...state.user,
                    commonCurrencies: (action as ICurrenciesUpdateStart).data.currencies
                }
            };
        case TIMEZONE_UPDATED:
            if (!state.user) return state;

            return {
                ...state,
                user: {
                    ...state.user,
                    timezone: (action as ITimezonesUpdateSuccess).data,
                },
            };
        case USER_UPDATED:
            return {
                ...state,
                user: {
                    ...state.user,
                    displayName: (action as IUserUpdateSuccess).data.displayName
                },
            };
        default:
            return state;
    }
}
