import React from 'react';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Input } from 'antd';
import queryString from 'query-string';
import { resetPassword } from './reset.actions';
import { validateSubmit } from '../util';
const FormItem = Form.Item;

class ResetPasswordForm extends React.Component {
    handleSubmit = validateSubmit(this.props.form, (reset) => {
        delete reset.passwordConfirmation;
        const parsedQuery = queryString.parse(this.props.location.search);
        reset.token = parsedQuery.token;
        reset.email = parsedQuery.user;

        this.props.dispatch(resetPassword(reset));
    });

    checkPassword = (rule, value, callback) => {
        const password = this.props.form.getFieldValue('password');
        if (password === value) callback()
        else callback(true);
    }

    checkConfirm = (rule, value, callback) => {
        const form = this.props.form
        if (value && form.getFieldValue('passwordConfirmation')) {
            form.validateFields(['passwordConfirmation'], { force: true });
        }
        callback();
    }

    render() {
        const isFetching = this.props.reset.isFetching;
        const { getFieldDecorator } = this.props.form;
        const resetItem = [
            <p key="reset-paragraph" className="result-text">Your password has been successfully reset</p>,
            <Link key="back" to="/signin" className="result-text">Back to sign in</Link>
        ]
        const resetForm = [
            <FormItem key="password">
                {getFieldDecorator('password', {
                    validateTrigger: 'onBlur',
                    rules: [
                        { required: true, message: 'Please input your password' },
                        { message: '', validator: this.checkConfirm }
                    ],
                })(
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password"
                        disabled={isFetching} />
                    )}
            </FormItem>,
            <FormItem key="confirm">
                {getFieldDecorator('passwordConfirmation', {
                    validateTrigger: 'onBlur',
                    rules: [
                        { required: true, message: 'Please confirm your password' },
                        { message: 'The passwords must match!', validator: this.checkPassword }
                    ],
                })(
                    <Input prefix={<Icon type="lock"
                        style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password" placeholder="Password Confirmation"
                        disabled={isFetching} />
                    )}
            </FormItem>,
            <FormItem key="submit">
                <Button type="primary" icon="check" htmlType="submit" className="full-width" loading={isFetching}>
                    Reset password
                    </Button>
            </FormItem>
        ]
        return (
            <Form onSubmit={this.handleSubmit} className="auth-form">
                <h1>Reset password</h1>
                {this.props.reset.resetPassword ? resetItem : resetForm}
            </Form>);
    }
}
ResetPasswordForm.propTypes = {
    reset: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

const WrappedResetPasswordForm = Form.create()(ResetPasswordForm);

const mapStateToProps = (state) => ({
    reset: state.reset,
    location: state.routing.location,
});

export default withRouter(connect(mapStateToProps)(WrappedResetPasswordForm))
