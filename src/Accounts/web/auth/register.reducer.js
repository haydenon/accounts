import {
    CHECK_TOKEN,
    TOKEN_VALID,
    TOKEN_INVALID,
    REGISTER,
    REGISTERED,
    REGISTER_FAILED,
    REGISTRATION_CLEAR,
    CURRENCIES_SET,
} from './register.actions';

export function register(state = {
    isFetching: false,
    validToken: false,
    tokenError: null,
    registerError: null,
    registered: false,
    setCurrencies: false,
}, action) {
    switch (action.type) {
        case CHECK_TOKEN:
            return {
                ...state,
                isFetching: true,
                token: action.token,
                tokenError: null,
                validToken: false
            };
        case TOKEN_VALID:
            return {
                ...state,
                isFetching: false,
                validToken: true,
            };
        case TOKEN_INVALID:
            return {
                ...state,
                isFetching: false,
                validToken: false,
                tokenError: action.error
            };
        case REGISTER:
            return {
                ...state,
                isFetching: true,
                registerError: null,
                registered: false
            }
        case REGISTERED:
            return {
                ...state,
                isFetching: false,
                registered: true
            }
        case REGISTER_FAILED:
            return {
                ...state,
                isFetching: false,
                registerError: action.error
            }
        case CURRENCIES_SET:
            return {
                ...state,
                setCurrencies: true
            }
        case REGISTRATION_CLEAR:
            return {
                isFetching: false,
                validToken: false,
                tokenError: null,
                registerError: null,
                registered: false,
                setCurrencies: false
            }
        default:
            return state;
    }
}
