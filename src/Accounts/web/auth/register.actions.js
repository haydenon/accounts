import { get, post } from '../util';
import { signedIn } from './auth.actions';
import { message } from 'antd';
import { push } from 'react-router-redux';
import config from '../config';

export const CHECK_TOKEN = 'CHECK_TOKEN';
export const TOKEN_VALID = 'TOKEN_VALID';
export const TOKEN_INVALID = 'TOKEN_INVALID';

export const REGISTER = 'REGISTER';
export const REGISTERED = 'REGISTERED';
export const REGISTER_FAILED = 'REGISTER_FAILED';

export const CURRENCIES_SET = 'CURRENCIES_SET';

export const REGISTRATION_CLEAR = 'CLEAR REGISTRATION'

export const checkToken = token => ({
    type: CHECK_TOKEN,
    token: token
})

export const tokenValid = () => ({
    type: TOKEN_VALID,
})

export const tokenInvalid = (error) => ({
    type: TOKEN_INVALID,
    error
})

export const register = () => ({
    type: REGISTER,
})

export const registered = () => ({
    type: REGISTERED,
})

export const registerFailed = (error) => ({
    type: REGISTER_FAILED,
    error
})

export const currenciesSet = () => ({
    type: CURRENCIES_SET
})

export const clearRegistration = () => ({
    type: REGISTRATION_CLEAR,
})

export const fetchCheckToken = (token = { token: '' }) => dispatch => {
    dispatch(checkToken(token))
    get(`/api/auth/validRegisterToken?token=${token.token}`)
        .then(() => dispatch(tokenValid()))
        .catch((err) => {
            dispatch(tokenInvalid(err.message))
            message.error(err.message, 5)
        })
}

export const fetchRegister = (registerModel) => (dispatch, getState) => {
    if (config.tokensEnabled) {
        const token = getState().register.token;
        registerModel.token = token.token;
    } else {
        registerModel.token = "disabled";
    }

    dispatch(register())
    post(`/api/auth/register`, registerModel)
        .then((json) => {
            dispatch(registered())
            dispatch(signedIn(json))
        })
        .catch((err) => {
            dispatch(registerFailed(err.message))
            message.error(err.message, 6)
        })
}

export const finishRegistration = () => (dispatch)  => {
    const hide = message.loading('Signing in', 0)
    setTimeout(() => {
        hide()
        dispatch(push('/accounts'))
        dispatch(clearRegistration())
    }, 3000)
}
