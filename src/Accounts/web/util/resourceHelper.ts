import { Dispatch } from 'redux';

import { IResource, Relation, RelationKey, ResourceId } from '../resource';
import { IBaseLoadState, IResourceState } from '../resource.reducer';
import { Option } from './index';

const getIds = <T extends IResource>(
  resources: T[],
  paged?: ResourceId[],
  total?: number
) => (start: number, items: number): Option<ResourceId[]> => {
  if (!paged || !resources || total === undefined) {
    return undefined;
  }

  if (start + items > total) {
    items = total - start;
  }

  if (paged.length < start + items) {
    return undefined;
  }

  const ids = paged.slice(start, start + items).filter(() => true);
  if (ids.length < items || ids.some((id) => id === undefined || id === null)) {
    return undefined;
  }

  return ids;
};

const hasLoaded = <T extends IResource>(
  resources: T[],
  paged?: ResourceId[],
  total?: number
) => (start: number, items: number): boolean =>
    Option.map(
      getIds(resources, paged, total)(start, items),
      (ids) => ids.every((id) => resources.some((r) => r.id === id))
    ) || false;

const getPage = <T extends IResource>(
  resources: T[],
  paged?: ResourceId[],
  total?: number
) => (start: number, items: number): Option<T[]> =>
    Option.map(
      getIds(resources, paged, total)(start, items),
      (ids) => ids.map((id) => resources.find((r) => r.id === id) as T)
    );

const getParent = <T extends IResource>(state: IResourceState<T>, relation?: Relation): Option<IBaseLoadState> => {
  return relation ? state.load.relations[relation.key][relation.value] : state.load;
};

export const createResourceHelper = <T extends IResource>(state: IResourceState<T>) =>
  (relation?: Relation) => ({
    list: (start: number, numItems: number): Option<T[]> => {
      const parent = getParent(state, relation);
      if (!parent) {
        return undefined;
      }

      return getPage(state.resources, parent.paged, parent.total)(start, numItems);
    },
    listAll: (): Option<T[]> => {
      const parent = getParent(state, relation);
      if (!parent || !parent.total) {
        return undefined;
      }

      return getPage(state.resources, parent.paged, parent.total)(0, parent.total);
    },
    loaded: (start: number, numItems: number): boolean => {
      const parent = getParent(state, relation);
      return parent
        ? hasLoaded(state.resources, parent.paged, parent.total)(start, numItems)
        : false;
    },
    loadedAll: (): boolean => {
      const parent = getParent(state, relation);
      return parent !== undefined && parent.total !== undefined;
    },
    isLoading: (): boolean => {
      const parent = getParent(state, relation);
      return !parent || parent.fetching;
    },
    total: (): Option<number> => {
      const parent = getParent(state, relation);
      return parent && parent.total;
    }
  });
