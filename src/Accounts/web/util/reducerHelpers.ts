import { Action } from 'redux';

export interface IAsyncState {
  fetching: boolean;
  error?: string;
}

export interface IAsyncItem<T> extends IAsyncState {
  item?: T;
}

export const DEFAULT_ASYNC_STATE: IAsyncState = {
  fetching: false
};

export interface IStringErrorAction extends Action {
  error: string;
}

export const asyncInitState: IAsyncState = {
  fetching: false,
  error: undefined
};

export const asyncLoadedState = <T>(item: T): IAsyncItem<T> => ({
  fetching: false,
  error: undefined,
  item
});

export const asyncInProgress = (state: IAsyncState): IAsyncState => ({
  fetching: true,
  error: state.error,
});

export const asyncError = (error: string): IAsyncState => ({
  fetching: false,
  error,
});
