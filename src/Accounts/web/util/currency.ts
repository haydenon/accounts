const accounting = require('accounting');

export const currencyPrecision = (curr: string) => {
  switch (curr) {
    case 'BIF':
    case 'BYR':
    case 'CLP':
    case 'DJF':
    case 'GNF':
    case 'ILS':
    case 'JPY':
    case 'KMF':
    case 'KRW':
    case 'MGA':
    case 'PYG':
    case 'RWF':
    case 'UGX':
    case 'VND':
    case 'VUV':
    case 'XAF':
    case 'XOF':
    case 'XPF':
      return 0;
    case 'BHD':
    case 'IQD':
    case 'JOD':
    case 'KWD':
    case 'OMR':
    case 'TND':
      return 3;
    default:
      return 2;
  }
};

const currencySymbol = (curr: string) => {
  switch (curr) {
    case 'AED':
      return 'د.إ';
    case 'ALL':
      return 'Lek';
    case 'AFN':
      return '؋';
    case 'AWG':
    case 'ANG':
      return 'ƒ';
    case 'AZN':
      return '₼';
    case 'BYN':
      return 'Br';
    case 'BAM':
      return 'KM';
    case 'BWP':
      return 'P';
    case 'BGN':
    case 'KZT':
    case 'KGS':
    case 'UZS':
      return 'лв';
    case 'KHR':
      return '៛';
    case 'CRC':
      return '₡';
    case 'HRK':
      return 'kn';
    case 'CUP':
      return '₱';
    case 'CZK':
      return 'Kč';
    case 'DKK':
    case 'NOK':
    case 'ISK':
    case 'SEK':
      return 'kr';
    case 'EGP':
    case 'FKP':
    case 'GIP':
    case 'GGP':
    case 'IMP':
    case 'JEP':
    case 'LBP':
    case 'SHP':
    case 'SYP':
    case 'GBP':
      return '£';
    case 'EUR':
      return '€';
    case 'GHS':
      return '¢';
    case 'GTQ':
      return 'Q';
    case 'HNL':
      return 'L';
    case 'HUF':
      return 'Ft';
    case 'IDR':
      return 'Rp';
    case 'IRR':
    case 'OMR':
    case 'QAR':
    case 'SAR':
    case 'YER':
      return '﷼';
    case 'ILS':
      return '₪';
    case 'KPW':
    case 'KRW':
      return '₩';
    case 'LAK':
      return '₭';
    case 'MKD':
      return 'ден';
    case 'MYR':
      return 'RM';
    case 'INR':
      return '₹';
    case 'TRY':
      return '₺';
    case 'MUR':
    case 'NPR':
    case 'PKR':
    case 'SCR':
    case 'LKR':
      return '₨';
    case 'MNT':
      return '₮';
    case 'MZN':
      return 'MT';
    case 'NGN':
      return '₦';
    case 'PAB':
      return 'B/.';
    case 'PYG':
      return 'Gs';
    case 'PEN':
      return 'S/.';
    case 'PHP':
      return '₱';
    case 'PLN':
      return 'zł';
    case 'RON':
      return 'lei';
    case 'RUB':
      return '₽';
    case 'RSD':
      return 'Дин.';
    case 'SOS':
      return 'S';
    case 'ZAR':
      return 'R';
    case 'CHF':
      return 'CHF';
    case 'THB':
      return '฿';
    case 'UAH':
      return '₴';
    case 'VEF':
      return 'Bs';
    case 'VND':
      return '₫';
    case 'ARS':
    case 'AUD':
    case 'BSD':
    case 'BBD':
    case 'BMD':
    case 'BND':
    case 'CAD':
    case 'KYD':
    case 'CLP':
    case 'COP':
    case 'XCD':
    case 'SVC':
    case 'FJD':
    case 'GYD':
    case 'HKD':
    case 'LRD':
    case 'MXN':
    case 'NAD':
    case 'NZD':
    case 'SGD':
    case 'SBD':
    case 'SRD':
    case 'TVD':
    case 'USD':
      return '$';
    case 'BZD':
      return 'BZ$';
    case 'BOB':
      return '$b';
    case 'BRL':
      return 'R$';
    case 'DOP':
      return 'RD$';
    case 'JMD':
      return 'J$';
    case 'NIO':
      return 'C$';
    case 'TWD':
      return 'NT$';
    case 'TTD':
      return 'TT$';
    case 'UYU':
      return '$U';
    case 'ZWD':
      return 'Z$';
    case 'CNY':
    case 'JPY':
      return '¥';
    default:
      return undefined;
  }
};

const format = accounting.formatMoney;
const moneyFormat = (currency: string, amount: number) => {
  const precision = currencyPrecision(currency);
  const symbol = currencySymbol(currency);
  return symbol
    ? format(amount, { symbol, format: `%s%v ${currency}`, precision })
    : format(amount, { symbol: currency, format: '%v %s', precision });
};

export const formatMoney = moneyFormat;
