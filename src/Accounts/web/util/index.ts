import { WrappedFormUtils } from 'antd/lib/form/Form';
import ColorHash from 'color-hash';
import { FormEvent } from 'react';
const colorHash = new ColorHash();

const handleResponse = (response: any) => {
    if (response.status >= 400 && response.status < 600) {
        return response.json().then((err: any) => { throw new Error(err.errors[0]); });
    } else if (response.status === 204) {
        return Promise.resolve(null);
    }

    return response.json();
};

const postFetch = (url: string, body: any) =>
    fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin',
        body: body ? JSON.stringify(body) : null
    }).then(handleResponse);

const putFetch = (url: string, body: any) =>
    fetch(url, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin',
        body: body ? JSON.stringify(body) : null
    }).then(handleResponse);

const getFetch = (url: string) => fetch(url, {
    headers: {
        ['Accept']: 'application/json, text/plain, */*',
    },
    credentials: 'same-origin',
}).then(handleResponse);

const deleteFetch = (url: string) => fetch(url, {
    method: 'DELETE',
    headers: {
        ['Accept']: 'application/json, text/plain, */*',
    },
    credentials: 'same-origin',
}).then(handleResponse);

const errors = (form: WrappedFormUtils) => {
    const fieldErrors = form.getFieldsError();
    return Object.keys(fieldErrors).reduce((errs, key) =>
        errs.concat((fieldErrors as any)[key] || []), []);
};

const formSubmit = <T>(form: WrappedFormUtils, onSuccess: (values: T) => void) => {
    return (e: FormEvent<any>) => {
        e.preventDefault();
        form.validateFields();
        if (errors(form).length) {
            return;
        }
        if (onSuccess) {
            onSuccess(form.getFieldsValue() as T);
        }
    };
};

type Size = 'large' | 'small' | 'default' | undefined;

const avatar = (name: string, size: Size): { color: string; text: string; } => {
    name = name || 'Loading';
    const color = colorHash.hex(name);
    const text = name
        .split(' ')
        .filter((str) => str.trim())
        .map((str) => str.trim()[0])
        .join('').toUpperCase();
    return {
        color,
        text
    };
};

export type Option<T> = T | undefined;

export const Option = {
    map: <T, R>(value: T | undefined, func: (value: T) => R): R | undefined =>
        value !== undefined ? func(value) : undefined,
    match: <T, R>(value: T | undefined, ifSome: (value: T) => R, ifNone: () => R): R =>
        value ? ifSome(value) : ifNone(),
};

export const post = postFetch;
export const get = getFetch;
export const del = deleteFetch;
export const put = putFetch;

export const formErrors = errors;
export const validateSubmit = formSubmit;

export const avatarDetails = avatar;

export { currencyPrecision, formatMoney } from './currency';

export const sort = <T>(arr: T[], comparison?: (t1: T, t2: T) => number): T[] =>
    new Array<T>().concat(arr).sort(comparison);

export const groupBy = <T, TProp>(arr: T[], property: (t: T, index: number) => TProp) => {
    const distinct = Array.from(new Set<TProp>(arr.map(property)));
    return distinct.map((prop) => {
        const equalValues = arr.filter((item, ind) => property(item, ind) === prop);
        return {
            items: equalValues,
            property: prop,
            count: equalValues.length
        };
    });
};

export const sumBy = <T>(arr: T[], property: (t: T, index: number) => number): number =>
    arr.reduce((acc, next, ind) => property(next, ind), 0);

export const min = (arr: number[]): number => Math.min(...arr);
export const max = (arr: number[]): number => Math.max(...arr);
export const minBy = <T>(arr: T[], prop: (t: T) => number): T =>
    arr.reduce((low, next) => prop(low) <= prop(next) ? low : next);
export const maxBy = <T>(arr: T[], prop: (t: T) => number): T =>
    arr.reduce((high, next) => prop(high) >= prop(next) ? high : next);
