const { fx } = require('money');

import { currencyPrecision } from './currency';

const round = (num: number, decimals: number) => {
    return Number((Math.round(+(num + 'e' + decimals)) + 'e-' + decimals));
};

export function exchange(sourceCurrency: string, targetCurrency: string, amount: number, data: any) {
    fx.rates = data.rates;
    fx.base = data.base;
    return round(fx(amount).from(sourceCurrency).to(targetCurrency), currencyPrecision(targetCurrency));
}

export function exchangeWithRate(targetCurrency: string, amount: number, rate: number) {
    return round(amount * rate, currencyPrecision(targetCurrency));
}
