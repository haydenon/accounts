import { IAuthState } from '../auth/auth.reducer';
import { IRatesState } from '../rates/rates.reducer';
import { ISettingsState, ITimezone } from '../settings/settings.reducer';

const commonCurrencies = (auth: IAuthState) =>
  (exceptions: string[]) => {
    if (!auth.user) {
      return [];
    }

    return exceptions
      ? auth.user.commonCurrencies
        .filter((cc) => exceptions.findIndex((e) => e.toLowerCase() === cc.toLowerCase()) === -1)
      : auth.user.commonCurrencies;
  };

const nonCommonCurrencies = (rates: IRatesState, auth: IAuthState) =>
  (exceptions?: string[]) => {
    if (!auth.user && !exceptions) {
      return rates.names;
    }
    const except = (exceptions || []).concat(auth.user ? auth.user.commonCurrencies : []);

    return rates.names
      .filter((c) => except.findIndex((e) => e.toLowerCase() === c.toLowerCase()) === -1);
  };

const allCurrencies = (rates: IRatesState) =>
  (exceptions?: string[]) =>
    exceptions
      ? rates.names.filter((c) => exceptions.findIndex((e) => e.toLowerCase() === c.toLowerCase()) === -1)
      : rates.names;

export const createCurrencyHelper = (rates: IRatesState, auth: IAuthState) => ({
  commonCurrencies: commonCurrencies(auth),
  nonCommonCurrencies: nonCommonCurrencies(rates, auth),
  currencies: allCurrencies(rates),
});

const timezoneValues = (timezone: ITimezone) => ({
  display: `(UTC ${timezone.offset}) - ${timezone.display}`,
  value: timezone.display
});

const allTimezones = (settings: ISettingsState) =>
  () => {
    if (!settings.timezones.length)
      return [];

    const zones = settings.timezones
      .filter((tz) => !tz.display.startsWith('Etc'));
    const unique = Array.from(new Set(zones.map((tz) => tz.display)));
    return unique.map((display) => {
      const zone = zones.filter((z) => z.display === display)[0];
      return timezoneValues(zone);
    });
  };

const userTimezone = (settings: ISettingsState, auth?: IAuthState) =>
  () => {
    if (!settings.timezones.length)
      return { display: '', value: '' };

    if (auth && auth.user && auth.user.timezone) {
      const tzId = auth.user.timezone;
      const tzs = settings.timezones
        .filter((tz) => tz.timeZone.toLowerCase() === tzId.toLowerCase());
      if (tzs.length) {
        return timezoneValues(tzs[0]);
      }
    }

    const intl: any = (window as any).Intl;
    const userZone = intl && intl.DateTimeFormat
      ? intl.DateTimeFormat().resolvedOptions().timeZone
      : '';
    const zones = settings.timezones
      .filter((tz) => tz.timeZone.toLowerCase() === userZone.toLowerCase());
    return (zones.length && timezoneValues(zones[0])) || allTimezones(settings)()[0];
  };

export const createTimezoneHelper = (settings: ISettingsState, auth?: IAuthState) => ({
  timezones: allTimezones(settings),
  timezoneValues,
  userTimezone: userTimezone(settings, auth),
});
