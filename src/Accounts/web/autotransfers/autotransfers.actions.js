import { message } from 'antd';
import { actions } from '../resource.actions';
import { Relation } from '../resource';

export const autotransferActions = actions('autotransfer');

const getAccountId = (id) => id ? new Relation('accountId', id) : null;

const load = autotransferActions.loadResource(
    null,
    () => message.error('Failed to retrieve recurring transfers', 3),
    (config) => config.relation ? `/api/account/${config.relation.value}/autotransfer/` : `/api/autotransfer`)

export const loadAutoTransfers = (accountId = null) => load({
    relation: getAccountId(accountId)
});

export const createAutoTransfer = autotransferActions.createResource(
    () => message.success("Created new recurring transfer"),
    (err) => message.error(err.message, 6),
    () => `/api/autotransfer/`)

export const deleteAutoTransfer = autotransferActions.deleteResource(
    () => message.success("Successfully deleted recurring transfer"),
    (err) => message.error(err.message, 6),
    (t) => `/api/autotransfer/${t.id}/`);

export const updateAutoTransfer = autotransferActions.updateResource(
    () => message.success("Successfully updated recurring transfer"),
    (err) => message.error(err.message, 6),
    (t) => `/api/autotransfer/${t.id}/`);
