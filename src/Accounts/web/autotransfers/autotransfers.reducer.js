import { autotransferActions } from './autotransfers.actions';
import { reducer } from '../resource.reducer';

export const autoTransfers = reducer(autotransferActions, {
  relations: ['accountId'],
})
