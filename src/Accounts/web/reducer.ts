import { routerReducer, RouterState } from 'react-router-redux';
import { combineReducers } from 'redux';
import { accountShares } from './accounts/account-share.reducer';
import { accounts } from './accounts/accounts.reducer';
import { auth, IAuthState } from './auth/auth.reducer';
import { register } from './auth/register.reducer';
import { reset } from './auth/reset.reducer';
import { autoTransfers } from './autotransfers/autotransfers.reducer';
import { IRatesState, rates } from './rates/rates.reducer';
import { IScreenState, screenWidth } from './screen';
import { settings } from './settings/settings.reducer';
import { transfers } from './transfers/transfers.reducer';
import { ITransfersSummaryState, transfersSummaries } from './transfers/transfersSummary.reducer';

export interface IState {
    routing: RouterState;
    auth: IAuthState;
    register: any;
    rates: IRatesState;
    screenWidth: IScreenState;
    transfersSummaries: ITransfersSummaryState;
}

export const reducer = combineReducers({
    auth,
    register,
    settings: settings as any,
    reset,
    accounts: accounts as any,
    accountShares: accountShares as any,
    transfers: transfers as any,
    autoTransfers: autoTransfers as any,
    transfersSummaries: transfersSummaries as any,
    rates: rates as any,
    routing: routerReducer,
    screenWidth: screenWidth as any
});
