import React from 'react';
import PropTypes from 'prop-types';
import { AutoSizer, List as VList } from 'react-virtualized';
import { List, Spin } from 'antd';

class VirtualList extends React.Component {
  height = (h) => this.props.height && this.props.height(h) || h;

  width = (w) => this.props.width && this.props.width(w) || w;

  render() {
    return (<AutoSizer>
      {({ height, width }) =>
        <List style={{ width: "100%" }}>
          {!this.props.loading ? <VList
            height={this.height(height)}
            width={this.width(width)}
            overscanRowCount={2}
            rowCount={this.props.rowCount}
            rowHeight={this.props.rowHeight()}
            rowRenderer={({ index, key, style }) => this.props.rowRenderer({ index, key, style, width, height })}>
          </VList> : null}
          {this.props.loading ? <div style={{ width: this.width(width), height: this.height(height), position: 'relative' }}>
            <Spin size="large" style={{ position: 'absolute', bottom: '50%', left: '50%' }} />
          </div> : null}
        </List>}
    </AutoSizer>);
  }
}
VirtualList.propTypes = {
  loading: PropTypes.bool.isRequired,
  rowCount: PropTypes.number.isRequired,
  rowHeight: PropTypes.func.isRequired,
  rowRenderer: PropTypes.func.isRequired,
  height: PropTypes.func,
  width: PropTypes.func
}

export default VirtualList
