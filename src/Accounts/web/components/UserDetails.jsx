import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Avatar, Popover, Menu, Icon } from 'antd';
import { fetchSignOut } from '../auth/auth.actions';
import '../styles/userDetails.scss';
import { avatarDetails } from '../util/index';

const menu = (menuClick) => (
    <div className="userDetailsPopMenu">
        <Menu onClick={menuClick}>
            <Menu.Item key="signOut" className="menuItem">
                <Icon type="logout" className="icon" />
                <span className="text">Sign out</span>
            </Menu.Item>
        </Menu>
    </div>
);

class UserDetails extends React.Component {
    onMenuClick = (e) => {
        switch (e.key) {
            case "signOut":
                this.props.dispatch(fetchSignOut())
                return;
        }
    }

    render() {
        const { text, color } = avatarDetails(this.props.user.displayName);
        return (
            <div>
                <Popover content={menu(this.onMenuClick)}
                    trigger={['click']}
                    placement="bottomRight"
                    title={this.props.user.email}>
                    {<Avatar
                        className="accounts-avatar"
                        style={{ backgroundColor: color, verticalAlign: 'middle' }}
                        size={'large'}
                    >{text}</Avatar>}
                </Popover>
            </div>
        )
    }
}
UserDetails.propTypes = {
    user: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    user: state.auth.user
});

export default connect(mapStateToProps)(UserDetails);
