import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    Button,
    Divider,
    Popconfirm,
    Menu,
    Dropdown,
    Icon
} from 'antd'

import './actions.scss'

class Actions extends React.Component {
    button = (action, ind) => {
        if (action.popoverText) {
            return (
                <Popconfirm key={ind} title={action.popoverText} okText="Yes" cancelText="No" onConfirm={action.onChoose}>
                    <Button type={action.type} icon={action.icon}>{action.text}</Button>
                </Popconfirm>
            );
        } else {
            return <Button type={action.type} icon={action.icon} key={ind} onClick={action.onChoose}>{action.text}</Button>;
        }
    }

    menuItem = (action, ind) => {
        if (action.popoverText) {
            return (
                <Menu.Item key={ind}>
                    <Popconfirm key={ind} title={action.popoverText} okText="Yes" cancelText="No" onConfirm={action.onChoose}>
                        <a className={`sm-action action-${action.type}`}>
                            <Icon type={action.icon} />
                            {action.text}
                        </a>
                    </Popconfirm>
                </Menu.Item>
            );
        } else {
            return (
                <Menu.Item key={ind}>
                    <a className={`sm-action action-${action.type}`} onClick={action.onChoose}>
                        <Icon type={action.icon} />
                        {action.text}
                    </a>
                </Menu.Item>
            );
        }
    }

    render() {
        if (this.props.collapse(this.props.screenWidth.width)) return (
            <div className="actions-container">
                {this.props.actions.map((action, ind) => (
                    <span key={ind}>
                        {this.button(action)}
                        {ind < this.props.actions.length - 1 ? <Divider type="vertical" key={`divider${ind}`} /> : null}
                    </span>
                ))}
            </div>
        )

        let menu = (
            <Menu>
                {this.props.actions.map(this.menuItem)}
            </Menu>
        )

        return (
            <Dropdown key="actions" overlay={menu} trigger={['click']}>
                <Button icon="ellipsis" shape="circle"></Button>
            </Dropdown>
        );
    }
}
Actions.propTypes = {
    actions: PropTypes.array.isRequired,
    screenWidth: PropTypes.object.isRequired,
    collapse: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    screenWidth: state.screenWidth
});

export default connect(mapStateToProps)(Actions);
