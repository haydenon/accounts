import moment, { Moment } from 'moment';
import {
    RATE_LOAD,
    RATE_LOAD_FAILED,
    RATE_LOADED,
} from './rates.actions';

export interface IRates {
    base: string;
    rates: {
        [curr: string]: number;
    };
}

export interface IRatesState {
    data?: IRates;
    names: string[];
    isFetching: boolean;
    expiry: Moment;
    ratesFailed: boolean;
}

function getNames(data: IRates) {
    return Object.keys(data.rates);
}

const ORIGINAL_STATE: IRatesState = {
    names: [],
    isFetching: false,
    expiry: moment(),
    ratesFailed: false,
};

export function rates(state = ORIGINAL_STATE, action: any) {
    switch (action.type) {
        case RATE_LOAD:
            return {
                ...state,
                isFetching: true,
                data: null,
                ratesFailed: false,
            };
        case RATE_LOADED:
            return {
                ...state,
                data: action.rates,
                names: getNames(action.rates),
                expiry: action.expiry,
                isFetching: false,
            };
        case RATE_LOAD_FAILED:
            return {
                ...state,
                isFetching: false,
                ratesFailed: true,
            };
        default:
            return state;
    }
}
