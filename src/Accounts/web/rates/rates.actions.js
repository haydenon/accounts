import { get } from '../util';
import moment from 'moment';

export const RATE_LOAD = '[rates] Load';
export const RATE_LOADED = '[rates] Loaded';
export const RATE_LOAD_FAILED = '[rates] Load Failed';

export const rateLoad = () => ({
    type: RATE_LOAD
})

export const rateLoaded = (rates, expiry) => ({
    type: RATE_LOADED,
    rates,
    expiry
})

export const rateLoadFailed = () => ({
    type: RATE_LOAD_FAILED
})

export const loadRates = () => dispatch => {
    dispatch(rateLoad())
    get(`/api/rate/`)
        .then((rates) => {
            for (let curr in rates.rates) {
                const upper = curr.toUpperCase()
                if (curr !== upper) {
                    rates.rates[upper] = rates.rates[curr]
                    delete rates.rates[curr]
                }
            }
            dispatch(rateLoaded(rates, moment().add(1, 'h')))
        })
        .catch(() => {
            dispatch(rateLoadFailed())
        })
}
