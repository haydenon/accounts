import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Switch } from 'react-router'
import { Redirect } from 'react-router-dom'
import AppRoute from './AppRoute';
import Accounts from './accounts/Accounts';
import Dashboard from './dashboard/Dashboard';
import AccountDetails from './accounts/AccountDetails';
import SignIn from './auth/SignIn';
import Register from './auth/Register';
import ForgotPassword from './auth/ForgotPassword';
import ResetPassword from './auth/ResetPassword';
import Settings from './settings/Settings';
import './styles/index.scss';

const dashboardPadding = {
  small: "12px 0px 12px 12px",
  medium: "16px 0px 16px 16px",
}

const Routes = (props) => (
  <ConnectedRouter history={props.history}>
    <Switch>
      <AppRoute path="/settings" component={Settings} />
      <AppRoute background path="/accounts/:id" component={AccountDetails} />
      <AppRoute path="/accounts" component={Accounts} />
      <AppRoute background padding={dashboardPadding} path="/dashboard" component={Dashboard} />
      <Route path="/signin" component={SignIn} />
      <Route path="/register" component={Register} />
      <Route path="/forgotPassword" component={ForgotPassword} />
      <Route path="/resetPassword" component={ResetPassword} />
      <Redirect from="/" to="/accounts" />
    </Switch>
  </ConnectedRouter>
);
Routes.propTypes = {
  history: PropTypes.object.isRequired,
  configuration: PropTypes.object
}

const mapStateToProps = (state) => ({
  configuration: state.settings.configuration
})

export default connect(mapStateToProps)(Routes);
