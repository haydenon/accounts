import { get, post, del, put } from './util';

const LOAD = (name) => `[${name}] Load`
const LOADED = (name) => `[${name}] Loaded`
const LOAD_FAILED = (name) => `[${name}] Load failed`

const CREATE = (name) => `[${name}] Create`
const CREATED = (name) => `[${name}] Created`
const CREATE_FAILED = (name) => `[${name}] Create failed`

const DELETE = (name) => `[${name}] Delete`
const DELETED = (name) => `[${name}] Deleted`
const DELETE_FAILED = (name) => `[${name}] Delete failed`

const UPDATE = (name) => `[${name}] Update`
const UPDATED = (name) => `[${name}] Updated`
const UPDATE_FAILED = (name) => `[${name}] Update failed`

const withConfig = (action, config) => {
    action.config = config || {};
    return action;
}

const resourceLoad = (name) => (config) =>
    withConfig({ type: LOAD(name) }, config);

const resourceLoaded = (name) => (resources, total, config) =>
    withConfig({ type: LOADED(name), resources, total }, config);

const resourceLoadFailed = (name) => (error, config) =>
    withConfig({ type: LOAD_FAILED(name), error }, config);

const resourceCreate = (name) => () => ({
    type: CREATE(name)
})

const resourceCreated = (name) => (resource) => ({
    type: CREATED(name),
    resource
})

const resourceCreateFailed = (name) => (error) => ({
    type: CREATE_FAILED(name),
    error
})

const resourceDelete = (name) => (resource) => ({
    type: DELETE(name),
    resource
})

const resourceDeleted = (name) => (resource) => ({
    type: DELETED(name),
    resource
})

const resourceDeleteFailed = (name) => (resource, error) => ({
    type: DELETE_FAILED(name),
    resource,
    error
})

const resourceUpdate = (name) => (original, updated) => ({
    type: UPDATE(name),
    original,
    updated
})

const resourceUpdated = (name) => (original, updated) => ({
    type: UPDATED(name),
    original,
    updated
})

const resourceUpdateFailed = (name) => (original, updated, error) => ({
    type: UPDATE_FAILED(name),
    original,
    updated,
    error
})

const loadResource = (name) => (success, error, getUrl) => (config) => (dispatch) => {
    dispatch(resourceLoad(name)(config))
    const url = (getUrl && getUrl(config)) || `/api/${name}/`;
    return get(url)
        .then((resp) => {
            const resources = resp instanceof Array
                ? resp
                : resp.resources;
            const total = resp instanceof Array
                ? null
                : resp.total;

            dispatch(resourceLoaded(name)(resources, total, config));
            success && success(resources, dispatch);
        })
        .catch((err) => {
            dispatch(resourceLoadFailed(name)(err.message, config));
            error && error(err, dispatch);
        })
}

const createResource = (name) => (success, error, getUrl) => (resource) => (dispatch) => {
    dispatch(resourceCreate(name)())
    const url = (getUrl && getUrl()) || `/api/${name}/`;
    return post(url, resource)
        .then((newResource) => {
            dispatch(resourceCreated(name)(newResource));
            success && success(newResource, dispatch)
        })
        .catch((err) => {
            dispatch(resourceCreateFailed(name)(err.message))
            error && error(err, dispatch)
        })
}

const deleteResource = (name) => (success, error, getUrl) => (resource) => (dispatch) => {
    dispatch(resourceDelete(name)(resource))
    const url = (getUrl && getUrl(resource)) || `/api/${name}/${resource.id}/`;
    return del(url, resource)
        .then(() => {
            dispatch(resourceDeleted(name)(resource));
            success && success(dispatch)
        })
        .catch((err) => {
            dispatch(resourceDeleteFailed(name)(resource, err.message))
            error && error(err, dispatch)
        })
}

const updateResource = (name) => (success, error, getUrl) => (original, updated) => (dispatch) => {
    dispatch(resourceUpdate(name)(original, updated))
    const url = (getUrl && getUrl(original)) || `/api/${name}/${original.id}/`;
    return put(url, updated)
        .then((update) => {
            dispatch(resourceUpdated(name)(original, update));
            success && success(dispatch)
        })
        .catch((err) => {
            dispatch(resourceUpdateFailed(name)(original, updated, err.message))
            error && error(err, dispatch)
        })
}

export const actions = (name) => ({
    LOAD: LOAD(name),
    LOADED: LOADED(name),
    LOAD_FAILED: LOAD_FAILED(name),

    CREATE: CREATE(name),
    CREATED: CREATED(name),
    CREATE_FAILED: CREATE_FAILED(name),

    DELETE: DELETE(name),
    DELETED: DELETED(name),
    DELETE_FAILED: DELETE_FAILED(name),

    UPDATE: UPDATE(name),
    UPDATED: UPDATED(name),
    UPDATE_FAILED: UPDATE_FAILED(name),

    resourceLoad: resourceLoad(name),
    resourceLoaded: resourceLoaded(name),
    resourceLoadFailed: resourceLoadFailed(name),

    loadResource: loadResource(name),

    resourceCreate: resourceCreate(name),
    resourceCreated: resourceCreated(name),
    resourceCreateFailed: resourceCreateFailed(name),

    createResource: createResource(name),

    resourceDelete: resourceDelete(name),
    resourceDeleted: resourceDeleted(name),
    resourceDeleteFailed: resourceDeleteFailed(name),

    deleteResource: deleteResource(name),

    resourceUpdate: resourceUpdate(name),
    resourceUpdated: resourceUpdated(name),
    resourceUpdateFailed: resourceUpdateFailed(name),

    updateResource: updateResource(name),
})
