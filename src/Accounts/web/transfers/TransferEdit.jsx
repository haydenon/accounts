import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import {
    Modal,
    Form,
    InputNumber,
    Row,
    Col,
    DatePicker,
    Input,
    Button,
    Select,
    Switch,
    Radio,
    message
} from 'antd';
import moment from 'moment';

import { validateSubmit } from '../util/index';
import { updateTransfer } from './transfers.actions';
import { updateAutoTransfer } from '../autotransfers/autotransfers.actions';
import { loadRates } from '../rates/rates.actions';
import { sizes } from '../screen';
import { onFinish } from '../actions';

import './transferEdit.scss';
import { createCurrencyHelper } from '../util/stateHelpers';
import { updateTransferDetails } from './transfer.service';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const OptGroup = Select.OptGroup;
const FormItem = Form.Item;

class TransferEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = { income: false }
    }

    componentDidMount() {
        if (this.props.rates.expiry < moment()) {
            this.props.dispatch(loadRates());
        }
    }

    componentDidUpdate(prevProps) {
        const transfer = this.props.transfer;

        if (transfer === prevProps.transfer) {
            return;
        }

        if (!transfer) {
            this.setState({ visible: false });
            return;
        }

        const income = transfer.amount > 0
        this.setState({ income, visible: true });
        const currency = transfer.originalCurrency || transfer.currency || this.accountCurrency(transfer.accountId)
        this.props.form.setFieldsValue({
            originalCurrency: currency,
            date: moment(transfer.date),
            amount: Math.abs(transfer.originalAmount || transfer.amount),
            description: transfer.description,
            period: transfer.period,
            periodsPerOccurence: transfer.periodsPerOccurence
        });
    }

    handleRecurringUpdate = (transfer) => {
        const original = this.props.transfer;
        const updated = { ...original }

        updated.amount = +transfer.amount;
        updated.description = transfer.description

        if (!this.state.income) {
            updated.amount = -updated.amount;
        }

        updated.currency = transfer.originalCurrency
        updated.period = transfer.period;
        updated.periodsPerOccurence = transfer.periodsPerOccurence;

        return updated;
    }

    handleUpdate = (transfer) => {
        const original = this.props.transfer;
        const recurring = this.props.recurring;
        const updated = recurring
            ? this.handleRecurringUpdate(transfer)
            : updateTransferDetails(this.props.rates.data)(original, transfer, this.state.income)

        const action = recurring
            ? updateAutoTransfer(original, updated)
            : onFinish(
                updateTransfer(original, updated),
                () => message.success("Successfully updated transfer"),
                (err) => message.error(err.message, 6)
            )
        this.props.dispatch(action)
            .then(() => {
                const updateStatus = recurring
                    ? this.props.transfers.update[updated.id]
                    : this.props.autoTransfers.update[updated.id]
                const failed = updateStatus && updateStatus.error;
                if (!failed) {
                    this.props.handleCancel();
                }
            });
    }

    handleSubmit = () => validateSubmit(this.props.form, this.handleUpdate)({ preventDefault: () => { } });

    accountCurrency = () => {
        if (!this.props.transfer) { return ''; }

        return this.props.accounts.resources
            .filter(acc => acc.id === this.props.transfer.accountId)[0].currency;
    }

    positive = (rule, value, callback) => {
        if (!value || +value > 0)
            callback();
        else
            callback(true)
    }

    periodName = () => {
        const name = this.props.form.getFieldValue('period').toLowerCase();
        const single =
            +this.props.form.getFieldValue('periodsPerOccurence') === 1
        return name + (single ? '' : 's');
    }

    recurringRow = () => {
        const { getFieldDecorator } = this.props.form;
        return !this.props.recurring
            ? null
            :
            (<Row type="flex" justify="start" gutter={32}>
                <Col>
                    <FormItem>
                        {getFieldDecorator('period', {
                            initialValue: 'Day'
                        })(
                            <RadioGroup size={this.props.screenWidth.width >= 450 ? 'default' : 'small'}>
                                <RadioButton value="Day">Day</RadioButton>
                                <RadioButton value="Week">Week</RadioButton>
                                <RadioButton value="Month">Month</RadioButton>
                                <RadioButton value="Year">Year</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                </Col>
                <Col xl={8}>
                    <FormItem className="">
                        Every
                        {getFieldDecorator('periodsPerOccurence', {
                            initialValue: 1,
                            rules: [
                                { required: true, message: 'Please enter an amount' },
                                { message: 'Value must be positive', validator: this.positive }
                            ],
                        })(
                            <InputNumber className="recurring-num-periods" />
                        )}
                        {this.periodName()}
                    </FormItem>
                </Col>
            </Row>)
    }

    positiveAmount = (rule, value, callback) => {
        if (+value < 1) callback(true);
        else callback()
    }

    transferId = () => this.props.transfer && this.props.transfer.id || 0;

    modalWidth = () => {
        const width = this.props.screenWidth.width;
        return width * 0.80 >= sizes.XLARGE ? sizes.XLARGE : '85%'
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        let fetching = false;
        if (this.transferId()) {
            const updateStatus = this.props.recurring
                ? this.props.autoTransfers.update[this.props.transfer.id]
                : this.props.transfers.update[this.props.transfer.id];
            fetching = !!(updateStatus && updateStatus.fetching)
        }

        const common = this.currencyHelper.commonCurrencies([this.accountCurrency()]);
        const nonCommon = this.currencyHelper.nonCommonCurrencies([this.accountCurrency()]);
        const accountCurr = this.accountCurrency();
        return (
            <Modal
                className="transfer-edit-modal"
                width={this.modalWidth()}
                visible={this.state.visible}
                title={`Edit ${this.props.recurring && 'recurring' || ''} transfer`}
                onOk={this.handleSubmit}
                onCancel={this.props.handleCancel}
                footer={[
                    <Button key="back" onClick={this.props.handleCancel}>Cancel</Button>,
                    <Button key="submit" loading={fetching} type="primary" icon="check" onClick={this.handleSubmit}>Update</Button>
                ]}>
                <Form onSubmit={this.handleSubmit} className="transfer-create-form">
                    <Row>
                        <Col>
                            <div className="transfer-switch">
                                <label>
                                    <span className="transfer-switch-label">
                                        {this.state.income ? 'Income' : 'Expense'}
                                    </span>
                                    <Switch className="transfer-expense-toggle"
                                        checked={this.state.income}
                                        checkedChildren="+"
                                        unCheckedChildren="−"
                                        onChange={(checked) => this.setState({ income: checked })} />
                                </label>
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={this.props.screenWidth.width >= sizes.XLARGE ? 32 : 16}>
                        {this.props.recurring ? null : (
                            <Col sm={24} md={6} lg={6} xl={5}>
                                <FormItem>
                                    {getFieldDecorator('date', {
                                        initialValue: moment().startOf('day'),
                                        rules: [
                                            { type: 'object', required: true, message: 'Please enter a date' },
                                        ]
                                    })(
                                        <DatePicker format="YYYY/MM/DD" placeholder="Date" className="required" />
                                    )}
                                </FormItem>
                            </Col>)}
                        <Col sm={24} md={5} lg={4}>
                            <FormItem>
                                {getFieldDecorator('originalCurrency', {
                                    validateTrigger: 'onBlur',
                                    initialValue: accountCurr
                                })(
                                    <Select>
                                        <OptGroup label="Account">
                                            <Option key={accountCurr} value={accountCurr}>{accountCurr}</Option>
                                        </OptGroup>
                                        {common.length ? (
                                            <OptGroup label="Common">
                                                {common.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                            </OptGroup>
                                        ) : null}
                                        <OptGroup label="Other">
                                            {nonCommon.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                        </OptGroup>
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col sm={24} md={5} lg={5} xl={5}>
                            <FormItem>
                                {getFieldDecorator('amount', {
                                    validateTrigger: 'onBlur',
                                    rules: [
                                        { required: true, message: 'Please enter an amount' },
                                        { message: 'Value must be positive', validator: this.positiveAmount }
                                    ],
                                })(
                                    <InputNumber className="required"
                                        placeholder="Amount" />
                                )}
                            </FormItem>
                        </Col>
                        <Col sm={24} md={6} lg={7} xl={10}>
                            <FormItem>
                                {getFieldDecorator('description', {})(
                                    <Input placeholder="Description" />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                    {this.recurringRow()}
                    {this.props.recurring ? <i>Updating this recurring transfer will not update any of the previous transfers created by it.</i> : null}
                </Form>
            </Modal>
        )
    }

    get currencyHelper() { return createCurrencyHelper(this.props.rates, this.props.auth); }
}
TransferEdit.propTypes = {
    transfer: PropTypes.object,
    recurring: PropTypes.bool,
    transfers: PropTypes.object.isRequired,
    autoTransfers: PropTypes.object.isRequired,
    rates: PropTypes.object.isRequired,
    accounts: PropTypes.object.isRequired,
    screenWidth: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    handleCancel: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    accounts: state.accounts,
    auth: state.auth,
    rates: state.rates,
    transfers: state.transfers,
    autoTransfers: state.autoTransfers,
    screenWidth: state.screenWidth
})

export default connect(mapStateToProps)(Form.create()(TransferEdit));
