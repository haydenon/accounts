import { ITransfer } from './transfer';
import { updateTransferDetails } from './transfer.service';

const ANY_CURRENCY = 'BSE';
const OTHER_CURRENCY_1 = 'OTH';
const OTHER_CURRENCY_2 = 'CUR';

const rates = {
    base: ANY_CURRENCY,
    rates: {
        [ANY_CURRENCY]: 1,
        [OTHER_CURRENCY_1]: 2,
        [OTHER_CURRENCY_2]: 3
    }
};

const transfer = (
    amount: number,
    currency: string,
    originalAmount?: number,
    originalCurrency?: string
): ITransfer => ({
    amount,
    currency,
    originalAmount,
    originalCurrency
}) as any;

describe('updateTransferDetails', () => {
    const createTestCase = (
        label: string
    ) => (
        amount: number,
        currency: string,
        originalAmount?: number,
        originalCurrency?: string
    ) => (
        updateAmount: number,
        updateCurrency: string
    ) => (
        expectedAmount: number,
        expectedOriginalCurrency?: string,
        expectedOriginalAmount?: number,
                ) => ({
                    label,
                    amount,
                    currency,
                    originalAmount,
                    originalCurrency,
                    updateAmount,
                    updateCurrency,
                    expectedAmount,
                    expectedOriginalAmount,
                    expectedOriginalCurrency,
                });

    const runTest = (testCase: any) => test(testCase.label, () => {
        // Arrange
        const original = transfer(
            testCase.amount,
            testCase.currency,
            testCase.originalAmount,
            testCase.originalCurrency);

        const updates = transfer(
            testCase.updateAmount,
            testCase.currency,
            testCase.originalAmount,
            testCase.updateCurrency);

        // Act
        const result = updateTransferDetails(rates)(original, updates, true);

        // Assert
        expect(result.amount).toBe(testCase.expectedAmount);
        expect(result.currency).toBe(testCase.currency);
        expect(result.originalAmount).toBe(testCase.expectedOriginalAmount);
        expect(result.originalCurrency).toBe(testCase.expectedOriginalCurrency);
    });

    describe('with no pre-existing currency change', () => {
        [
            createTestCase('doesn\'t include original currency/amount when currency is target currency')
                (1, ANY_CURRENCY)(2, ANY_CURRENCY)(2),
            createTestCase('uses exchange rate when changing to other currency')
                (1, ANY_CURRENCY)(2, OTHER_CURRENCY_1)(1, OTHER_CURRENCY_1, 2),
        ].forEach(runTest);
    });

    describe('with pre-existing currency change', () => {
        [
            createTestCase('uses same rate when currency is original currency')
                (1, ANY_CURRENCY, 56, OTHER_CURRENCY_1)(112, OTHER_CURRENCY_1)(2, OTHER_CURRENCY_1, 112),
            createTestCase('uses exchange rate when currency isn\'t existing original currency')
                (1, ANY_CURRENCY, 50, OTHER_CURRENCY_2)(2, OTHER_CURRENCY_1)(1, OTHER_CURRENCY_1, 2),
            createTestCase('doesn\'t include original currency/amount when currency is target currency')
                (1, ANY_CURRENCY, 50, OTHER_CURRENCY_2)(2000, ANY_CURRENCY)(2000),

        ].forEach(runTest);
    });
});
