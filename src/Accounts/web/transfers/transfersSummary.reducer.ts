import { asyncError, asyncInProgress, asyncLoadedState, DEFAULT_ASYNC_STATE, IAsyncItem } from '../util/reducerHelpers';
import { ITransfersSummary } from './transfersSummary';
import {
  TransfersSummaryAction,
  TransfersSummaryLoadError,
  TransfersSummaryLoadStart,
  TransfersSummaryLoadSuccess
} from './transfersSummary.actions';

export interface ITransfersSummaryState {
  load: IAsyncItem<ITransfersSummary[]>;
}

const originalState: ITransfersSummaryState = {
  load: DEFAULT_ASYNC_STATE,
};

export const transfersSummaries = (state = originalState, action: TransfersSummaryAction): ITransfersSummaryState => {
  switch (action.type) {
    case TransfersSummaryLoadStart:
      return {
        ...state,
        load: asyncInProgress(state.load)
      };
    case TransfersSummaryLoadSuccess:
      return {
        ...state,
        load: asyncLoadedState(action.data),
      };
    case TransfersSummaryLoadError:
      return {
        ...state,
        load: asyncError(action.error.message)
      };
    default:
      const _exhaustiveCheck: never = action;
      return state;
  }
};
