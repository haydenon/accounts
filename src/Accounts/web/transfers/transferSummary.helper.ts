import { Moment } from 'moment';
import { Dispatch } from 'redux';

import { ISummaryRequestParameters, loadTransfersSummary } from './transfersSummary.actions';
import { ITransfersSummaryState, transfersSummaries } from './transfersSummary.reducer';

export type SummaryPeriod = 'Day' | 'Week' | 'Month';

const loadTransferSummaries = (dispatch: Dispatch<any>) =>
    (start: Moment, end: Moment, period: SummaryPeriod) => {
        const params: ISummaryRequestParameters = {
            startDate: start.format('YYYY-MM-DD'),
            endDate: end.format('YYYY-MM-DD'),
            period
        };

        dispatch(loadTransfersSummary(params));
    };

const transfersSummaryList = (state: ITransfersSummaryState) => () => {
    return state.load;
};

export const createTransferSummaryHelper = (state: ITransfersSummaryState, dispatch: Dispatch<any>) => ({
    loadTransferSummaries: loadTransferSummaries(dispatch),
    transferSummaries: transfersSummaryList(state)
});
