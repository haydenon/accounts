export interface ITransfersSummary {
  accountId: number;
  startDate: string;
  endDate: string;
  amount: number;
  startBalance: number;
}
