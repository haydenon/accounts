import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import {
    Form,
    InputNumber,
    Row,
    Col,
    DatePicker,
    Input,
    Button,
    Select,
    Switch,
    Radio,
    message
} from 'antd';
import moment from 'moment';

import './transferCreate.scss';
import { validateSubmit } from '../util/index';
import { createTransfer } from './transfers.actions';
import { createAutoTransfer } from '../autotransfers/autotransfers.actions';
import { loadRates } from '../rates/rates.actions';
import { exchange } from '../util/exchange';
import { sizes } from '../screen';
import { createCurrencyHelper } from '../util/stateHelpers';
import { onFinish } from '../actions';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;
const Option = Select.Option;
const OptGroup = Select.OptGroup;
const FormItem = Form.Item;

class TransferCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = { recurring: false }
    }

    componentDidMount() {
        if (this.props.rates.expiry < moment()) {
            this.props.dispatch(loadRates());
        }
    }

    handleSubmit = validateSubmit(this.props.form, (transfer) => {
        transfer.accountId = this.props.accountId;
        transfer.date = transfer.date.format('YYYY-MM-DD')

        if (!this.state.income) {
            transfer.amount = -transfer.amount;
        }

        const recurring = this.state.recurring;
        if (recurring) {
            transfer.currency = transfer.originalCurrency
            delete transfer.originalCurrency;
        } else {
            if (transfer.originalCurrency === this.accountCurrency()) {
                delete transfer.originalCurrency;
            } else {
                transfer.originalAmount = transfer.amount;
                transfer.amount = exchange(
                    transfer.originalCurrency,
                    this.accountCurrency(),
                    transfer.amount,
                    this.props.rates.data)
            }
        }
        transfer.amount = +transfer.amount;
        const action = recurring
            ? createAutoTransfer(transfer)
            : onFinish(
                createTransfer(transfer),
                () => message.success("Created new transfer"),
                (err) => message.error(err.message, 6)
            );
        this.props.dispatch(action)
            .then(() => {
                const failed = recurring ? this.props.transfers.create.error : this.props.autoTransfers.create.error;
                if (!failed) {
                    this.props.form.setFieldsValue({
                        originalCurrency: this.accountCurrency(),
                        date: moment().startOf('day'),
                        amount: '',
                        description: '',
                        period: 'Day',
                        periodsPerOccurence: 1
                    });

                    if (this.props.onCreate) {
                        this.props.onCreate(transfer);
                    }
                }
            });
    });

    accountCurrency = () => this.props.accounts.resources
        .filter(acc => acc.id === this.props.accountId)[0].currency

    positive = (rule, value, callback) => {
        if (!value || +value > 0)
            callback();
        else
            callback(true)
    }

    periodName = () => {
        const name = this.props.form.getFieldValue('period').toLowerCase();
        const single =
            +this.props.form.getFieldValue('periodsPerOccurence') === 1
        return name + (single ? '' : 's');
    }

    recurringRow = () => {
        const { getFieldDecorator } = this.props.form;
        return !this.state.recurring
            ? null
            :
            (<Row type="flex" justify="start" gutter={32}>
                <Col>
                    <FormItem>
                        {getFieldDecorator('period', {
                            initialValue: 'Day'
                        })(
                            <RadioGroup size={this.props.screenWidth.width >= 450 ? 'default' : 'small'}>
                                <RadioButton value="Day">Day</RadioButton>
                                <RadioButton value="Week">Week</RadioButton>
                                <RadioButton value="Month">Month</RadioButton>
                                <RadioButton value="Year">Year</RadioButton>
                            </RadioGroup>
                        )}
                    </FormItem>
                </Col>
                <Col xl={8}>
                    <FormItem className="">
                        Every
                        {getFieldDecorator('periodsPerOccurence', {
                            initialValue: 1,
                            rules: [
                                { required: true, message: 'Please enter an amount' },
                                { message: 'Value must be positive', validator: this.positive }
                            ],
                        })(
                            <InputNumber className="recurring-num-periods" />
                        )}
                        {this.periodName()}
                    </FormItem>
                </Col>
            </Row>)
    }

    positiveAmount = (rule, value, callback) => {
        if (+value < 1) callback(true);
        else callback()
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        const fetching = this.state.recurring
            ? this.props.autoTransfers.create.fetching
            : this.props.transfers.create.fetching
        const accountCurr = this.accountCurrency();
        const common = this.currencyHelper.commonCurrencies([accountCurr]);
        const nonCommon = this.currencyHelper.nonCommonCurrencies([accountCurr]);
        return (
            <Form onSubmit={this.handleSubmit} className="transfer-create-form">
                <Row>
                    <Col>
                        <div className="transfer-switch">
                            <label>
                                <span className="transfer-switch-label">
                                    Recurring
                                </span>
                                <Switch onChange={(checked) => this.setState({ recurring: checked })} />
                            </label>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <div className="transfer-switch">
                            <label>
                                <span className="transfer-switch-label">
                                    {this.state.income ? 'Income' : 'Expense'}
                                </span>
                                <Switch className="transfer-expense-toggle"
                                    checkedChildren="+"
                                    unCheckedChildren="−"
                                    onChange={(checked) => this.setState({ income: checked })} />
                            </label>
                        </div>
                    </Col>
                </Row>
                <Row gutter={this.props.screenWidth.width >= sizes.XLARGE ? 32 : 16}>
                    <Col sm={24} md={6} lg={6} xl={5}>
                        <FormItem>
                            {getFieldDecorator('date', {
                                initialValue: moment().startOf('day'),
                                rules: [
                                    { type: 'object', required: true, message: 'Please enter a date' },
                                ]
                            })(
                                <DatePicker format="YYYY/MM/DD" placeholder="Date" className="required" />
                            )}
                        </FormItem>
                    </Col>
                    <Col sm={24} md={5} lg={4}>
                        <FormItem>
                            {getFieldDecorator('originalCurrency', {
                                validateTrigger: 'onBlur',
                                initialValue: accountCurr
                            })(
                                <Select>
                                    <OptGroup label="Account">
                                        <Option key={accountCurr} value={accountCurr}>{accountCurr}</Option>
                                    </OptGroup>
                                    {common.length ? (
                                        <OptGroup label="Common">
                                            {common.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                        </OptGroup>
                                    ) : null}
                                    <OptGroup label="Other">
                                        {nonCommon.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                    </OptGroup>
                                </Select>
                            )}
                        </FormItem>
                    </Col>
                    <Col sm={24} md={5} lg={5} xl={5}>
                        <FormItem>
                            {getFieldDecorator('amount', {
                                validateTrigger: 'onBlur',
                                rules: [
                                    { required: true, message: 'Please enter an amount' },
                                    { message: 'Value must be positive', validator: this.positiveAmount }
                                ],
                            })(
                                <InputNumber className="required"
                                    placeholder="Amount" />
                            )}
                        </FormItem>
                    </Col>
                    <Col sm={24} md={6} lg={7} xl={10}>
                        <FormItem>
                            {getFieldDecorator('description', {})(
                                <Input placeholder="Description" />
                            )}
                        </FormItem>
                    </Col>
                </Row>
                {this.recurringRow()}
                <FormItem>
                    <Button loading={fetching} type="primary" htmlType="submit" icon="plus">Create</Button>
                </FormItem>
            </Form>
        )
    }

    get currencyHelper() { return createCurrencyHelper(this.props.rates, this.props.auth); }
}
TransferCreate.propTypes = {
    transfers: PropTypes.object.isRequired,
    screenWidth: PropTypes.object.isRequired,
    autoTransfers: PropTypes.object.isRequired,
    rates: PropTypes.object.isRequired,
    accountId: PropTypes.number.isRequired,
    accounts: PropTypes.object.isRequired,
    auth: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired,
    onCreate: PropTypes.func
}

const mapStateToProps = (state) => ({
    accounts: state.accounts,
    auth: state.auth,
    rates: state.rates,
    transfers: state.transfers,
    autoTransfers: state.autoTransfers,
    screenWidth: state.screenWidth
})

export default connect(mapStateToProps)(Form.create()(TransferCreate));
