import { Action } from 'redux';

import {
  createAsyncActionOrchestration,
  IAction,
  IAsyncActions,
  IErrorAction,
} from '../actions';
import { get } from '../util';
import { ITransfersSummary } from './transfersSummary';

type AccountId = number;

export interface ISummaryRequestParameters {
  accountId?: AccountId;
  startDate: string;
  endDate: string;
  period: string;
}

type TransferSummaryActionType =
  | '[TransfersSummary] load start'
  | '[TransfersSummary] load success'
  | '[TransfersSummary] load error';

export const TransfersSummaryLoadStart = '[TransfersSummary] load start';
export const TransfersSummaryLoadSuccess = '[TransfersSummary] load success';
export const TransfersSummaryLoadError = '[TransfersSummary] load error';

interface ITransfersSummaryAction extends Action {
  type: TransferSummaryActionType;
}

interface ITransfersSummaryLoadStart extends ITransfersSummaryAction, IAction<ISummaryRequestParameters> {
  type: '[TransfersSummary] load start';
}

interface ITransfersSummaryLoadSuccess extends ITransfersSummaryAction, IAction<ITransfersSummary[]> {
  type: '[TransfersSummary] load success';
}

interface ITransfersSummaryLoadError extends ITransfersSummaryAction, IErrorAction<ISummaryRequestParameters> {
  type: '[TransfersSummary] load error';
}

export type TransfersSummaryAction =
  | ITransfersSummaryLoadStart
  | ITransfersSummaryLoadSuccess
  | ITransfersSummaryLoadError;

const TransfersSummaryLoadActions:
  IAsyncActions<ISummaryRequestParameters, ITransfersSummary[], ITransfersSummary[]> = {
  start: (data) => ({ type: TransfersSummaryLoadStart, data }),
  success: (data) => ({ type: TransfersSummaryLoadSuccess, data }),
  error: (error, data) => ({ type: TransfersSummaryLoadError, error, data }),
};

export const loadTransfersSummary =
  createAsyncActionOrchestration<ISummaryRequestParameters, ITransfersSummary[], ITransfersSummary[]>(
    (parameters) => {
      const url = parameters.accountId !== undefined
        ? `/api/account/${parameters.accountId}/transfer/summary`
        : `/api/transfer/summary`;
      return get(`${url}?startDate=${parameters.startDate}&endDate=${parameters.endDate}&period=${parameters.period}`);
    },
    TransfersSummaryLoadActions
  );
