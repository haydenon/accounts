import { IResource } from '../resource';

export interface ITransfer extends IResource {
    accountId: number;
    date: string;
    amount: number;
    currency: string;
    originalAmount?: number;
    originalCurrency?: string;
    description: string;
    createdAt: string;
}
