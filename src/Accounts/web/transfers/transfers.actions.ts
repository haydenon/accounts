import { message } from 'antd';

import {
    createAsyncActionOrchestration,
    createAsyncOrchestrationWith,
    IAction,
    IActionError,
    IAsyncActions,
    ICreateAsyncActions,
    ICreateFailedAction,
    ICreateStartAction,
    ICreateSuccessAction,
    IDeleteAsyncActions,
    IDeleteFailedAction,
    IDeleteStartAction,
    IDeleteSuccessAction,
    ILoadAsyncActions,
    ILoadConfig,
    ILoadData,
    ILoadFailedAction,
    ILoadStartAction,
    ILoadSuccessAction,
    ILoadSuccessData,
    IUpdateData,
    IUpdateFailedAction,
    IUpdateStartAction,
} from '../actions';
import { Relation } from '../resource';
import { del, get, post, put } from '../util';
import { ITransfer } from './transfer';

type LoadActions =
    | '[Transfers] load start'
    | '[Transfers] load success'
    | '[Transfers] load failed';

type CreateActions =
    | '[Transfers] create start'
    | '[Transfers] create success'
    | '[Transfers] create failed';

type DeleteActions =
    | '[Transfers] delete start'
    | '[Transfers] delete success'
    | '[Transfers] delete failed';

type UpdateActions =
    | '[Transfers] update start'
    | '[Transfers] update success'
    | '[Transfers] update failed';

export type TransferActionTypes = LoadActions | CreateActions | DeleteActions | UpdateActions;

interface ITransferAction {
    type: TransferActionTypes;
}

export const TransfersLoadStart = '[Transfers] load start';
export const TransfersLoadSuccess = '[Transfers] load success';
export const TransfersLoadFailed = '[Transfers] load failed';

export interface ITransferLoadStartAction extends ITransferAction, ILoadStartAction<ITransfer> {
    type: '[Transfers] load start';
}
export interface ITransferLoadSuccessAction extends ILoadSuccessAction<ITransfer> {
    type: '[Transfers] load success';
}
export interface ITransferLoadFailedAction extends ILoadFailedAction<ITransfer> {
    type: '[Transfers] load failed';
}

export const TransfersCreateStart = '[Transfers] create start';
export const TransfersCreateSuccess = '[Transfers] create success';
export const TransfersCreateFailed = '[Transfers] create failed';

export interface ITransferCreateStartAction extends ICreateStartAction<ITransfer> {
    type: '[Transfers] create start';
}
export interface ITransferCreateSuccessAction extends ICreateSuccessAction<ITransfer> {
    type: '[Transfers] create success';
}
export interface ITransferCreateFailedAction extends ICreateFailedAction<ITransfer> {
    type: '[Transfers] create failed';
}

export const TransfersDeleteStart = '[Transfers] delete start';
export const TransfersDeleteSuccess = '[Transfers] delete success';
export const TransfersDeleteFailed = '[Transfers] delete failed';

export interface ITransferDeleteStartAction extends IDeleteStartAction<ITransfer> {
    type: '[Transfers] delete start';
}
export interface ITransferDeleteSuccessAction extends IDeleteSuccessAction<ITransfer> {
    type: '[Transfers] delete success';
}
export interface ITransferDeleteFailedAction extends IDeleteFailedAction<ITransfer> {
    type: '[Transfers] delete failed';
}

export const TransfersUpdateStart = '[Transfers] update start';
export const TransfersUpdateSuccess = '[Transfers] update success';
export const TransfersUpdateFailed = '[Transfers] update failed';

interface ITransferUpdateResp {
    transfer: ITransfer;
    accountBalanceChange: number;
}

interface ITransferUpdateData extends IUpdateData<ITransfer> {
    accountBalanceChange: number;
}

export interface ITransferUpdateStartAction extends IUpdateStartAction<ITransfer> {
    type: '[Transfers] update start';
}
export interface ITransferUpdateSuccessAction extends IAction<ITransferUpdateData> {
    type: '[Transfers] update success';
}
export interface ITransferUpdateFailedAction extends IUpdateFailedAction<ITransfer> {
    type: '[Transfers] update failed';
}

export type TransferAction =
    | ITransferLoadStartAction
    | ITransferLoadSuccessAction
    | ITransferLoadFailedAction
    | ITransferCreateStartAction
    | ITransferCreateSuccessAction
    | ITransferCreateFailedAction
    | ITransferDeleteStartAction
    | ITransferDeleteSuccessAction
    | ITransferDeleteFailedAction
    | ITransferUpdateStartAction
    | ITransferUpdateSuccessAction
    | ITransferUpdateFailedAction;

const getAccountId = (id?: number): Relation | undefined => id ? new Relation('accountId', id) : undefined;

const getUrl = (config: ILoadConfig) => {
    const url = config.relation ? `/api/account/${config.relation.value}/transfer/` : `/api/transfer`;
    const params = [];
    if (config.skip) {
        params.push(`skip=${config.skip}`);
    }
    if (config.limit) {
        params.push(`limit=${config.limit}`);
    }

    return params.length
        ? `${url}?${params.join('&')}`
        : url;
};

const TransferLoadActions: ILoadAsyncActions<ITransfer> = {
    start: (data) => ({ type: '[Transfers] load start', data }),
    success: (data, start) => ({
        type: '[Transfers] load success', data: {
            config: start.config,
            data: data instanceof Array ? data : data.resources,
            total: data instanceof Array ? data.length : data.total,
        }
    }),
    error: (error, data) => ({ type: '[Transfers] load failed', error, data }),
};

export const loadTransfers = (limit: number, skip: number, accountId?: number) =>
    createAsyncOrchestrationWith({
        config: {
            relation: getAccountId(accountId),
            skip,
            limit
        }
    },
        (data) => get(getUrl(data.config)),
        TransferLoadActions
    );

const TransferCreateActions: ICreateAsyncActions<ITransfer> = {
    start: (data) => ({ type: '[Transfers] create start', data }),
    success: (data) => ({ type: '[Transfers] create success', data }),
    error: (error, data) => ({ type: '[Transfers] create failed', error, data }),
};

export const createTransfer = createAsyncActionOrchestration(
    (data) => post('/api/transfer/', data),
    TransferCreateActions
);

interface ITransferDeleteResp {
    transferId: number;
    accountBalanceChange: number;
}

interface ITransferDelete extends ITransferDeleteResp {
    accountId: number;
}

const TransferDeleteActions: IAsyncActions<ITransfer, ITransferDeleteResp, ITransferDelete> = {
    start: (data) => ({ type: '[Transfers] delete start', data }),
    success: (data, start) => ({ type: '[Transfers] delete success', data: { ...data, accountId: start.accountId } }),
    error: (error, data) => ({ type: '[Transfers] delete failed', error, data }),
};

export const deleteTransfer = createAsyncActionOrchestration(
    (data) => del(`/api/transfer/${data.id}/`),
    TransferDeleteActions
);

const TransferUpdateActions: IAsyncActions<IUpdateData<ITransfer>, ITransferUpdateResp, ITransferUpdateData> = {
    start: (data) => ({ type: '[Transfers] update start', data }),
    success: (data, start) => ({
        type: '[Transfers] update success', data: {
            original: start.original,
            update: data.transfer,
            accountBalanceChange: data.accountBalanceChange
        }
    }),
    error: (error, data) => ({ type: '[Transfers] update failed', error, data }),
};

export const updateTransfer = (original: ITransfer, update: ITransfer) =>
    createAsyncOrchestrationWith(
        { original, update },
        (data) => put(`/api/transfer/${data.original.id}/`, data.update),
        TransferUpdateActions
    );
