import { Moment } from 'moment';
const moment: (item: any) => Moment = require('moment');

import { IRates } from '../rates/rates.reducer';
import { exchange, exchangeWithRate } from '../util/exchange';
import { ITransfer } from './transfer';

export const updateTransferDetails = (rates: IRates) =>
    (original: ITransfer, updates: ITransfer, income: boolean): ITransfer => {
        if (!updates.originalCurrency) {
            throw new Error('Update must specify a currency');
        }
        const updated = { ...original };

        updated.amount = +updates.amount;
        updated.description = updates.description;

        if (!income) {
            updated.amount = -updated.amount;
        }

        updated.date = moment(updates.date).format('YYYY-MM-DD');
        if (updates.originalCurrency === original.currency) {
            delete updated.originalCurrency;
            delete updated.originalAmount;
        } else if (updates.originalCurrency === original.originalCurrency && original.originalAmount) {
            const rate = original.amount / original.originalAmount;
            updated.originalAmount = updates.amount;
            updated.amount = exchangeWithRate(original.currency, updated.amount, rate);
        } else {
            updated.originalCurrency = updates.originalCurrency;
            updated.originalAmount = updated.amount;
            updated.amount = exchange(
                updates.originalCurrency,
                original.currency,
                updated.amount,
                rates);
        }

        return updated;
    };
