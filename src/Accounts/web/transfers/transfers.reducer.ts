import { SIGNED_OUT } from '../auth/auth.actions';
import { IResource } from '../resource';
import {
  addToPaged,
  IResourceState,
  removePaged,
  updateForLoad,
  updateForLoaded,
  updateForLoadFailed,
  updateTotal
} from '../resource.reducer';
import { DEFAULT_ASYNC_STATE } from '../util/reducerHelpers';
import { ITransfer } from './transfer';
import {
  TransferAction,
  TransfersCreateFailed,
  TransfersCreateStart,
  TransfersCreateSuccess,
  TransfersDeleteFailed,
  TransfersDeleteStart,
  TransfersDeleteSuccess,
  TransfersLoadFailed,
  TransfersLoadStart,
  TransfersLoadSuccess,
  TransfersUpdateFailed,
  TransfersUpdateStart,
  TransfersUpdateSuccess
} from './transfers.actions';

export interface ITransfersState extends IResourceState<ITransfer> { }

const ordering = (t1: ITransfer, t2: ITransfer) => {
  const dateCmp = t2.date.localeCompare(t1.date);
  return dateCmp === 0
    ? t2.createdAt.localeCompare(t1.createdAt)
    : dateCmp;
};

const relations = ['accountId'];

const originalState: ITransfersState = {
  resources: [],
  load: {
    fetching: false,
    relations: {
      accountId: {}
    }
  },
  create: DEFAULT_ASYNC_STATE,
  delete: {},
  update: {}
};

export const transfers = (state = originalState, action: TransferAction) => {
  if ((action.type as string) === SIGNED_OUT) {
    return originalState;
  }

  switch (action.type) {
    case TransfersLoadStart:
      return updateForLoad({
        ...state,
        load: {
          ...state.load,
          fetching: true,
          error: undefined
        }
      }, action.data.config);
    case TransfersLoadSuccess:
      return updateForLoaded({
        ...state,
        load: {
          ...state.load,
          fetching: false
        }
      }, action.data.config, action.data.data, action.data.total || 0);
    case TransfersLoadFailed:
      return updateForLoadFailed({
        ...state,
        load: {
          ...state.load,
          fetching: false
        }
      }, action.data.config, action.error.message);
    case TransfersCreateStart:
      return {
        ...state,
        create: {
          ...state.create,
          fetching: true,
          error: undefined
        }
      };
    case TransfersCreateSuccess:
      const newState = addToPaged({
        ...state,
        resources: [...state.resources, action.data],
        create: {
          ...state.create,
          fetching: false,
        }
      }, relations,
        state.resources,
        action.data,
        ordering as (r1: IResource, r2: IResource) => number,
        (relation, rId) => (action.data as any)[relation] === rId
      );
      return updateTotal(
        newState,
        relations,
        true,
        (relation, rId) => (action.data as any)[relation] === rId
      );
    case TransfersCreateFailed:
      return {
        ...state,
        create: {
          ...state.create,
          fetching: false,
          error: action.error.message
        }
      };
    case TransfersDeleteStart:
      return {
        ...state,
        delete: {
          ...state.delete,
          [action.data.id]: {
            fetching: true,
            error: undefined
          }
        }
      };
    case TransfersDeleteSuccess:
      return updateTotal(removePaged({
        ...state,
        resources: state.resources.filter((r) => r.id !== action.data.id),
        delete: {
          ...state.delete,
          [action.data.id]: {
            fetching: false,
          }
        }
      }, relations), relations, false, (relation, rId) => (action.data as any)[relation] === rId);
    case TransfersDeleteFailed:
      return {
        ...state,
        delete: {
          ...state.delete,
          [action.data.id]: {
            fetching: false,
            error: action.error.message
          }
        }
      };
    case TransfersUpdateStart:
      return {
        ...state,
        update: {
          ...state.update,
          [action.data.update.id]: {
            fetching: true,
            error: undefined
          }
        }
      };
    case TransfersUpdateSuccess:
      const res = state.resources;
      const index = res.findIndex((i) => i.id === action.data.update.id);

      return {
        ...state,
        resources: [...res.slice(0, index), action.data.update, ...res.slice(index + 1, res.length)],
        update: {
          ...state.update,
          [action.data.update.id]: {
            fetching: false,
          }
        }
      };
    case TransfersUpdateFailed:
      return {
        ...state,
        update: {
          ...state.update,
          [action.data.update.id]: {
            fetching: false,
            error: action.error.message
          }
        }
      };
    default:
      const _exhaustiveCheck: never = action;
      return state;
  }
};
