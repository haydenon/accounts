import React from 'react';
import { connect } from 'react-redux';
import { Route } from 'react-router'
import { push } from 'react-router-redux'
import PropTypes from 'prop-types';
import { Link, Redirect } from 'react-router-dom';
import { Layout, Menu, Icon, Row, Col } from 'antd';
import UserDetails from './components/UserDetails';
import 'normalize.css';
import './styles/index.scss';
import './styles/layout.scss';
import { checkIfSignedIn } from './auth/auth.actions';
import { loadConfiguration } from './settings/settings.actions';
import { sizes } from './screen';

const { Header, Sider, Content } = Layout;

const override = (width, value) => {
  if (!value) {
    return undefined;
  } else if (typeof (value) === 'number' || typeof (value) === 'string') {
    return value;
  } else if (width <= sizes.SMALL) {
    return value.small;
  } else if (width <= sizes.MEDIUM) {
    return value.medium;
  } else if (width <= sizes.LARGE) {
    return value.large;
  } else if (width <= sizes.XLARGE) {
    return value.xlarge
  } else {
    return value.xxlarge
  }
}

class AppRouteComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { collapsed: true };
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  componentDidMount() {
    this.props.dispatch(checkIfSignedIn((d) => d(push('/signin'))))
    if (!this.props.configuration) {
      this.props.dispatch(loadConfiguration());
    }
  }

  contentMargin = () => {
    const getOverride = () => override(this.props.screenWidth.width, this.props.margin);
    if (this.props.screenWidth.width >= 600)
      return getOverride() || '18px 18px'
    if (this.props.screenWidth.width >= 410)
      return getOverride() || '12px 12px'
    else
      return getOverride() || '8px 8px';
  }

  contentPadding = () => {
    const getOverride = () => override(this.props.screenWidth.width, this.props.padding);
    if (!this.props.background)
      return getOverride() || 0
    if (this.props.screenWidth.width >= 600)
      return getOverride() || 24;
    if (this.props.screenWidth.width >= 410)
      return getOverride() || 16;
    else
      return getOverride() || 12;
  }

  render() {
    if (!this.props.user) {
      return this.props.authAttempted
        ? (<Redirect to={{ pathname: '/signin' }} />)
        : (<span>
        </span>)
    }

    if (!this.props.configuration) {
      return (<span></span>);
    }

    const path = (name) => !name ? 'accounts' : name;
    const {
      component: Component,
      user: _user,
      authAttempted: _attempted,
      dispatch: _dispatch,
      pathname,
      ...rest
    } = this.props;

    return (
      <Route {...rest} render={matchProps => (
        <Layout className="layout">
          <Sider
            trigger={null}
            collapsible
            collapsed={this.state.collapsed}>
            <div className="logo">
              <Link to={"/"}>
                <img src={'/assets/logo_white.png'} />
              </Link>
            </div>
            <Menu theme="dark" mode="inline" selectedKeys={[path(pathname.split("/")[1])]}>
              <Menu.Item key="accounts">
                <Link to={'/accounts'}>
                  <Icon type="wallet" />
                  <span>Accounts</span>
                </Link>
              </Menu.Item>
              {
                this.props.configuration.dashboardEnabled
                  ? (
                    <Menu.Item key="dashboard">
                      <Link to={'/dashboard'}>
                        <Icon type="area-chart" />
                        <span>Dashboard</span>
                      </Link>
                    </Menu.Item>
                  )
                  : null
              }
              <Menu.Item key="settings">
                <Link to={'/settings'}>
                  <Icon type="setting" />
                  <span>Settings</span>
                </Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Header style={{ background: '#fff', padding: 0 }}>
              <Row type="flex" justify="space-between" align="middle">
                <Col>
                  {this.props.screenWidth.width >= 600 ? (
                    <Icon
                      className="trigger"
                      type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                      onClick={this.toggle} />) : null}
                </Col>
                <Col>
                  <UserDetails className="auth"></UserDetails>
                </Col>
              </Row>
            </Header>
            <Content style={{ height: '100%', margin: this.contentMargin(), padding: this.contentPadding(), background: this.props.background ? '#fff' : 'none' }}>
              <Component {...matchProps} />
            </Content>
          </Layout>
        </Layout>
      )} />
    );
  }
}
AppRouteComponent.propTypes = {
  margin: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]),
  padding: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.object,
  ]),
  dispatch: PropTypes.func.isRequired,
  screenWidth: PropTypes.object.isRequired,
  user: PropTypes.object,
  configuration: PropTypes.object,
  authAttempted: PropTypes.bool.isRequired,
  pathname: PropTypes.string.isRequired,
  component: PropTypes.any.isRequired,
  background: PropTypes.bool
}

const mapStateToProps = (state) => ({
  user: state.auth.user,
  authAttempted: !!state.auth.attempted,
  pathname: state.routing.location.pathname,
  screenWidth: state.screenWidth,
  configuration: state.settings.configuration
})

export default connect(mapStateToProps)(AppRouteComponent);
