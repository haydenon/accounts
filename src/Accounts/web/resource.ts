export type ResourceId = number;

export type RelationKey = string;

export interface IResource {
  id: ResourceId;
}

export class Relation {
  constructor(
    public key: RelationKey,
    public value: ResourceId
  ) { }
}
