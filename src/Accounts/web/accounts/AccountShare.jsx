import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { Button, Form, Input, message, Switch, Table } from 'antd';

import { validateSubmit } from '../util/index';

class AccountShare extends React.Component {
    onSubmit = validateSubmit(this.props.form, (share) => {
        const email = share['share-email'].trim();
        if (this.props.shares.every((s) => s.shareUser.toLowerCase() !== email.toLowerCase())) {
            this.props.onShare(email);
            this.props.form.resetFields();
        } else {
            message.warning(`Already sharing with user ${email}`);
        }
    })

    update = (share) => (checked) => {
        const updated = {
            ...share,
            readonly: !checked,
        }
        this.props.onUpdate(share, updated);
    }

    remove = (share) => () => {
        this.props.onRemove(share);
    }

    size = () => this.props.screenWidth.width > 500
        ? 'default'
        : 'small';

    render() {
        const { getFieldDecorator } = this.props.form;
        const shareMargin = 5;
        const shareWidth = 92;
        return (
            <div>
                <Form onSubmit={this.onSubmit}>
                    <Form.Item>
                        <div
                            style={{
                                display: 'inline-block',
                                width: `calc(100% - ${shareWidth + shareMargin}px)`,
                                marginRight: shareMargin,
                            }}
                        >
                            {getFieldDecorator('share-email', {
                                validateTrigger: 'onBlur',
                                rules: [{ type: 'email', required: true, message: 'Enter user\'s email address' }]
                            })(<Input placeholder="User email" />)}
                        </div>
                        <Button
                            type="primary"
                            style={{ display: 'inline-block', width: shareWidth }}
                            icon="share-alt"
                            htmlType="submit"
                            loading={this.props.accountShares.create.fetching}
                        >
                            Share
                        </Button>
                    </Form.Item>
                </Form>
                <Table columns={this.columns} rowKey="id" dataSource={this.props.shares} size={this.size()} />
            </div>
        );
    }

    columns = [{
        title: 'User',
        dataIndex: 'shareUser',
        key: 'user'
    }, {
        title: 'Can update',
        dataIndex: 'readonly',
        key: 'can-update',
        render: (readonly, record) => (
            <Switch size={this.size()} defaultChecked={!readonly} onChange={this.update(record)} />
        )
    }, {
        title: 'Remove',
        key: 'remove',
        render: (_, record) => (
            <Button size={this.size()} shape="circle" type="danger" icon="close" onClick={this.remove(record)} />
        ),
    }];
}
AccountShare.propTypes = {
    screenWidth: PropTypes.object.isRequired,
    accountShares: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    shares: PropTypes.array.isRequired,
    onShare: PropTypes.func.isRequired,
    onUpdate: PropTypes.func.isRequired,
    onRemove: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
    accountShares: state.accountShares,
    screenWidth: state.screenWidth,
})

export default connect(mapStateToProps)(Form.create()(AccountShare));
