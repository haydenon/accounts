import { IResource, ResourceId } from '../resource';

export interface IAccountShare {
  id: ResourceId;
  accountId: ResourceId;
  shareUser: string;
  readonly: boolean;
}

export interface IAccount extends IResource {
  createdAt: string;
  name: string;
  balance: number;
  currency: string;
  owner: string | null;
  readonly: boolean;
  shares: IAccountShare[];
}
