import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import { Avatar, Row, Col, Card, Icon, Form, Input, Button, Select } from 'antd';
import { loadAccounts, createAccount } from './accounts.actions';
import { loadRates } from '../rates/rates.actions';
import { avatarDetails, validateSubmit, formatMoney } from '../util';
import moment from 'moment';
const { Meta } = Card;
const FormItem = Form.Item;
const Option = Select.Option;
const OptGroup = Select.OptGroup;

import './accounts.scss';
import { createCurrencyHelper } from '../util/stateHelpers';

const placeholders = [
    { id: 1, name: 'Loading', balance: 0 },
    { id: 2, name: 'Loading', balance: 0 },
    { id: 3, name: 'Loading', balance: 0 }
]

class CreateCard extends React.Component {
    componentDidMount() {
        this.nameInput.focus();
    }

    handleSubmit = validateSubmit(this.props.form, () => {
        this.props.onSubmit(this.props.form.getFieldsValue());
    })

    render() {
        const fetching = this.props.accounts.create.fetching
            || this.props.rates.isFetching;
        const { getFieldDecorator } = this.props.form;
        const common = this.currencyHelper.commonCurrencies();
        const nonCommon = this.currencyHelper.nonCommonCurrencies();
        const currencyItems = nonCommon.map((curr) => <Option key={curr} value={curr}>{curr}</Option>);
        const initial = common.length ? common[0] : nonCommon[0];
        return (
            <Form layout="vertical" onSubmit={this.handleSubmit}>
                <Card bordered={false} className="create-account-name">
                    <Row gutter={8}>
                        <Col xs={8} sm={5} lg={8} xl={7}>
                            <FormItem>
                                {getFieldDecorator('currency', {
                                    validateTrigger: 'onBlur',
                                    initialValue: initial
                                })(
                                    <Select>
                                        {common.length ? (
                                            <OptGroup label="Common">
                                                {common.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                            </OptGroup>) : null}
                                        {common.length ?
                                            (<OptGroup label="Others">
                                                {currencyItems}
                                            </OptGroup>)
                                            : currencyItems
                                        }
                                    </Select>
                                )}
                            </FormItem>
                        </Col>
                        <Col xs={16} sm={19} lg={16} xl={17}>
                            <FormItem className="account-name-input">
                                {getFieldDecorator('name', {
                                    validateTrigger: 'onBlur',
                                    rules: [{ required: true, message: null }]
                                })(
                                    <Input prefix={<Icon type="tag" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        ref={(ip) => this.nameInput = ip}
                                        placeholder="Name"
                                        disabled={fetching} />
                                )}
                            </FormItem>
                        </Col>
                    </Row>
                </Card>
                <FormItem>
                    <Row gutter={8}>
                        <Col xs={12} xl={17}>
                            <Button icon="plus" type="primary" htmlType="submit"
                                className="full-width"
                                loading={fetching}>
                                Create
                            </Button>
                        </Col>
                        <Col xs={12} xl={7}>
                            <Button icon="close" type="danger" className="full-width"
                                onClick={this.props.onCancel}
                                disabled={fetching}>
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </FormItem>
            </Form>
        );
    }

    get currencyHelper() { return createCurrencyHelper(this.props.rates, this.props.auth); }
}
CreateCard.propTypes = {
    auth: PropTypes.object.isRequired,
    rates: PropTypes.object.isRequired,
    accounts: PropTypes.object.isRequired,
    form: PropTypes.object.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    auth: state.auth,
    rates: state.rates,
    accounts: state.accounts
});

const WrappedCreateCard = connect(mapStateToProps)(Form.create()(CreateCard));

class Accounts extends React.Component {
    constructor(props) {
        super(props);
        this.state = { adding: false };
    }

    componentDidMount() {
        if (!this.props.accounts.load.fetching) {
            this.props.dispatch(loadAccounts())
        }

        if (this.props.rates.expiry < moment()) {
            this.props.dispatch(loadRates());
        }
    }

    handleCreate = (account) => {
        this.props.dispatch(createAccount(account))
            .then(() => {
                if (!this.props.accounts.create.error) {
                    this.setState({ adding: false })
                }
            });
    }

    newCard = () => (
        <Col key="new" xs={24} lg={8} className="new-account-card account">
            <div onClick={() => this.setState({ adding: true })}>
                <Card bordered={true}>
                    <Icon type="plus" /> New account
                </Card>
            </div>
        </Col>
    )

    newAccount = () => {
        if (this.props.accounts.load.fetching) return <span></span>;

        return this.state.adding ? (
            <Col key="new" xs={24} lg={8} className="account">
                <WrappedCreateCard
                    onSubmit={this.handleCreate}
                    onCancel={() => this.setState({ adding: false })} />
            </Col>
        ) : this.newCard()
    }

    description = (item) => {
        return formatMoney(item.currency, item.balance);
    }

    title = (item) => {
        return [
            <div key="name">{item.name}</div>,
            item.owner ? <div key="owner" className="owner">{item.owner}</div> : null
        ];
    }

    className = (item, fetching) => {
        let classes = 'account';
        if (fetching) {
            classes += ' loading';
        }
        if (item && item.owner) {
            classes += ' with-owner';
        }
        return classes;
    }

    render() {
        const fetching = this.props.accounts.load.fetching
        const items = fetching
            ? placeholders
            : this.props.accounts.resources
        return (
            <div>
                <Row gutter={16}>
                    {items.map((item) => {
                        const { text, color } = avatarDetails(item.name);
                        return (
                            <Col key={item.id} xs={24} lg={8} className={this.className(item, fetching)} >
                                <Link to={`/accounts/${item.id}`}>
                                    <Card loading={fetching} bordered={false}>
                                        <Meta title={this.title(item)}
                                            avatar={<Avatar
                                                className="accounts-avatar"
                                                style={{ backgroundColor: color, verticalAlign: 'middle' }}
                                                size={'large'}
                                            >{text}</Avatar>}
                                            description={this.description(item)} />
                                    </Card>
                                </Link>
                            </Col>
                        )
                    })}
                    {this.newAccount()}
                </Row>
            </div>
        );
    }
}
Accounts.propTypes = {
    rates: PropTypes.object.isRequired,
    accounts: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

export default connect(mapStateToProps)(Accounts);
