import React from 'react';
import { connect, Dispatch } from 'react-redux';

import { Button, Form, Icon, Input, Modal, Select, Tabs } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
const FormItem = Form.Item;
const TabPane = Tabs.TabPane;
const Option = Select.Option;
const OptGroup = Select.OptGroup;

import { createAccountShare, deleteAccountShare, updateAccountShare } from './account-share.actions';

import { IAuthState } from '../auth/auth.reducer';
import { IRatesState } from '../rates/rates.reducer';
import { IResourceState } from '../resource.reducer';
import { IScreenState, sizes } from '../screen';
import { Configuration } from '../settings/settings.reducer';
import { validateSubmit } from '../util';
import { createCurrencyHelper } from '../util/stateHelpers';
import { IAccount, IAccountShare } from './accounts';
import { updateAccount } from './accounts.actions';
import AccountShare from './AccountShare';

import './accountSettings.scss';

interface IGeneralProps extends FormComponentProps {
    account: IAccount;
    accounts: IResourceState<IAccount>;
    auth: IAuthState;
    rates: IRatesState;
    configuration: Configuration;
    onUpdate: (original: IAccount, updated: IAccount) => void;
}

interface IAccountUpdate {
    name: string;
    currency: string;
}

class GeneralSettingsForm extends React.Component<IGeneralProps> {
    private handleSubmit = validateSubmit<IAccountUpdate>(this.props.form, (update) => {
        const updated = {
            ...this.props.account,
            name: update.name,
            currency: update.currency
        };

        this.props.onUpdate(this.props.account, updated);
    });

    public componentDidMount() {
        const account = this.props.account;
        this.props.form.setFieldsValue({
            name: account.name,
            currency: account.currency
        });
    }

    public componentDidUpdate(prevProps: IGeneralProps) {
        const account = this.props.account;

        if (account === prevProps.account) {
            return;
        }

        this.props.form.setFieldsValue({
            name: account.name,
            currency: account.currency
        });
    }

    public render() {
        const { getFieldDecorator } = this.props.form;
        const accountCurr = this.accountCurrency();
        const common = this.currencyHelper.commonCurrencies([this.accountCurrency()]);
        const nonCommon = this.currencyHelper.nonCommonCurrencies([this.accountCurrency()]);
        const update = this.props.accounts.update[this.props.account.id];
        const updating = update && update.fetching;
        const canChangeCurrency = this.props.configuration.currencyChangeEnabled;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem label="Name">
                    {getFieldDecorator('name', {
                        rules: [{ required: true, message: 'Please enter a name' }]
                    })(
                        <Input placeholder="Name" style={{ maxWidth: 350 }} />
                    )}
                </FormItem>
                <FormItem label="Currency">
                    {getFieldDecorator('currency', {
                        validateTrigger: 'onBlur',
                        initialValue: accountCurr,
                        rules: [{ required: true, message: 'Please choose a currency' }]
                    })(
                        <Select disabled={!canChangeCurrency} style={{ maxWidth: 100 }}>
                            <OptGroup label="Account">
                                <Option key={accountCurr} value={accountCurr}>{accountCurr}</Option>
                            </OptGroup>
                            {common.length ? (
                                <OptGroup label="Common">
                                    {common.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                                </OptGroup>
                            ) : null}
                            <OptGroup label="Other">
                                {nonCommon.map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                            </OptGroup>
                        </Select>
                    )}
                    {
                        canChangeCurrency
                            ? null
                            : <span className="currency__help-text--disabled">
                                Changing account currency is temporarily disabled, but will be back soon
                            </span>
                    }
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                        loading={updating}
                        disabled={!this.changed()}
                    >
                        Update
                    </Button>
                </FormItem>
            </Form >
        );
    }

    private get currencyHelper() { return createCurrencyHelper(this.props.rates, this.props.auth); }

    private changed = () => {
        const values = this.props.form.getFieldsValue() as IAccountUpdate;
        return this.props.account.currency !== values.currency
            || this.props.account.name !== values.name;
    }

    private accountCurrency = () => this.props.account.currency;
}

const mapGeneralStateToProps = (state: any) => ({
    auth: state.auth,
    rates: state.rates,
    accounts: state.accounts,
    configuration: state.settings.configuration,
});

const WrappedGeneralForm = connect(mapGeneralStateToProps)(Form.create()(GeneralSettingsForm));

interface IProps {
    dispatch: Dispatch<any>;
    account: IAccount;
    visible: boolean;
    onCancel: () => void;
    screenWidth: IScreenState;
}

class AccountSettings extends React.Component<IProps> {
    public render() {
        return (
            <Modal
                width={this.modalWidth()}
                title="Account settings"
                visible={this.props.visible}
                onCancel={this.props.onCancel}
                footer={[<Button key="done" onClick={this.props.onCancel}> Done</ Button>]}
            >
                <Tabs defaultActiveKey="1">
                    <TabPane tab={<span><Icon type="setting" />General</span>} key="1">
                        <WrappedGeneralForm account={this.props.account} onUpdate={this.updateAccount} />
                    </TabPane>
                    <TabPane tab={<span><Icon type="share-alt" />Sharing</span>} key="2">
                        <AccountShare
                            onShare={this.createShare}
                            onUpdate={this.updateShare}
                            onRemove={this.removeShare}
                            shares={this.props.account.shares}
                        />
                    </TabPane>
                </Tabs>
            </Modal>
        );
    }

    private updateAccount = (original: IAccount, updated: IAccount) =>
        this.props.dispatch(updateAccount(original, updated))

    private modalWidth = () => {
        const width = this.props.screenWidth.width * 0.80 >= sizes.MEDIUM ? sizes.MEDIUM : '85%';
        return width;
    }

    private createShare = (email: string) => {
        const share = {
            id: 0,
            accountId: this.props.account.id,
            shareUser: email,
            readonly: true
        };
        this.props.dispatch(createAccountShare(this.props.account, share));
    }

    private updateShare = (original: IAccountShare, updated: IAccountShare) => {
        this.props.dispatch(updateAccountShare(this.props.account, original, updated));
    }

    private removeShare = (share: IAccountShare) => {
        this.props.dispatch(deleteAccountShare(this.props.account, share));
    }
}

const mapStateToProps = (state: { screenWidth: IScreenState }) => ({
    screenWidth: state.screenWidth,
});

export default connect(mapStateToProps)(AccountSettings);
