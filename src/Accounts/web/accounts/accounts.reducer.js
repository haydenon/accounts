import { accountActions } from './accounts.actions';
import {
    TransfersCreateSuccess,
    TransfersDeleteSuccess,
    TransfersUpdateSuccess
} from '../transfers/transfers.actions';
import { reducer } from '../resource.reducer';
import { accountShareActions } from './account-share.actions';

const replace = (arr, pred, item) => {
    const index = arr.findIndex(pred);
    return [...arr.slice(0, index), item, ...arr.slice(index + 1, arr.length)];
}

const updateAccountBalance = (getAccountId, getDifference) => (state, action) => {
    const accountId = getAccountId(action);
    const difference = getDifference(action)
    const index = state.resources.findIndex(a => a.id === accountId);
    const origAccount = state.resources[index];
    const account = {
        ...origAccount,
        balance: origAccount.balance + difference
    };
    return {
        ...state,
        resources: replace(state.resources, (_, ind) => ind === index, account)
    }
}

const getResourceAccountId = (action) => action.data.accountId;
const getDeleteDifference = (action) => -action.data.accountBalanceChange;
const getAddDifference = (action) => action.data.amount;

const getUpdateDifference = (action) => action.data.accountBalanceChange

const onAccountShareAction = (updateShares, update = false) => (state, action) => {
    const accountId = update
        ? action.updated.accountId
        : getResourceAccountId(action);
    const index = state.resources.findIndex(a => a.id === accountId);
    const origAccount = state.resources[index];
    const account = {
        ...origAccount,
        shares: updateShares(origAccount.shares, update ? action.updated : action.resource)
    };
    return {
        ...state,
        resources: replace(state.resources, (_, ind) => ind === index, account)
    }
}

const addAccountShare = onAccountShareAction((shares, share) => {
    return [...shares, share];
});

const updateAccountShare = onAccountShareAction((shares, share) => {
    return replace(shares, (s) => s.id === share.id, share);
}, true);

const removeAccountShare = onAccountShareAction((shares, share) => {
    const ind = shares.findIndex((s) => s.id === share.id);
    return [...shares.slice(0, ind), ...shares.slice(ind + 1, shares.length)];
});

export const accounts = reducer(accountActions, {
    customActionHandler: {
        [TransfersCreateSuccess]: updateAccountBalance(getResourceAccountId, getAddDifference),
        [TransfersDeleteSuccess]: updateAccountBalance(getResourceAccountId, getDeleteDifference),
        [TransfersUpdateSuccess]: updateAccountBalance((action) => action.data.update.accountId, getUpdateDifference),
        [accountShareActions.CREATED]: addAccountShare,
        [accountShareActions.UPDATED]: updateAccountShare,
        [accountShareActions.DELETED]: removeAccountShare,
    }
})
