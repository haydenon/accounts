import { accountShareActions } from './account-share.actions';
import { reducer } from '../resource.reducer';

export const accountShares = reducer(accountShareActions, { relations: ['accountId'] })
