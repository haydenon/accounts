import React from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import { List, Row, Col, Button, Collapse, Divider, Icon, Form, Input, Tabs, message } from 'antd';
import Actions from '../components/Actions';
import { loadAccounts, deleteAccount, updateAccount } from './accounts.actions';
import AccountSettings from './AccountSettings';
import TransferCreate from '../transfers/TransferCreate';
import TransferEdit from '../transfers/TransferEdit';
import { loadTransfers, deleteTransfer } from '../transfers/transfers.actions';
import { loadAutoTransfers, deleteAutoTransfer } from '../autotransfers/autotransfers.actions';
import moment from 'moment'
import { formatMoney, validateSubmit } from '../util/index';
import { sizes } from '../screen';
import { AutoSizer } from 'react-virtualized';
import VirtualList from '../components/VirtualList';

const Panel = Collapse.Panel;
const TabPane = Tabs.TabPane;

import './accountDetails.scss'
import { createResourceHelper } from '../util/resourceHelper';
import { Relation } from '../resource';

class AccountNameForm extends React.Component {
    componentDidMount() {
        this.nameInput.focus();

        // This puts the caret at the end of the text
        const val = this.nameInput.input.value;
        this.nameInput.input.value = '';
        this.nameInput.input.value = val;
    }

    handleSubmit = validateSubmit(this.props.form, (accountName) => this.props.handleSubmit(accountName.name));

    render() {
        const { getFieldDecorator } = this.props.form;
        const status = this.props.accounts.update[this.props.account.id];
        const fetching = status && status.fetching;
        return (
            <Form onSubmit={this.handleSubmit}>
                <div className="account-edit-name-container">
                    {getFieldDecorator('name', { initialValue: this.props.account.name })(
                        <Input placeholder="Account name"
                            className="account-edit-name-input"
                            ref={(ip) => this.nameInput = ip} />
                    )}
                    <Button type="primary"
                        className="account-edit-update"
                        htmlType="submit"
                        icon="check"
                        loading={fetching}>Update</Button>
                </div>
            </Form>);
    }
}
AccountNameForm.propTypes = {
    account: PropTypes.object,
    accounts: PropTypes.object,
    dispatch: PropTypes.func.isRequired,
    form: PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired
}

const mapHeaderStateToProps = (state) => ({
    accounts: state.accounts
});

const WrappedHeaderForm = connect(mapHeaderStateToProps)(Form.create()(AccountNameForm));

class AccountDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addTransfer: false,
            showSettings: false,
            page: 1,
            pageSize: 10
        };
    }

    id = () => +this.props.match.params.id;

    transferHelper = () => createResourceHelper(this.props.transfers)(new Relation('accountId', this.id()));

    transfers = (page = 0, pageSize = 0) => this.transferHelper().list((page - 1) * pageSize, pageSize) || [];

    totalTransfers = () => this.transferHelper().total() || 0;

    autoTransfers = () => this.props.autoTransfers.resources.filter((t) => t.accountId === this.id());

    componentDidMount() {
        if (!this.props.accounts.resources.length) {
            this.props.dispatch(loadAccounts())
        }

        this.loadPage(this.state.page, this.state.pageSize);
        this.props.dispatch(loadAutoTransfers(this.id()))
    }

    deleteAccount = () => {
        this.props.dispatch(deleteAccount(this.account()))
            .then(() => {
                if (!this.props.accounts.delete[this.id()].error) {
                    this.props.dispatch(push('/accounts'))
                }
            })
    }

    deleteTransfer = (transfer) => {
        this.props.dispatch(deleteTransfer(transfer))
            .then(() => {
                let page = this.state.page;
                const total = this.totalTransfers()
                if ((page - 1) * this.state.pageSize >= total && page > 1) {
                    page -= 1;
                    this.setState({
                        page
                    })
                }

                this.loadPage(page, this.state.pageSize)
            })
            .then(() => message.success("Successfully deleted transfer"))
            .catch((err) => message.error(err.message, 6));
    }

    editTransfer = (transfer, recurring) => {
        this.setState({
            editingTransfer: transfer,
            editingRecurring: recurring
        });
    }

    cancelEdit = () => this.setState({ editingTransfer: null, editingRecurring: false });

    deleteAutoTransfer = (auto) => {
        this.props.dispatch(deleteAutoTransfer(auto));
    }

    handleNameUpdate = (name) => {
        if (name === this.account().name) {
            this.setState({ editName: false });
            return;
        }

        const update = {
            ...this.account(),
            name
        };

        this.props.dispatch(updateAccount(this.account(), update))
            .then(() => {
                if (!this.props.accounts.update[update.id].error) {
                    this.setState({ editName: false });
                }
            });
    };

    screenWidth = () => this.props.screenWidth.width;

    account = () => {
        const index = this.props.accounts.resources.findIndex((a) => a.id === this.id());
        return this.props.accounts.resources[index];
    }

    transferDescription = (transfer) => {
        const amount = formatMoney(transfer.currency, transfer.amount);
        const original = transfer.originalAmount ? ` ← ${formatMoney(transfer.originalCurrency, transfer.originalAmount)}` : ''
        return `${amount}${original}`;
    }

    autoTransferDescription = (autoTransfer) =>
        formatMoney(autoTransfer.currency, autoTransfer.amount);

    normalName = () => (<span onClick={() => this.setState({ editName: true })}>
        <h2 className="account-details-name">{this.account().name}</h2>
        <Icon className="account-name-edit" type="edit" />
    </span>);

    showButtons = (auto) => (width) =>
        width >= sizes.MEDIUM && ((auto && (width >= 1300 || width < sizes.LARGE)) || !auto);

    loadPage = (page, pageSize) => {
        const start = (page - 1) * pageSize;

        const loaded = this.transferHelper().loaded(start, pageSize)

        if (!loaded) {
            this.props.dispatch(loadTransfers(pageSize, start, this.id()))
                .catch(() => message.error('Failed to retrieve transfers', 3));
        }
    }

    setPage = (page, pageSize) => {
        this.setState({
            page,
            pageSize
        })

        this.loadPage(page, pageSize);
    }

    changePageSize = (page, pageSize) => {
        this.setState({
            page,
            pageSize
        });

        this.loadPage(page, pageSize);
    }

    onTransferCreate = () => {
        this.loadPage(this.state.page, this.state.pageSize);
    }

    transferDeleteText = (auto) => `Are you sure delete this ${auto ? 'recurring ' : ''}transfer?`;

    normalList = (isTabbed) => {
        const fetchingTransfer = !this.transferHelper()
            .loaded((this.state.page - 1) * this.state.pageSize, this.state.pageSize);

        const date = (item) => moment(item.date).format("YYYY/MM/DD");

        const title = (item) => this.screenWidth() >= sizes.LARGE
            ? <span>
                <span>{date(item)}</span>
                {item.recurring ? (<Icon className="transfer-recurring-symbol" type="retweet" />) : null}
            </span>
            : <span>{item.description || 'No description'}</span>;

        const description = (item) => this.screenWidth() >= sizes.LARGE
            ? [this.transferDescription(item)]
            : [date(item), <br key="br" />, this.transferDescription(item)];

        const canWrite = !this.account().owner || !this.account().readonly;

        return (
            <AutoSizer>
                {({ height, width }) => {
                    height = height - (isTabbed ? 45 : 0);

                    this.listHeight = height;
                    const data = this.transfers(this.state.page, this.state.pageSize);

                    return (<div style={{ height, width }}>
                        <List
                            style={{ height, overflowY: "auto", overflowX: "hidden" }}
                            loading={fetchingTransfer}
                            dataSource={data}
                            pagination={{
                                current: this.state.page,
                                total: this.totalTransfers(),
                                onChange: this.setPage,
                                onShowSizeChange: this.changePageSize,
                                showSizeChanger: true,
                                pageSizeOptions: ['10', '25', '50'],
                                pageSize: this.state.pageSize,
                                size: this.props.screenWidth.width >= sizes.LARGE ? "default" : "small"
                            }}
                            renderItem={(item) => (
                                <List.Item key={item.id} className="transfer" actions={[
                                    canWrite ? <Actions
                                        key="actions"
                                        actions={[
                                            { text: 'Edit', icon: 'edit', type: 'default', onChoose: () => this.editTransfer(item, false) },
                                            { text: 'Delete', popoverText: this.transferDeleteText(), icon: 'delete', type: 'danger', onChoose: () => this.deleteTransfer(item) },
                                        ]}
                                        collapse={this.showButtons()}
                                    /> : null]}>
                                    <List.Item.Meta
                                        title={title(item)}
                                        description={description(item)} />
                                    {this.screenWidth() >= sizes.LARGE
                                        ? <div
                                            style={{ width: width - 420 }}
                                            className="transfer-description">
                                            {item.description || 'No description'}
                                        </div>
                                        : null}
                                </List.Item>)}
                        /></div>)
                }}
            </AutoSizer>
        );
    }

    recurringList = (isTabbed) => {
        const canWrite = !this.account().owner || !this.account().readonly;
        return (
            <VirtualList loading={this.props.autoTransfers.load.fetching}
                height={(h) => h - (isTabbed ? 45 : 0)}
                rowCount={this.autoTransfers().length}
                rowHeight={() => 95}
                rowRenderer={({ index, key, style }) => {
                    const item = this.autoTransfers()[index]
                    return (
                        <List.Item key={key} style={style} className="recurring-transfer" actions={[
                            canWrite ? <Actions
                                key="actions"
                                actions={[
                                    { text: 'Edit', icon: 'edit', type: 'default', onChoose: () => this.editTransfer(item, true) },
                                    { text: 'Delete', popoverText: this.transferDeleteText(true), icon: 'delete', type: 'danger', onChoose: () => this.deleteAutoTransfer(item) },
                                ]}
                                collapse={this.showButtons(true)} /> : null]}>
                            <List.Item.Meta
                                title={item.description || 'No description'}
                                description={[
                                    moment(item.nextDate).format("YYYY/MM/DD"),
                                    <br key="br" />,
                                    this.autoTransferDescription(item)]} />
                        </List.Item>);
                }} />);
    }

    tabbedLists = () => {
        return <Tabs defaultActiveKey="1">
            <TabPane tab="Past" key="1">{this.normalList(true)}</TabPane>
            <TabPane tab="Recurring" key="2">{this.recurringList(true)}</TabPane>
        </Tabs>;
    }

    openSettings = () => {
        this.setState({ showSettings: true });
    }

    closeSettings = () => {
        this.setState({ showSettings: false });
    }

    render() {
        if (!this.props.accounts.resources.length
            || this.props.accounts.load.fetching
            || !this.account()) {
            return (<span></span>)
        }

        const deleteText =
            `Are you sure that you want to delete this account, along with all its transfers?\nThis action is not reversible`;

        const isOwner = !this.account().owner;
        const readonly = !isOwner && this.account().readonly;

        return (
            <Row gutter={48} style={{ height: '100%' }}>
                <Col lg={18} xl={16} sm={24} style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                    <div style={{ flex: '0 0 65px' }}>
                        <Row type="flex" justify="space-between" align="middle">
                            <Col>
                                {isOwner
                                    ? (
                                        this.state.editName
                                            ? <WrappedHeaderForm account={this.account()} handleSubmit={this.handleNameUpdate} />
                                            : this.normalName()
                                    )
                                    : <h2 className="account-details-name">{this.account().name}</h2>
                                }
                                {this.account().owner ? <div>{this.account().owner}</div> : null}
                                <div className="account-details-balance">{formatMoney(this.account().currency, this.account().balance)}</div>
                            </Col>
                            {readonly ? null : <Col className="account-details-actions">
                                {!this.state.addTransfer ?
                                    (<Button type="primary" icon="plus" onClick={() => this.setState({ addTransfer: true })}>
                                        New transfer
                                </Button>)
                                    : (<Button type="default" icon="cross" onClick={() => this.setState({ addTransfer: false })}>
                                        Close
                                </Button>)}
                                {isOwner ? [
                                    <Divider key="divider" type="vertical" />,
                                    <Actions
                                        key="actions"
                                        actions={[
                                            // { text: 'Share', icon: 'share-alt', type: 'default', onChoose: this.openShare },
                                            { text: 'Settings', icon: 'setting', type: 'default', onChoose: this.openSettings },
                                            { text: 'Delete', popoverText: deleteText, icon: 'delete', type: 'danger', onChoose: this.deleteAccount },
                                        ]}
                                        collapse={(w) => w >= sizes.MEDIUM} />
                                ] : null}
                            </Col>}
                            <AccountSettings
                                visible={this.state.showSettings}
                                onCancel={this.closeSettings}
                                account={this.account()}
                            />
                        </Row>
                    </div>
                    <Collapse bordered={false} activeKey={this.state.addTransfer ? 'add-transfer' : null}>
                        <Panel key="add-transfer" showArrow={false}>
                            <TransferCreate accountId={this.id()} onCreate={this.onTransferCreate}></TransferCreate>
                        </Panel>
                    </Collapse>
                    <div style={{ flex: '1 1 auto' }} className="transfer-list">
                        {this.props.screenWidth.width >= sizes.LARGE ? this.normalList() : this.tabbedLists()}
                    </div>
                </Col>
                {this.props.screenWidth.width >= sizes.LARGE ? <Col lg={6} xl={8} sm={24} style={{ height: '100%', display: 'flex', flexDirection: 'column' }}>
                    <h3 style={{ flex: '0 0 24px' }}>Recurring transfers</h3>
                    <div style={{ flex: '1 1 auto' }}>
                        {this.recurringList()}
                    </div>
                </Col> : null}
                <TransferEdit transfer={this.state.editingTransfer}
                    recurring={this.state.editingRecurring}
                    handleCancel={this.cancelEdit}></TransferEdit>
            </Row >
        )
    }
}
AccountDetails.propTypes = {
    accounts: PropTypes.object.isRequired,
    transfers: PropTypes.object.isRequired,
    autoTransfers: PropTypes.object.isRequired,
    screenWidth: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
    accounts: state.accounts,
    transfers: state.transfers,
    autoTransfers: state.autoTransfers,
    screenWidth: state.screenWidth
});

export default connect(mapStateToProps)(AccountDetails)
