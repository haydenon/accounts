import { message } from 'antd';
import { actions } from '../resource.actions';

export const accountShareActions = actions('account share');

export const createAccountShare = (account, share) => accountShareActions.createResource(
  () => message.success("Added sharing with user"),
  (err) => message.error(err.message, 6),
  () => `/api/account/${account.id}/share/`)(share);

export const deleteAccountShare = (account, share) => accountShareActions.deleteResource(
  () => message.success("Removed sharing with user"),
  (err) => message.error(err.message, 6),
  (s) => `/api/account/${account.id}/share/${s.id}/`)(share);

export const updateAccountShare = (account, original, updated) => accountShareActions.updateResource(
  () => message.success("Updated sharing with user"),
  (err) => message.error(err.message, 6),
  (s) => `/api/account/${account.id}/share/${s.id}/`)(original, updated);
