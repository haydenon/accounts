import { message } from 'antd';
import { actions } from '../resource.actions';

export const accountActions = actions('account');

export const ACCOUNT_LOAD = accountActions.LOAD
export const ACCOUNT_LOADED = accountActions.LOADED
export const ACCOUNT_LOAD_FAILED = accountActions.LOAD_FAILED

export const ACCOUNT_CREATE = accountActions.CREATE
export const ACCOUNT_CREATED = accountActions.CREATED
export const ACCOUNT_CREATE_FAILED = accountActions.CREATE_FAILED

export const accountLoad = accountActions.resourceLoad;
export const accountLoaded = accountActions.resourceLoaded;
export const accountLoadFailed = accountActions.resourceLoadFailed;

export const accountCreate = accountActions.resourceCreate;
export const accountCreated = accountActions.resourceCreated;
export const accountCreateFailed = accountActions.resourceCreateFailed;

export const loadAccounts = accountActions.loadResource(
    null,
    () => message.error('Failed to retrieve accounts', 3),
    () => `/api/account/`)

export const createAccount = accountActions.createResource(
    () => message.success("Created new account"),
    (err) => message.error(err.message, 6),
    () => `/api/account/`)

export const deleteAccount = accountActions.deleteResource(
    () => message.success("Successfully deleted account"),
    (err) => message.error(err.message, 6),
    (t) => `/api/account/${t.id}/`);

export const updateAccount = accountActions.updateResource(
    () => message.success("Successfully updated account"),
    (err) => message.error(err.message, 6),
    (t) => `/api/account/${t.id}/`);
