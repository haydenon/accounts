import React from 'react';
import { connect, Dispatch } from 'react-redux';

import moment from 'moment';

import { Select } from 'antd';
import { SelectValue } from 'antd/lib/select';
const SelectOption = Select.Option;

import { IAccount } from '../accounts/accounts';
import { loadAccounts } from '../accounts/accounts.actions';
import SummaryLineChart from '../charts/SummaryLineChart';
import { loadRates } from '../rates/rates.actions';
import { IRatesState } from '../rates/rates.reducer';
import { IResourceState } from '../resource.reducer';
import { ITransfersSummaryState } from '../transfers/transfersSummary.reducer';
import { createTransferSummaryHelper } from '../transfers/transferSummary.helper';
import { Option } from '../util';
import { createResourceHelper } from '../util/resourceHelper';

import './dashboard.scss';

interface IProps {
    accounts: IResourceState<IAccount>;
    transfersSummaries: ITransfersSummaryState;
    rates: IRatesState;
    dispatch: Dispatch<any>;
}

interface IState {
    selectedAccount?: number;
}

class Dashboard extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {};
    }

    public componentDidMount() {
        if (!this.accountHelper.loadedAll()) {
            this.props.dispatch(loadAccounts());
        }

        if (this.props.rates.expiry < moment()) {
            this.props.dispatch(loadRates());
        }

        if (!this.transfersSummaryHelper.transferSummaries().item) {
            const end = moment().startOf('day');
            const start = moment(end).subtract(1, 'month');

            this.transfersSummaryHelper.loadTransferSummaries(start, end, 'Day');
        }

        if (this.accountHelper.loadedAll()) {
            const firstAccount = Option.map(
                this.accountHelper.listAll(),
                (accounts) => accounts.length ? accounts[0].id : undefined
            );

            this.setState({
                selectedAccount: firstAccount
            });
        }
    }

    public componentDidUpdate() {
        if (!this.state.selectedAccount && this.accountHelper.loadedAll()) {
            const firstAccount = Option.map(
                this.accountHelper.listAll(),
                (accounts) => accounts.length ? accounts[0].id : undefined
            );

            this.setState({
                selectedAccount: firstAccount
            });
        }
    }

    public render() {
        const summaries = this.transfersSummaryHelper.transferSummaries();
        const accounts = this.accountHelper.listAll();
        if (!this.state.selectedAccount || !accounts || !summaries.item) {
            return null;
        }
        const selectedAccount = accounts.find((a) => a.id === this.state.selectedAccount);
        if (!selectedAccount) {
            return null;
        }

        const data = summaries.item.filter(
            (s) => s.accountId === this.state.selectedAccount);

        const end = moment().startOf('day');
        const start = moment(end).subtract(1, 'month');

        return (
            <div>
                <Select
                    size="small"
                    defaultValue={this.state.selectedAccount}
                    style={{ width: 250 }}
                    onChange={this.onAccountSelect}
                >
                    {this.chartAccounts().map((acc: { value: number; name: string; }) => (
                        <SelectOption key={acc.value} value={acc.value}>{acc.name}</SelectOption>)
                    )}
                </Select>
                <SummaryLineChart
                    summaries={data}
                    start={start}
                    end={end}
                    account={selectedAccount}
                />
            </div>
        );
    }

    private onAccountSelect = (value: SelectValue) => {
        if (typeof (value) === 'number') {
            this.setState({
                selectedAccount: value
            });
        }
    }

    private chartAccounts = () => (this.accountHelper.listAll() || []).map(((acc) => ({
        value: acc.id,
        name: acc.name,
    })))

    private get accountHelper() { return createResourceHelper(this.props.accounts)(); }
    private get transfersSummaryHelper() {
        return createTransferSummaryHelper(this.props.transfersSummaries, this.props.dispatch);
    }
}

const mapStateToProps = (state: any) => ({
    transfersSummaries: state.transfersSummaries,
    accounts: state.accounts,
    rates: state.rates,
});

export default connect(mapStateToProps)(Dashboard);
