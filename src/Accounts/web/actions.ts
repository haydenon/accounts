import { Action, Dispatch } from 'redux';

import { IState } from './reducer';
import { IResource, Relation, RelationKey, ResourceId } from './resource';

export interface IAction<T> extends Action {
  data: T;
}

export interface IErrorAction<T> extends Action {
  error: IActionError;
  data: T;
}

export interface IActionError {
  message: string;
}

export interface ILoadConfig {
  relation?: Relation;
  limit?: number;
  skip?: number;
}

export interface ILoadData<T extends IResource> {
  config: ILoadConfig;
}

export interface ILoadSuccessData<T extends IResource> extends ILoadData<T> {
  data: T[];
  total?: number;
}

export interface ILoadStartAction<T extends IResource> extends IAction<ILoadData<T>> { }
export interface ILoadSuccessAction<T extends IResource> extends IAction<ILoadSuccessData<T>> { }
export interface ILoadFailedAction<T extends IResource> extends IErrorAction<ILoadData<T>> { }

export interface ICreateStartAction<T extends IResource> extends IAction<T> { }
export interface ICreateSuccessAction<T extends IResource> extends IAction<T> { }
export interface ICreateFailedAction<T extends IResource> extends IErrorAction<T> { }

export interface IDeleteStartAction<T extends IResource> extends IAction<T> { }
export interface IDeleteSuccessAction<T extends IResource> extends IAction<T> { }
export interface IDeleteFailedAction<T extends IResource> extends IErrorAction<T> { }

export interface IUpdateData<T extends IResource> {
  original: T;
  update: T;
}

export interface IUpdateStartAction<T extends IResource> extends IAction<IUpdateData<T>> { }
export interface IUpdateSuccessAction<T extends IResource> extends IAction<IUpdateData<T>> { }
export interface IUpdateFailedAction<T extends IResource> extends IErrorAction<IUpdateData<T>> { }

export interface IAsyncActions<T, TResp, R> {
  start: (data: T) => IAction<T>;
  success: (data: TResp, start: T) => IAction<R>;
  error: (error: IActionError, data: T) => IErrorAction<T>;
}

type LoadResponse<T> = T[] | { resources: T[]; total: number; };
export interface ILoadAsyncActions<T extends IResource>
  extends IAsyncActions<ILoadData<T>, LoadResponse<T>, ILoadSuccessData<T>> { }
export interface ICreateAsyncActions<T extends IResource> extends IAsyncActions<T, T, T> { }
export interface IDeleteAsyncActions<T extends IResource> extends IAsyncActions<T, T, T> { }
export interface IUpdateAsyncActions<T extends IResource> extends IAsyncActions<IUpdateData<T>, T, IUpdateData<T>> { }

export const createAsyncActionOrchestration = <T, TResp, R>(
  action: (data: T, getState: () => IState) => Promise<TResp>,
  { start, success, error }: IAsyncActions<T, TResp, R>) =>
  (data: T) => {
    return (dispatch: Dispatch<Action>, getState: () => IState): Promise<R | IActionError> => {
      return new Promise((resolve, reject) => {
        dispatch(start(data));
        action(data, getState)
          .then((res) => {
            const successAction = success(res, data);
            dispatch(successAction);
            resolve(successAction.data);
          })
          .catch((err) => {
            const errorAction = error(err, data);
            dispatch(errorAction);
            reject({ error: errorAction.error, data: errorAction.data });
          });
      });
    };
  };

export const createAsyncOrchestrationWith = <T, TResp, R>(
  data: T,
  action: (data: T, getState: () => IState) => Promise<TResp>,
  actions: IAsyncActions<T, TResp, R>
) =>
  (dispatch: Dispatch<any>, getState: () => IState) => {
    return createAsyncActionOrchestration<T, TResp, R>(action, actions)(data)(dispatch, getState);
  };

export const onSuccess = <T, R>(
  action: (data: T, getState: () => IState) => Promise<R>,
  success: (response: R) => void
) =>
  (data: T, getState: () => IState) => action(data, getState)
    .then((response: R) => {
      success(response);
    });

export const onFailure = <T, R>(
  action: (data: T, getState: () => IState) => Promise<R>,
  failed: ({ error, data }: { error: IActionError, data: R }) => void
) =>
  (data: T, getState: () => IState) => action(data, getState)
    .catch((errData: { error: IActionError, data: R }) => {
      failed(errData);
    });

export const onFinish = <T, R>(
  action: (data: T, getState: () => IState) => Promise<R>,
  success: (response: R) => void,
  failed: ({ error, data }: { error: IActionError, data: R }) => void
) =>
  (data: T, getState: () => IState) => action(data, getState)
    .then((response: R) => {
      success(response);
    })
    .catch((errData: { error: IActionError, data: R }) => {
      failed(errData);
    });
