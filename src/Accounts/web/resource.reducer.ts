import { Action, AnyAction } from 'redux';

import { SIGNED_OUT } from './auth/auth.actions';
import { IResource, Relation, RelationKey, ResourceId } from './resource';
import { DEFAULT_ASYNC_STATE, IAsyncState, IStringErrorAction } from './util/reducerHelpers';

interface IResourceAsyncState {
    [id: number]: IAsyncState;
}

export interface IBaseLoadState {
    paged?: ResourceId[];
    total?: number;
    fetching: boolean;
    error?: string;
}

interface ILoadState extends IBaseLoadState {
    relations: {
        [relation: string]: {
            [id: number]: IBaseLoadState;
        }
    };
}

export interface IResourceState<T extends IResource> {
    resources: T[];
    load: ILoadState;
    create: IAsyncState;
    delete: IResourceAsyncState;
    update: IResourceAsyncState;
}

interface ILoadConfig {
    relation?: Relation;
    limit?: number;
    skip?: number;
}

interface IResourceAction<T extends IResource> extends AnyAction {
    config: ILoadConfig;
}

interface ILoadAction<T extends IResource> extends IResourceAction<T> {
    resources: T[];
    total?: number;
}

interface ISingleAction<T extends IResource> extends IResourceAction<T> {
    resource: T;
}

interface IUpdateAction<T extends IResource> extends ISingleAction<T> {
    updated: T;
}

interface IResourceErrorAction<T extends IResource> extends IStringErrorAction, IResourceAction<T> {
    config: ILoadConfig;
}

interface ISingleErrorAction<T extends IResource> extends IStringErrorAction, ISingleAction<T> { }

interface IUpdateErrorAction<T extends IResource> extends IStringErrorAction, IUpdateAction<T> { }

interface IResourceActions {
    LOAD: string;
    LOADED: string;
    LOAD_FAILED: string;
    CREATE: string;
    CREATED: string;
    CREATE_FAILED: string;
    UPDATE: string;
    UPDATED: string;
    UPDATE_FAILED: string;
    DELETE: string;
    DELETED: string;
    DELETE_FAILED: string;
}

export const updateForLoad =
    <T extends IResource>(object: IResourceState<T>, config: ILoadConfig): IResourceState<T> => {
        if (!config.relation) {
            return object;
        }

        const keyParent = object.load.relations[config.relation.key];

        return {
            ...object,
            load: {
                ...object.load,
                relations: {
                    ...object.load.relations,
                    [config.relation.key]: {
                        ...object.load.relations[config.relation.key],
                        [config.relation.value]: {
                            paged: keyParent[config.relation.value] && keyParent[config.relation.value].paged || [],
                            fetching: true,
                            error: undefined
                        }
                    }
                }
            }
        };
    };

const setPaging = (parent: IBaseLoadState, resources: IResource[], start: number, numItems: number, total: number) => {
    start = start || 0;
    const newPaged = parent.paged ? [...parent.paged] : [];
    const max = Math.min(numItems, total - start);
    for (let i = 0; i < max; i++) {
        newPaged[start + i] = resources[i].id;
    }

    parent.paged = newPaged;
};

const forAllRelations = <T extends IResource>(
    state: IResourceState<T>,
    relations: RelationKey[],
    f: (childState: IBaseLoadState, relation: RelationKey, relationId: number) => void
) => {
    for (const relation of relations) {
        for (const childId of Object.keys(state.load.relations[relation]).map((id) => parseInt(id, 10))) {
            const child = state.load.relations[relation][childId];
            if (child) f(child, relation, childId);
        }
    }
};

export const removePaged = <T extends IResource>(state: IResourceState<T>, relations: RelationKey[]) => {
    state.load.paged = [];
    forAllRelations(state, relations, (child) => child.paged = []);
    return state;
};

export const addToPaged = <T extends IResource>(
    state: IResourceState<T>,
    relations: RelationKey[],
    resources: IResource[],
    resource: IResource,
    ordering: (r1: IResource, r2: IResource) => number,
    relationIdPred: (relation: RelationKey, relationId: ResourceId) => boolean
) => {
    if (!ordering) {
        return state;
    }

    const replacePaged = (parent: IBaseLoadState) => {
        // TODO: Need to change the initialisation value, so we can tell the
        // difference between items that haven't been loaded yet to items that
        // have been loaded, but have no items.
        if (parent.paged === undefined) {
            return;
        }
        let indx = parent.paged.findIndex((id: ResourceId) => {
            return id !== undefined && ordering(resource, resources.find((item) => item.id === id) as IResource) <= 0;
        });

        // If we can find it's position inbetween loaded items, or at the very ends of the items we can guarantee that
        // we can correctly store the position of the new item. If we can't find it's position, we need to flush the
        // order cache and reload the pages.
        if (indx === 0
            || (indx === -1 && parent.paged.length === parent.total)
            || parent.paged[indx - 1] !== undefined) {
            if (indx === -1) indx = parent.paged.length;
            parent.paged =
                [...parent.paged.slice(0, indx), resource.id, ...parent.paged.slice(indx, parent.paged.length)];
        } else {
            parent.paged = [];
        }
    };

    replacePaged(state.load);

    forAllRelations(state, relations, (child, relation, key) => {
        if (relationIdPred(relation, key) && child.paged) {
            replacePaged(child);
        }
    });

    return state;
};

export const updateTotal = <T extends IResource>(
    state: IResourceState<T>,
    relations: RelationKey[],
    increment: boolean,
    relationIdPred: (relation: RelationKey, relationId: ResourceId) => boolean
) => {
    const incr = (increment ? 1 : -1);
    if (state.load.total !== undefined && state.load.total !== null) {
        state.load.total = state.load.total || 0;
        state.load.total = Math.max(0, state.load.total + incr);
    }

    forAllRelations(state, relations, (child, relation, key) => {
        if (relationIdPred(relation, key)) child.total = Math.max(0, (child.total || 0) + incr);
    });

    return state;
};

export const updateForLoaded = <T extends IResource>(
    object: IResourceState<T>,
    config: ILoadConfig,
    resources: T[],
    total: number
): IResourceState<T> => {
    const others = object.resources.filter((r) => resources.every((loaded) => r.id !== loaded.id));
    object.resources = [...others, ...resources];

    if (!config.relation) {
        object.load.total = total ? total : object.resources.length;
        if (config.skip !== undefined || config.limit !== undefined) {
            setPaging(object.load, resources, config.skip || 0, config.limit || 0, total);
        } else {
            object.load.paged = object.resources.map((r) => r.id);
        }

        return object;
    }

    const obj: IResourceState<T> = {
        ...object,
        load: {
            ...object.load,
            relations: {
                ...object.load.relations,
                [config.relation.key]: {
                    ...object.load.relations[config.relation.key],
                    [config.relation.value]: {
                        ...object.load.relations[config.relation.key][config.relation.value],
                        total: total || resources.length,
                        fetching: false,
                    }
                }
            }
        }
    };

    if (config.skip !== undefined || config.limit !== undefined) {
        setPaging(
            obj.load.relations[config.relation.key][config.relation.value],
            resources,
            config.skip || 0,
            config.limit || 0,
            total
        );
    }

    return obj;
};

export const updateForLoadFailed = <T extends IResource>(
    object: IResourceState<T>,
    config: ILoadConfig,
    error: string
): IResourceState<T> => {
    if (!config.relation) {
        object.load.error = error;
        return object;
    }

    return {
        ...object,
        load: {
            ...object.load,
            relations: {
                ...object.load.relations,
                [config.relation.key]: {
                    ...object.load.relations[config.relation.key],
                    [config.relation.value]: {
                        ...object.load.relations[config.relation.key][config.relation.value],
                        fetching: false,
                        error
                    }
                }
            }
        }
    };
};

type CustomHandler<T extends IResource> = (
    state: IResourceState<T>,
    action: IResourceAction<T>,
    originalReducer: (state: IResourceState<T>) => IResourceState<T>
) => IResourceState<T>;

interface IReducerOptions<T extends IResource> {
    relations?: RelationKey[];
    customActionHandler?: {
        [type: string]: CustomHandler<T>;
    };
    ordering?: (r1: IResource, r2: IResource) => number;
}

export const reducer = <T extends IResource>(resourceActions: IResourceActions, options: IReducerOptions<T> = {}) => {
    const customActionHandler = options.customActionHandler || {};
    const relations = options.relations || [];

    const originalState: IResourceState<T> = {
        resources: [],
        load: {
            fetching: false,
            relations: {}
        },
        create: DEFAULT_ASYNC_STATE,
        delete: {},
        update: {}
    };

    for (const relation of relations) {
        originalState.load.relations[relation] = {};
    }

    const reducerActions = (state: IResourceState<T>, action: IResourceAction<T>): IResourceState<T> => {
        switch (action.type) {
            case resourceActions.LOAD:
                return updateForLoad({
                    ...state,
                    load: {
                        ...state.load,
                        fetching: true,
                        error: undefined
                    },
                }, action.config);
            case resourceActions.LOADED: {
                const act = (action as ILoadAction<T>);
                return updateForLoaded({
                    ...state,
                    load: {
                        ...state.load,
                        fetching: false
                    }
                }, act.config, act.resources, act.total || 0);
            }
            case resourceActions.LOAD_FAILED: {
                const act = (action as IResourceErrorAction<T>);
                return updateForLoadFailed({
                    ...state,
                    load: {
                        ...state.load,
                        fetching: false
                    }
                }, act.config, act.error);
            }
            case resourceActions.CREATE:
                return {
                    ...state,
                    create: {
                        ...state.create,
                        fetching: true,
                        error: undefined
                    }
                };
            case resourceActions.CREATED: {
                const act = (action as ISingleAction<T>);
                let newState = {
                    ...state,
                    resources: [...state.resources, act.resource],
                    create: {
                        ...state.create,
                        fetching: false,
                    }
                };
                if (options.ordering) {
                    newState = addToPaged(newState,
                        relations,
                        state.resources,
                        act.resource,
                        options.ordering,
                        (relation, rId) => (act.resource as any)[relation] === rId
                    );
                }
                return updateTotal(
                    newState,
                    relations,
                    true,
                    (relation, rId) => (act.resource as any)[relation] === rId
                );
            }
            case resourceActions.CREATE_FAILED: {
                const act = (action as IResourceErrorAction<T>);
                return {
                    ...state,
                    create: {
                        ...state.create,
                        fetching: false,
                        error: act.error
                    }
                };
            }
            case resourceActions.DELETE: {
                const act = (action as ISingleAction<T>);
                return {
                    ...state,
                    delete: {
                        ...state.delete,
                        [act.resource.id]: {
                            fetching: true,
                            error: undefined
                        }
                    }
                };
            }
            case resourceActions.DELETED: {
                const act = (action as ISingleAction<T>);
                return updateTotal(removePaged({
                    ...state,
                    resources: state.resources.filter((r) => r.id !== act.resource.id),
                    delete: {
                        ...state.delete,
                        [act.resource.id]: {
                            fetching: false,
                        }
                    }
                }, relations), relations, false, (relation, rId) => (act.resource as any)[relation] === rId);
            }
            case resourceActions.DELETE_FAILED: {
                const act = (action as ISingleErrorAction<T>);
                return {
                    ...state,
                    delete: {
                        ...state.delete,
                        [act.resource.id]: {
                            fetching: false,
                            error: act.error
                        }
                    }
                };
            }
            case resourceActions.UPDATE: {
                const act = (action as IUpdateAction<T>);
                return {
                    ...state,
                    update: {
                        ...state.update,
                        [act.updated.id]: {
                            fetching: true,
                            error: undefined
                        }
                    }
                };
            }
            case resourceActions.UPDATED: {
                const act = (action as IUpdateAction<T>);
                const res = state.resources;
                const index = res.findIndex((i) => i.id === act.updated.id);

                return {
                    ...state,
                    resources: [...res.slice(0, index), act.updated, ...res.slice(index + 1, res.length)],
                    update: {
                        ...state.update,
                        [act.updated.id]: {
                            fetching: false,
                        }
                    }
                };
            }
            case resourceActions.UPDATE_FAILED: {
                const act = (action as IUpdateErrorAction<T>);
                return {
                    ...state,
                    update: {
                        ...state.update,
                        [act.updated.id]: {
                            fetching: false,
                            error: act.error
                        }
                    }
                };
            }
            case SIGNED_OUT:
                return originalState;
            default:
                return state;
        }
    };

    return (state = originalState, action: IResourceAction<T>) => {
        if (customActionHandler && customActionHandler[action.type]) {
            const handler = customActionHandler[action.type];
            return handler(state, action, (resultingState) => reducerActions(resultingState, action));
        }

        return reducerActions(state, action);
    };
};
