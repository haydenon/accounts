import { Action, Dispatch } from 'redux';

const getScreenWidth = () => Math.max(
  document.documentElement ? document.documentElement.clientWidth : 0,
  window.innerWidth || 0
);

export const SCREEN_WIDTH_UPDATE = '[screen] Update width';

export const screenWidthUpdate = (width: number) => ({
  type: SCREEN_WIDTH_UPDATE,
  width
});

// Over 200 to avoid errors being thrown by ant design's tab resizes
const WAIT_TIME = 210;
let timeout = 0;

export const updateScreenWidth = () => (dispatch: Dispatch<any>) => {
  if (timeout) {
    clearTimeout(timeout);
  }
  timeout = window.setTimeout(() => {
    dispatch(screenWidthUpdate(getScreenWidth()));
  }, WAIT_TIME);
};

interface IWidthAction extends Action {
  width: number;
}

export interface IScreenState {
  width: number;
}

export function screenWidth(
  state = { width: getScreenWidth() },
  action: IWidthAction
): IScreenState {
  switch (action.type) {
    case SCREEN_WIDTH_UPDATE:
      return {
        width: action.width
      };
    default:
      return state;
  }
}

export const sizes = {
  SMALL: 576,
  MEDIUM: 768,
  LARGE: 992,
  XLARGE: 1200,
  XXLARGE: 1600
};
