import React from 'react';
import { connect } from 'react-redux';

import { Button, Form, Icon, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { SelectValue } from 'antd/lib/select';
const FormItem = Form.Item;
const Option = Select.Option;

import { IRatesState } from '../rates/rates.reducer';
import { validateSubmit } from '../util';
import { ISettingsState } from './settings.reducer';

interface IProps extends FormComponentProps {
    onSubmit: (currencies: string[]) => void;
    settings: ISettingsState;
    rates: IRatesState;
    currencies?: string[];
    disabled?: boolean;
    onChange?: (currencies: string[]) => void;
}

class CurrenciesForm extends React.Component<IProps> {
    private handleSubmit = validateSubmit(this.props.form, (currencySelection: { currencies: string[]; }) => {
        currencySelection.currencies = currencySelection.currencies || [];
        this.props.onSubmit(currencySelection.currencies);
    });

    public componentDidMount() {
        if (this.props.currencies) {
            this.setCurrencies(this.props.currencies);
        }
    }

    public componentDidUpdate(prevProps: IProps) {
        if (this.props.currencies && this.props.currencies !== prevProps.currencies) {
            this.setCurrencies(this.props.currencies);
        }
    }

    public render() {
        const { getFieldDecorator } = this.props.form;
        const fetching = this.props.settings.currenciesFetching;
        const fetchingRates = this.props.rates.isFetching;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem>
                    {getFieldDecorator('currencies')(
                        <Select
                            mode="multiple"
                            placeholder="Select currencies"
                            disabled={fetching || fetchingRates}
                            onChange={this.onChange}
                        >
                            {this.currencies().map((curr) => <Option key={curr} value={curr}>{curr}</Option>)}
                        </Select>
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="register-form-button"
                        loading={fetching || fetchingRates}
                        disabled={this.props.disabled}
                    >
                        Set currencies
                    </Button>
                </FormItem>
            </Form>);
    }

    private onChange = (changes: SelectValue) => {
        if (this.props.onChange) {
            const values = changes.valueOf();
            const currencies = values instanceof Array
                ? values
                : [values];
            this.props.onChange(currencies);
        }
    }

    private setCurrencies = (currencies: string[]) => {
        this.props.form.setFieldsValue({
            currencies,
        });
    }

    private currencies = () => {
        if (!this.props.rates.data) return [];

        return this.props.rates.names.sort();
    }
}

const mapStateToProps = (state: any) => ({
    settings: state.settings,
    rates: state.rates,
});

export default connect(mapStateToProps)(Form.create()(CurrenciesForm));
