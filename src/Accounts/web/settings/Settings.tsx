import React, { ChangeEvent } from 'react';
import { connect, Dispatch } from 'react-redux';

import moment from 'moment';

import { Button, Collapse, Form, Icon, Input, message, Select } from 'antd';
import { FormComponentProps } from 'antd/lib/form';
import { SelectValue } from 'antd/lib/select';
const Panel = Collapse.Panel;
const FormItem = Form.Item;
const Option = Select.Option;

import { IAuthState, IUser } from '../auth/auth.reducer';
import { loadRates } from '../rates/rates.actions';
import { IRatesState } from '../rates/rates.reducer';
import { validateSubmit } from '../util';
import { createTimezoneHelper } from '../util/stateHelpers';
import CurrenciesForm from './CurrenciesForm';
import { loadTimezones, updateTimezone, updateUser, updateUserCurrencies } from './settings.actions';
import { ISettingsState } from './settings.reducer';

import './settings.scss';

const currenciesChanged = (a: string[], b: string[]) => {
    return a.length !== b.length
        || b.some((c) => a.findIndex((cc: string) => cc === c) === -1);
};

interface IProps {
    settings: ISettingsState;
    rates: IRatesState;
    auth: IAuthState;
    dispatch: Dispatch<any>;
}

interface IState {
    currencies: string[];
    currenciesChanged: boolean;
    timezoneChanged: boolean;
    userChanged: boolean;
}

interface ITZProps extends FormComponentProps {
    onSubmit: (timezone: string) => void;
    settings: ISettingsState;
    auth: IAuthState;
    disabled?: boolean;
    onChange?: (timezone: string) => void;
}

const mapTimezoneState = (state: any) => ({
    settings: state.settings,
    auth: state.auth,
});

const TimezoneForm = connect(mapTimezoneState)(Form.create()(class extends React.Component<ITZProps> {
    private handleSubmit = validateSubmit(this.props.form, ({ timezone }: { timezone: string }) => {
        this.props.onSubmit(timezone);
    });

    public render() {
        const { getFieldDecorator } = this.props.form;
        const fetchingZones = this.props.settings.timezonesFetching;
        const fetching = this.props.settings.timezoneUpdate.fetching;
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem>
                    {getFieldDecorator('timezone', {
                        initialValue: this.timezoneHelper.userTimezone().value,
                        validateTrigger: 'onBlur',
                        rules: [{ required: true, message: 'Please select a timezone' }],
                    })(
                        <Select
                            placeholder="Select timezone"
                            disabled={fetchingZones}
                            onChange={this.onChange}
                        >
                            {this.timezoneHelper.timezones()
                                .map((timezone) =>
                                    <Option key={timezone.value} value={timezone.value}>{timezone.display}</Option>
                                )}
                        </Select>
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="register-form-button"
                        loading={fetching || fetchingZones}
                        disabled={this.props.disabled}
                    >
                        Set timezone
                    </Button>
                </FormItem>
            </Form>
        );
    }

    private get timezoneHelper() { return createTimezoneHelper(this.props.settings, this.props.auth); }

    private onChange = (change: SelectValue) => {
        if (this.props.onChange) {
            const value = change.valueOf();
            const timezone = value instanceof Array
                ? value[0]
                : value;
            this.props.onChange(timezone);
        }
    }
}));

interface IUserProps extends FormComponentProps {
    onSubmit: (user: IUser) => void;
    auth: IAuthState;
    settings: ISettingsState;
    disabled?: boolean;
    onChange?: (user: IUser) => void;
}

const mapUserState = (state: any) => ({
    auth: state.auth,
    settings: state.settings,
});

const UserForm = connect(mapUserState)(Form.create()(class extends React.Component<IUserProps> {
    private handleSubmit = validateSubmit(this.props.form, ({ displayName }: { displayName: string }) => {
        if (!this.props.auth.user) {
            return;
        }

        const user = {
            ...this.props.auth.user,
            displayName,
        };
        this.props.onSubmit(user);
    });

    public render() {
        const { getFieldDecorator } = this.props.form;
        const fetching = this.props.settings.userUpdate.fetching;
        const initialValue = this.props.auth.user
            ? this.props.auth.user.displayName
            : '';
        return (
            <Form onSubmit={this.handleSubmit}>
                <FormItem>
                    {getFieldDecorator('displayName', {
                        initialValue,
                        validateTrigger: 'onBlur',
                        rules: [{ required: true, message: 'Please provide a display name' }],
                    })(
                        <Input placeholder="Display name" onChange={this.onChange} />
                    )}
                </FormItem>
                <FormItem>
                    <Button
                        type="primary"
                        htmlType="submit"
                        className="register-form-button"
                        loading={fetching}
                        disabled={this.props.disabled}
                    >
                        Set display name
                    </Button>
                </FormItem>
            </Form>
        );
    }

    private onChange = (change: ChangeEvent<HTMLInputElement>) => {
        if (this.props.onChange && this.props.auth.user) {
            const value = change.target.value;
            const user = {
                ...this.props.auth.user,
                displayName: value,
            };
            this.props.onChange(user);
        }
    }
}));

class Settings extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
        this.state = {
            currencies: [],
            userChanged: false,
            timezoneChanged: false,
            currenciesChanged: false,
        };
    }

    public componentDidMount() {
        if (this.props.rates.expiry < moment()) {
            this.props.dispatch(loadRates());
        }

        if (!this.props.settings.timezones.length) {
            this.props.dispatch(loadTimezones(undefined));
        }

        if (this.props.auth.user) {
            this.setState({
                currencies: this.props.auth.user.commonCurrencies
            });
        }
    }

    public componentDidUpdate(_: IProps, prevState: IState) {
        if (this.props.auth.user && prevState.currencies !== this.props.auth.user.commonCurrencies) {
            this.setState({
                currencies: this.props.auth.user.commonCurrencies
            });
        }
    }

    public render() {
        return (
            <div className="accounts-settings">
                <Collapse bordered={false}>
                    <Panel
                        header={
                            <span>
                                <Icon className="settings-icon" type="pay-circle-o" />Common currencies
                        </span>
                        }
                        key="currencies"
                    >
                        <div style={{ maxWidth: 550, padding: 25 }}>
                            <CurrenciesForm
                                onSubmit={this.updateCurrencies}
                                currencies={this.state.currencies}
                                disabled={!this.state.currenciesChanged}
                                onChange={this.checkCurrencies}
                            />
                        </div>
                    </Panel>
                    <Panel
                        header={
                            <span>
                                <Icon className="settings-icon" type="user" />User details
                        </span>
                        }
                        key="timezone"
                    >
                        <div style={{ maxWidth: 550, padding: 25 }}>
                            <TimezoneForm
                                onSubmit={this.updateTimezone}
                                disabled={!this.state.timezoneChanged}
                                onChange={this.checkTimezone}
                            />
                        </div>
                        <div style={{ maxWidth: 550, padding: 25 }}>
                            <UserForm
                                onSubmit={this.updateUser}
                                disabled={!this.state.userChanged}
                                onChange={this.checkUser}
                            />
                        </div>
                    </Panel>
                </Collapse>
            </div>
        );
    }

    private checkCurrencies = (currencies: string[]) => {
        if (this.props.auth.user) {
            const user = this.props.auth.user;
            this.setState({
                currenciesChanged: currenciesChanged(user.commonCurrencies, currencies)
            });
        }
    }

    private checkTimezone = (timezone: string) => {
        if (this.props.auth.user) {
            const user = this.props.auth.user;
            this.setState({
                timezoneChanged: timezone !== user.timezone
            });
        }
    }

    private checkUser = (changed: IUser) => {
        if (this.props.auth.user) {
            const user = this.props.auth.user;
            this.setState({
                userChanged: user.displayName !== changed.displayName
            });
        }
    }

    private updateTimezone = (timezone: string) => {
        if (!this.state.timezoneChanged) {
            return;
        }

        this.props.dispatch(updateTimezone(timezone))
            .then(() => {
                if (!this.props.settings.timezoneUpdate.error) {
                    message.success('Updated timezone successfully');
                    this.setState({ timezoneChanged: false });
                }
            });
    }

    private updateCurrencies = (currencies: string[]) => {
        if (!this.state.currenciesChanged) {
            return;
        }

        this.props.dispatch(updateUserCurrencies({ currencies }))
            .then(() => {
                if (!this.props.settings.currenciesError) {
                    message.success('Updated currencies successfully');
                    this.setState({ currenciesChanged: false });
                }
            });
    }

    private updateUser = (user: IUser) => {
        if (!this.state.userChanged) {
            return;
        }

        this.props.dispatch(updateUser(user))
            .then(() => {
                if (!this.props.settings.userUpdate.error) {
                    message.success('Updated user successfully');
                    this.setState({ userChanged: false });
                }
            });
    }
}

const mapStateToProps = (state: any) => ({
    settings: state.settings,
    auth: state.auth,
    rates: state.rates,
});

export default connect(mapStateToProps)(Settings);
