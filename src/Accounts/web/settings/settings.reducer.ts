import { Action } from 'redux';

import { IActionError } from '../actions';
import { asyncError, asyncInitState, asyncInProgress, DEFAULT_ASYNC_STATE, IAsyncState } from '../util/reducerHelpers';
import {
    ConfigurationLoadError,
    ConfigurationLoadStart,
    ConfigurationLoadSuccess,
    SettingActions,
    TIMEZONE_UPDATE,
    TIMEZONE_UPDATE_FAILED,
    TIMEZONE_UPDATED,
    TIMEZONES_LOAD,
    TIMEZONES_LOAD_FAILED,
    TIMEZONES_LOADED,
    UPDATE_CURRENCIES,
    UPDATE_CURRENCIES_FAILED,
    UPDATED_CURRENCIES,
    USER_UPDATE,
    USER_UPDATE_FAILED,
    USER_UPDATED
} from './settings.actions';

export interface ITimezone {
    display: string;
    offset: string;
    timeZone: string;
}

export interface ISettingsState {
    timezonesError?: IActionError;
    timezonesFetching: boolean;
    timezones: ITimezone[];
    timezoneUpdate: IAsyncState;
    updateCurrencies: string[];
    currenciesError?: IActionError;
    currenciesFetching: boolean;
    user?: {
        commonCurrencies: string[];
    };
    userUpdate: IAsyncState;
    configurationLoad: IAsyncState;
    configuration?: Configuration;
}

const ORIGINAL_STATE: ISettingsState = {
    timezonesFetching: false,
    timezones: [],
    timezoneUpdate: {
        fetching: false
    },
    updateCurrencies: [],
    currenciesFetching: false,
    configurationLoad: DEFAULT_ASYNC_STATE,
    userUpdate: DEFAULT_ASYNC_STATE,
};

interface ITimezonesAction extends Action {
    timezones: string[];
}

interface IErrorAction extends Action {
    error: string;
}

interface ICurrenciesAction extends Action {
    currencies: string[];
}

export type Configuration = { [key: string]: any };

export function settings(
    state = ORIGINAL_STATE,
    action: SettingActions,
): ISettingsState {
    switch (action.type) {
        case TIMEZONES_LOAD:
            return {
                ...state,
                timezones: [],
                timezonesFetching: true,
                timezonesError: undefined
            };
        case TIMEZONES_LOADED: {
            return {
                ...state,
                timezones: action.data,
                timezonesFetching: false
            };
        }
        case TIMEZONES_LOAD_FAILED:
            return {
                ...state,
                timezonesFetching: false,
                timezonesError: action.error
            };
        case TIMEZONE_UPDATE:
            return {
                ...state,
                timezoneUpdate: {
                    fetching: true,
                    error: undefined,
                }
            };
        case TIMEZONE_UPDATED:
            return {
                ...state,
                timezoneUpdate: {
                    fetching: false,
                }
            };
        case TIMEZONE_UPDATE_FAILED:
            return {
                ...state,
                timezoneUpdate: {
                    fetching: false,
                    error: action.error.message,
                }
            };
        case UPDATE_CURRENCIES:
            return {
                ...state,
                updateCurrencies: action.data.currencies,
                currenciesFetching: true,
                currenciesError: undefined
            };
        case UPDATED_CURRENCIES:
            return {
                ...state,
                user: {
                    ...state.user,
                    commonCurrencies: state.updateCurrencies
                },
                currenciesFetching: false,
                updateCurrencies: []
            };
        case UPDATE_CURRENCIES_FAILED:
            return {
                ...state,
                currenciesFetching: false,
                currenciesError: action.error,
                updateCurrencies: []
            };
        case USER_UPDATE:
            return {
                ...state,
                userUpdate: {
                    ...state.userUpdate,
                    fetching: true,
                    error: undefined
                }
            };
        case USER_UPDATED:
            return {
                ...state,
                userUpdate: {
                    ...state.userUpdate,
                    fetching: false,
                }
            };
        case USER_UPDATE_FAILED:
            return {
                ...state,
                userUpdate: {
                    ...state.userUpdate,
                    fetching: false,
                    error: action.error.message
                }
            };
        case ConfigurationLoadStart:
            return {
                ...state,
                configurationLoad: asyncInProgress(state.configurationLoad)
            };
        case ConfigurationLoadSuccess:
            return {
                ...state,
                configuration: action.data,
                configurationLoad: asyncInitState
            };
        case ConfigurationLoadError:
            return {
                ...state,
                configurationLoad: asyncError(action.error.message)
            };
        default:
            return state;
    }
}
