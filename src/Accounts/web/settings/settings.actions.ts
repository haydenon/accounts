import { message } from 'antd';
import { Action, Dispatch } from 'redux';

import { createAsyncActionOrchestration, IAction, IActionError, IAsyncActions, IErrorAction } from '../actions';
import { IUser } from '../auth/auth.reducer';
import { IState } from '../reducer';
import { get, post, put } from '../util';
import { Configuration, ITimezone } from './settings.reducer';

type SettingsActionType =
    | '[Configuration] load start'
    | '[Configuration] load success'
    | '[Configuration] load error'
    | '[timezones] load'
    | '[timezones] loaded'
    | '[timezones] load failed'
    | '[timezones] update'
    | '[timezones] updated'
    | '[timezones] updated failed'
    | 'UPDATE_CURRENCIES'
    | 'UPDATED_CURRENCIES'
    | 'UPDATE_CURRENCIES_FAILED'
    | '[user] update'
    | '[user] updated'
    | '[user] update failed';

interface ISettingsAction extends Action {
    type: SettingsActionType;
}

type Currencies = { currencies: string[] };

export const TIMEZONES_LOAD = '[timezones] load';
export const TIMEZONES_LOADED = '[timezones] loaded';
export const TIMEZONES_LOAD_FAILED = '[timezones] load failed';

interface ITimezonesLoadStart extends ISettingsAction, IAction<void> { type: '[timezones] load'; }
interface ITimezonesLoadSuccess extends ISettingsAction, IAction<ITimezone[]> { type: '[timezones] loaded'; }
interface ITimezonesLoadError extends ISettingsAction, IErrorAction<void> { type: '[timezones] load failed'; }
type TimezonesLoadActions = ITimezonesLoadStart | ITimezonesLoadSuccess | ITimezonesLoadError;

export const TIMEZONE_UPDATE = '[timezones] update';
export const TIMEZONE_UPDATED = '[timezones] updated';
export const TIMEZONE_UPDATE_FAILED = '[timezones] updated failed';

interface ITimezonesUpdateStart extends ISettingsAction, IAction<string> { type: '[timezones] update'; }
export interface ITimezonesUpdateSuccess extends ISettingsAction, IAction<string> { type: '[timezones] updated'; }
interface ITimezonesUpdateError extends ISettingsAction, IErrorAction<string> { type: '[timezones] updated failed'; }
type TimezonesUpdateActions = ITimezonesUpdateStart | ITimezonesUpdateSuccess | ITimezonesUpdateError;

export const UPDATE_CURRENCIES = 'UPDATE_CURRENCIES';
export const UPDATED_CURRENCIES = 'UPDATED_CURRENCIES';
export const UPDATE_CURRENCIES_FAILED = 'UPDATE_CURRENCIES_FAILED';

export interface ICurrenciesUpdateStart extends ISettingsAction, IAction<Currencies> { type: 'UPDATE_CURRENCIES'; }
interface ICurrenciesUpdateSuccess extends ISettingsAction, IAction<void> { type: 'UPDATED_CURRENCIES'; }
interface ICurrenciesUpdateError extends ISettingsAction, IErrorAction<Currencies> { type: 'UPDATE_CURRENCIES_FAILED'; }
type CurrenciesUpdateActions = ICurrenciesUpdateStart | ICurrenciesUpdateSuccess | ICurrenciesUpdateError;

export const USER_UPDATE = '[user] update';
export const USER_UPDATED = '[user] updated';
export const USER_UPDATE_FAILED = '[user] update failed';

interface IUserUpdateStart extends ISettingsAction, IAction<IUser> { type: '[user] update'; }
export interface IUserUpdateSuccess extends ISettingsAction, IAction<IUser> { type: '[user] updated'; }
interface IUserUpdateError extends ISettingsAction, IErrorAction<IUser> { type: '[user] update failed'; }
type UserUpdateActions = IUserUpdateStart | IUserUpdateSuccess | IUserUpdateError;

export const ConfigurationLoadStart = '[Configuration] load start';
export const ConfigurationLoadSuccess = '[Configuration] load success';
export const ConfigurationLoadError = '[Configuration] load error';

interface IConfigurationLoadStart extends ISettingsAction, IAction<void> { type: '[Configuration] load start'; }
interface IConfigurationLoadSuccess extends ISettingsAction, IAction<Configuration> {
    type: '[Configuration] load success';
}
interface IConfigurationLoadError extends ISettingsAction, IErrorAction<void> { type: '[Configuration] load error'; }
type ConfigLoadActions = IConfigurationLoadStart | IConfigurationLoadSuccess | IConfigurationLoadError;

export type SettingActions =
    | ConfigLoadActions
    | TimezonesLoadActions
    | TimezonesUpdateActions
    | CurrenciesUpdateActions
    | UserUpdateActions;

const TimezonesLoadActions: IAsyncActions<void, ITimezone[], ITimezone[]> = {
    start: () => ({ type: TIMEZONES_LOAD, data: undefined }),
    success: (data) => ({ type: TIMEZONES_LOADED, data }),
    error: (error, data) => ({ type: TIMEZONES_LOAD_FAILED, error, data }),
};

export const loadTimezones = createAsyncActionOrchestration<void, ITimezone[], ITimezone[]>(
    () => get('/api/settings/timezone'), TimezonesLoadActions
);

const CurrencyUpdateActions: IAsyncActions<Currencies, void, void> = {
    start: (data) => ({ type: UPDATE_CURRENCIES, data }),
    success: () => ({ type: UPDATED_CURRENCIES, data: undefined }),
    error: (error, data) => ({ type: UPDATE_CURRENCIES_FAILED, error, data }),
};

export const updateUserCurrencies = (data: Currencies) => (dispatch: Dispatch<any>, getState: () => IState) => {
    const created = createAsyncActionOrchestration<Currencies, void, void>(
        () => post(`/api/settings/currencies`, data), CurrencyUpdateActions
    )(data)(dispatch, getState);
    created.catch((err) => message.error(err.message, 6));
    return created;
};

const UpdateTimezoneActions: IAsyncActions<string, string, string> = {
    start: (data) => ({ type: TIMEZONE_UPDATE, data }),
    success: (data) => ({ type: TIMEZONE_UPDATED, data }),
    error: (error, data) => ({ type: TIMEZONE_UPDATE_FAILED, error, data }),
};

export const updateTimezone = (data: string) => (dispatch: Dispatch<any>, getState: () => IState) => {
    const created = createAsyncActionOrchestration<string, string, string>(
        () => post(`/api/settings/timezone`, { timezone: data }), UpdateTimezoneActions
    )(data)(dispatch, getState);
    created.catch((err) => message.error(err.message, 6));
    return created;
};

const UpdateUserActions: IAsyncActions<IUser, IUser, IUser> = {
    start: (data) => ({ type: USER_UPDATE, data }),
    success: (data) => ({ type: USER_UPDATED, data }),
    error: (error, data) => ({ type: USER_UPDATE_FAILED, error, data }),
};

export const updateUser = createAsyncActionOrchestration<IUser, IUser, IUser>(
    (data) => put('/api/settings/user', data), UpdateUserActions
);

const ConfigurationLoadActions: IAsyncActions<void, Configuration, Configuration> = {
    start: () => ({ type: ConfigurationLoadStart, data: undefined }),
    success: (data) => ({ type: ConfigurationLoadSuccess, data }),
    error: (error, data) => ({ type: ConfigurationLoadError, error, data }),
};

export const loadConfiguration = createAsyncActionOrchestration<void, Configuration, Configuration>(
    () => get('/api/settings/configuration'), ConfigurationLoadActions
);
