namespace Accounts.ReferenceData

open System
open Microsoft.FSharp.Reflection

[<AutoOpen>]
module Helpers =
    let fromString (t : Type) (s : string) =
        match FSharpType.GetUnionCases t |> Array.filter (fun case -> case.Name = s) with
        |[|case|] -> Some (FSharpValue.MakeUnion(case,[||]) :?> 'a)
        |_ -> None

    let toString (x : 'a) = 
        match FSharpValue.GetUnionFields(x, x.GetType()) with
        | case, _ -> case.Name

