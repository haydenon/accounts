namespace Accounts.ReferenceData
    open Giraffe
    open Accounts
    open Accounts.Repository
    open Accounts.ReferenceData.Helpers
    open Accounts.Data.Models
    open Microsoft.AspNetCore.Http
    open Microsoft.Extensions.DependencyInjection
    open Microsoft.FSharp.Reflection
    open System.Linq
    open System
    open System.Threading.Tasks
    open Newtonsoft.Json

    module Data =
        [<JsonConverter(typeof<DiscriminatedUnionStringConverter>)>]
        type TokenType =
            | Registration 

        [<JsonConverter(typeof<DiscriminatedUnionStringConverter>)>]
        type Period =
        | Day        
        | Week
        | Month
        | Year

        let caseType (x:'a) = 
            match FSharpValue.GetUnionFields(x, typeof<'a>) with
            | case, _ -> case.DeclaringType

        let findByName<'T when 'T :> IReferenceEntity and 'T :not struct and 'T : null> (queryable : 'T IQueryable) name =
            query {
                for item in queryable do
                where (item.Name = name)
                select item
            } 
            |> Seq.head

        let refData<'T when 'T :> IReferenceEntity and 'T :not struct and 'T : null> (ctx : HttpContext) item =
            let repo = ctx.GetService<IRepository<'T>>()
            findByName repo.Query (toString item)

        let private createForType<'A, 'T when 'T :> IReferenceEntity and 'T : (new : unit -> 'T ) and 'T :not struct and 'T : null> (services : IServiceProvider) =
            let repo = services.GetService<IRepository<'T>>()
            let newWithName name =
                let item = new 'T()
                item.Name <- name
                item
            let cases = (FSharpType.GetUnionCases typeof<'A>) |> List.ofArray
            let existing = repo.Query |> List.ofSeq
            let toCreate = List.filter (fun (case : UnionCaseInfo) ->
                not <| Seq.exists (fun (item : 'T) ->
                    item.Name = case.Name) existing) cases
            let items = List.map (fun (case : UnionCaseInfo) -> newWithName case.Name) toCreate
            match items with
            | [] ->
                Task.FromResult true
            | _::_ ->
                repo.CreateRange items
                repo.SaveChangesAsync()

        let createMissingReferenceData (services : IServiceProvider) =
            [ createForType<TokenType, Data.Models.TokenType>
              createForType<Period, Data.Models.Period> ]
            |> List.fold (fun result creator ->
                result && (
                    creator services
                    |> Async.AwaitTask
                    |> Async.RunSynchronously)) true

