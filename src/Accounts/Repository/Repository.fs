namespace Accounts.Repository

open System.Linq
open System.Threading.Tasks

open Microsoft.EntityFrameworkCore

open Giraffe

open Accounts.Data
open Accounts.Data.Models

type IRepository<'T when 'T :not struct and 'T :null and 'T :> IEntity> =
    abstract member FindById : int64 -> Option<'T>
    abstract member FindByIdReadOnly : int64 -> Option<'T>
    abstract member Query : IQueryable<'T>
    abstract member QueryReadOnly : IQueryable<'T>
    abstract member Update : 'T -> unit
    abstract member UpdateRange : 'T seq -> unit
    abstract member Create : 'T -> unit
    abstract member CreateRange : 'T seq -> unit
    abstract member Delete : 'T -> unit
    abstract member DeleteRange : 'T seq -> unit
    abstract member SaveChangesAsync : unit -> Task<bool>

type Repository<'T when 'T :not struct and 'T :null and 'T :> IEntity>(context: AccountsContext) =
    let set() =
        context.Set<'T>()

    let findById (set : IQueryable<'T>) id =
        let items =
            query {
                for item in set do
                where (item.Id = id)
                select item }
        match Seq.isEmpty items with
        | true -> None
        | false -> Some (Seq.head items)

    interface IRepository<'T> with
        member _this.FindById(id) : Option<'T> =
            findById(set()) id

        member _this.FindByIdReadOnly(id) : Option<'T> =
            findById (set().AsNoTracking()) id

        member _this.Query : IQueryable<'T> =
            set() :> IQueryable<'T>

        member _this.QueryReadOnly : IQueryable<'T> =
            set().AsNoTracking() :> IQueryable<'T>

        member _this.Update (item : 'T) =
            let tracked = context.ChangeTracker.Entries<'T>() |> Seq.find (fun e -> e.Entity.Id = item.Id)
            if isNull tracked then
                set().Attach item |> ignore
            context.Entry(item).State <- EntityState.Modified

        member this.UpdateRange (items : 'T seq) =
            Seq.iter (this :> IRepository<'T>).Update items

        member _this.Create (item : 'T)=
            set().Add item |> ignore

        member _this.CreateRange(items: 'T seq) =
            set().AddRange items

        member _this.Delete (item : 'T) =
            set().Remove item |> ignore

        member _this.DeleteRange (items : 'T seq) =
            Seq.toList items
            |> set().RemoveRange

        member _this.SaveChangesAsync() : bool Task = task {
            let! changes = context.SaveChangesAsync()
            return changes > 0
        }
