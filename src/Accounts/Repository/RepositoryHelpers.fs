namespace Accounts.Repository

open System
open System.Linq
open System.Linq.Expressions

open Microsoft.AspNetCore.Http
open Microsoft.EntityFrameworkCore

open Npgsql
open NpgsqlTypes

open Giraffe

open Accounts.Data.Models

type Query =
    static member Any () =
        fun (q : IQueryable<'a>) -> q.Any()

    static member IncludeRelation (f : Expression<Func<'a, 'b>>) =
        fun (q : IQueryable<'a>) -> (q.Include f)

    static member Skip toSkip =
        fun (q : IQueryable<'a>) -> (q.Skip toSkip)

    static member Take toLimit =
        fun (q : IQueryable<'a>) -> (q.Take toLimit)

    static member OrderBy (f : Expression<Func<'a, 'b>>) =
        fun (q : IQueryable<'a>) -> (q.OrderBy f)

    static member OrderByDescending (f : Expression<Func<'a, 'b>>) =
        fun (q : IQueryable<'a>) -> (q.OrderByDescending f)

    static member ThenBy (f : Expression<Func<'a, 'b>>) =
        fun (q : IOrderedQueryable<'a>) -> (q.ThenBy f)

    static member ThenByDescending (f : Expression<Func<'a, 'b>>) =
        fun (q : IOrderedQueryable<'a>) -> (q.ThenByDescending f)

[<AutoOpen>]
module Helpers =
    let runQuery (q : IQueryable<'a>) = Seq.toList q |> ignore

    let sqlString (str : string) = RawSqlString.op_Implicit str

    let queryOptionalParam name (dbType : NpgsqlDbType) (value : 'a option) =
        let param = new NpgsqlParameter(name, dbType)
        match value with
        | Some value -> param.Value <- value
        | None -> param.Value <- DBNull.Value
        param

    let queryParam name (value : obj) =
        new NpgsqlParameter(name, value)

    let timeZoneFromId id =
            TimeZoneInfo.GetSystemTimeZones().First(fun tz -> tz.Id = id)

    let timeZoneForAutoTransfer (ctx : HttpContext) (auto : AutoTransfer) =
        let repo = ctx.GetService<IRepository<Account>>()
        let tzId =
            query {
                for acc in repo.Query do
                where (acc.Id = auto.AccountId)
                select acc.User.TimeZoneId
            } |> Seq.head
        timeZoneFromId tzId