namespace Accounts.Repository

open System
open Accounts.Data.Models

type RepositoryFactory(services : IServiceProvider) =
    member _this.Repository<'T when 'T :not struct and 'T :null and 'T :> IEntity>() =
        services.GetService(typeof<IRepository<'T>>) :?> IRepository<'T>