namespace Accounts.Repository

open System
open System.Linq

open Microsoft.EntityFrameworkCore
open NpgsqlTypes

open Accounts
open Accounts.Repository
open Accounts.Data
open Accounts.Data.Models
open Accounts.Services

type ITransferRepository =
  abstract member FindById : int64 -> Option<Transfer>
  abstract member Query : IQueryable<Transfer>
  abstract member CreateTransfer : Transfer -> Transfer
  abstract member UpdateTransfer : Transfer -> unit
  abstract member UpdateTransferCurrencyForAccount : (decimal -> decimal) -> Account -> unit
  abstract member DeleteTransfer : Transfer -> unit
  abstract member DeleteAllForAccount : int64 -> unit

type TransferRepository(context: AccountsContext, userService: UserService) =
  let create = "SELECT * FROM create_transfer(@account_id, @amount, @currency, @original_amount, @original_currency, @transfer_date, @description, @auto_transfer, @created_on, @created_by)"
  let update = "SELECT * FROM update_transfer(@transfer_id, @amount, @display_amount, @original_amount, @original_currency, @transfer_date, @description, @modified_on, @modified_by)"
  let delete = "SELECT * FROM delete_transfer(@transfer_id)"

  interface ITransferRepository with
    member __.FindById id =
      let items =
        query {
          for item in context.Transfer do
          where (item.Id = id)
          select item
        }
      match Seq.isEmpty items with
      | true -> None
      | false -> Some (Seq.head items)

    member __.Query = context.Transfer.AsQueryable()

    member __.CreateTransfer transfer =
      let accId = queryParam "account_id" transfer.AccountId
      let amt = queryParam "amount" transfer.Amount
      let curr = queryParam "currency" transfer.Currency
      let origAmt = queryOptionalParam "original_amount" NpgsqlDbType.Numeric (Option.fromNullable transfer.OriginalAmount)
      let origCurr = queryOptionalParam "original_currency" NpgsqlDbType.Varchar (Option.possibleNull transfer.OriginalCurrency)
      let date = queryParam "transfer_date" transfer.Date
      let desc = queryOptionalParam "description" NpgsqlDbType.Text (Option.possibleNull transfer.Description)
      let auto = queryParam "auto_transfer" transfer.AutoTransfer
      let createdOn = queryParam "created_on" DateTime.UtcNow
      let createdBy = queryParam "created_by" (userService.CurrentUserName())
      let details =
        context.TransferCreate.FromSql(sqlString create, accId, amt, curr, origAmt, origCurr, date, desc, auto, createdOn, createdBy)
        |> Seq.head
      transfer.Id <- details.TransferId
      transfer.AccountBalance <- details.AccountBalance
      transfer.DisplayAmount <- transfer.Amount
      transfer

    member __.UpdateTransfer transfer =
      let id = queryParam "transfer_id" transfer.Id
      let amt = queryParam "amount" transfer.Amount
      let dispAmt = queryParam "display_amount" transfer.DisplayAmount
      let origAmt = queryOptionalParam "original_amount" NpgsqlDbType.Numeric (Option.fromNullable transfer.OriginalAmount)
      let origCurr = queryOptionalParam "original_currency" NpgsqlDbType.Varchar (Option.possibleNull transfer.OriginalCurrency)
      let date = queryParam "transfer_date" transfer.Date
      let desc = queryOptionalParam "description" NpgsqlDbType.Text (Option.possibleNull transfer.Description)
      let modifiedOn = queryParam "modified_on" DateTime.UtcNow
      let modifiedBy = queryParam "modified_by" (userService.CurrentUserName())
      context.VoidQuery.FromSql(sqlString update, id, amt, dispAmt, origAmt, origCurr, date, desc, modifiedOn, modifiedBy) |> runQuery

    member __.DeleteTransfer transfer =
      let id = queryParam "transfer_id" transfer.Id
      context.VoidQuery.FromSql(sqlString delete, id) |> runQuery

    member __.UpdateTransferCurrencyForAccount exchange account =
      let updateBalance balance (transfer : Transfer) =
        transfer.AccountBalance <- balance + transfer.Amount
        transfer.AccountBalance
      let order (t1 : Transfer) (t2 : Transfer) =
        let dateCmp = compare t1.Date t2.Date
        if dateCmp = 0 then
          compare t1.CreatedOn t2.CreatedOn
        else
          dateCmp
      let transfers =
        query {
          for t in context.Transfer do
          where (t.AccountId = account.Id)
          select t
        }
        |> List.ofSeq
        |> List.sortWith order
      List.iter (fun (t : Transfer) -> t.Amount <- exchange t.Amount) transfers
      List.fold updateBalance 0m transfers |> ignore
      context.Transfer.UpdateRange transfers |> ignore

    member __.DeleteAllForAccount (id : int64) =
      let transfers = query {
        for t in context.Transfer do
        where (t.AccountId = id)
        select t
      }
      context.Transfer.RemoveRange transfers
