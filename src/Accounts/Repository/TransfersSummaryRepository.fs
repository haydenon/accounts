namespace Accounts.Repository

open System

open Microsoft.EntityFrameworkCore
open NpgsqlTypes

open Accounts
open Accounts.Repository
open Accounts.Data
open Accounts.Data.Models
open Accounts.ReferenceData.Data

type ITransfersSummaryRepository =
  abstract member SummariesForAccount : RecordId<Account> -> UserId -> Period -> DateTime option -> DateTime option -> TransfersSummary list
  abstract member SummariesForAllAccounts : UserId -> Period -> DateTime option -> DateTime option -> TransfersSummary list

type TransfersSummaryRepository(context: AccountsContext) =
  let getSummaries (accId : int64 option) (userId : string) (period : Period) (startDate : DateTime option) (endDate : DateTime option) =
    let user = queryParam "user_id" userId
    let account = queryOptionalParam "account_id" NpgsqlDbType.Bigint accId
    let start = queryOptionalParam "start_date" NpgsqlDbType.Timestamp startDate
    let finish = queryOptionalParam "end_date" NpgsqlDbType.Timestamp endDate

    let query =
      match period with
      | Day -> "day"
      | Week -> "week"
      | Month | Year -> "month"
      |> sprintf "SELECT * FROM get_transfers_summary_by_%s(@user_id, @account_id, @start_date, @end_date)"
      |> sqlString
    context.TransferSummaries.FromSql(query, user, account, start, finish) :> TransfersSummary seq
    |> List.ofSeq

  interface ITransfersSummaryRepository with
    member __.SummariesForAccount (RecordId accountId) (UserId userId) period startDate endDate =
      getSummaries (Some accountId) userId period startDate endDate

    member __.SummariesForAllAccounts (UserId userId) period startDate endDate =
      getSummaries None userId period startDate endDate
