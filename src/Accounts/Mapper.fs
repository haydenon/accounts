module Accounts.Mapper

open System

open Giraffe
open Accounts.Data
open Accounts.Errors
open Accounts.ViewModels
open ReferenceData.Data
open ReferenceData.Helpers
open System.Globalization
open Microsoft.AspNetCore.Http
open Accounts.Helpers.DateTimeHelpers
open Microsoft.AspNetCore.Identity

let toErrorModel errors : ErrorView =
    let error =
        match List.tryHead errors with
        | Some head -> head
        | None -> UnexpectedError
    { Error = message error; Code = 0 }

let dateFromString str =
    match DateTime.TryParseExact(str, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None) with
    | (true, date) -> date
    | _ -> failwith "Invalid date string"

let dateToString (date : DateTime) =
    date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)

let timeToString (time: DateTime) =
    time.ToString("yyyy-MM-dd_HH:mm:ss", CultureInfo.InvariantCulture)

let mapUser (_ : HttpContext) : Models.User -> UserView =
    fun user ->
        {
            Email = user.Email
            DisplayName = user.DisplayName
            CommonCurrencies = if isNull user.CommonCurrencies
                               then [||]
                               else user.CommonCurrencies.Split(';')
            Timezone = user.TimeZoneId
        }

let mapAccountShare (_ : HttpContext) :  Models.AccountShare -> AccountShareView =
    fun source ->
        {
            Id = source.Id
            AccountId = source.AccountId
            ShareUser = source.SharedEmail
            Readonly = source.Readonly
        }

let mapViewAccountShare (ctx : HttpContext) : AccountShareView -> Models.AccountShare =
    fun source ->
        let userManager = ctx.GetService<UserManager<Models.User>>()
        let normalised = userManager.NormalizeKey(source.ShareUser)
        let userId = Seq.head <| query {
            for user in userManager.Users do
            where (user.NormalizedEmail = normalised)
            select user.Id
        }
        let model = Models.AccountShare()
        model.Id <- source.Id
        model.AccountId <- source.AccountId
        model.SharedUserId <- userId
        model.SharedEmail <- source.ShareUser
        model.Readonly <- source.Readonly
        model

type SharesCollection = System.Collections.Generic.ICollection<Models.AccountShare>

let private accountShares ctx userId (shares : SharesCollection) =
    if isNull shares
    then []
    else
        List.ofSeq shares
        |> List.filter (fun s -> s.UserId = userId || s.SharedUserId = userId)
    |> List.map (mapAccountShare ctx)

let mapAccount (ctx : HttpContext) : Models.Account -> AccountView =
    fun account ->
        let userManager = ctx.GetService<UserManager<Models.User>>()
        let currentId = userManager.GetUserId(ctx.User)
        let user = if currentId = account.UserId then null else account.User.Email
        let balance =
            match Seq.tryHead account.Transfers with
            | Some t -> t.AccountBalance
            | None -> 0m
        let shares = accountShares ctx currentId account.AccountShares
        let isReadonly =
            if isNull user
            then Nullable()
            else Nullable((List.head shares).Readonly)
        {
            Id = account.Id
            CreatedAt = timeToString account.CreatedOn
            Name = account.Name
            Balance = balance
            Currency = account.Currency
            Shares = shares
            Owner = user
            Readonly = isReadonly
        }

let mapViewAccount (_ : HttpContext) : AccountView -> Models.Account =
    fun account ->
        let model = Models.Account()
        model.Name <- account.Name
        model.Currency <- account.Currency
        model.Id <- account.Id
        model

let mapTransfer (_ : HttpContext) : Models.Transfer -> TransferView =
    fun source ->
        {
            Id = source.Id
            CreatedAt = timeToString source.CreatedOn
            AccountId = source.AccountId
            Date = dateToString source.Date
            Amount = source.DisplayAmount
            Currency = source.Currency
            OriginalAmount = source.OriginalAmount
            OriginalCurrency = source.OriginalCurrency
            Description = source.Description
            Recurring = source.AutoTransfer
        }

let mapViewTransfer (_ : HttpContext) : TransferView -> Models.Transfer =
    fun source ->
        let model = Models.Transfer()
        model.AccountId <- source.AccountId
        model.Id <- source.Id
        model.Amount <- source.Amount
        model.OriginalAmount <- source.OriginalAmount
        model.OriginalCurrency <- source.OriginalCurrency
        model.Date <- dateFromString source.Date
        model.Description <- source.Description
        model

let mapAutoTransfer (_ : HttpContext) : Models.AutoTransfer -> AutoTransferView =
    fun source ->
        let period = (fromString typeof<Period> source.Period.Name).Value
        let nextDate =
            match Option.ofNullable source.LastTransfer with
            | None -> source.StartDate
            | Some d -> addTimeframe d period source.PeriodsPerOccurence 1
        {
            Id = source.Id
            CreatedAt = timeToString source.CreatedOn
            AccountId = source.AccountId
            Date = dateToString source.StartDate
            Amount = source.Amount
            Currency = source.Currency
            Description = source.Description
            Period = period
            PeriodsPerOccurence = source.PeriodsPerOccurence
            NextDate = nextDate |> dateToString
        }

let mapViewAutoTransfer (ctx : HttpContext): AutoTransferView -> Models.AutoTransfer =
    fun source ->
        let model = Models.AutoTransfer()
        model.Id <- source.Id
        model.AccountId <- source.AccountId
        model.Amount <- source.Amount
        model.Amount <- source.Amount
        model.Currency <- source.Currency
        model.StartDate <- dateFromString source.Date
        model.Description <- source.Description
        model.Period <- refData<Models.Period> ctx source.Period
        model.PeriodsPerOccurence <- source.PeriodsPerOccurence
        model

type Mapper =
    static member Map(ctx : HttpContext, m : Models.User) = mapUser ctx m
    static member Map(ctx : HttpContext, m : Models.Account) = mapAccount ctx m
    static member Map(ctx : HttpContext, m : ViewModels.AccountView) = mapViewAccount ctx m
    static member Map(ctx : HttpContext, s : Models.Transfer) = mapTransfer ctx s
    static member Map(ctx : HttpContext, s : ViewModels.TransferView) = mapViewTransfer ctx s
    static member Map(ctx : HttpContext, s : Models.AutoTransfer) = mapAutoTransfer ctx s
    static member Map(ctx : HttpContext, s : ViewModels.AutoTransferView) = mapViewAutoTransfer ctx s
    static member Map(ctx : HttpContext, s : Models.AccountShare) = mapAccountShare ctx s
    static member Map(ctx : HttpContext, s : ViewModels.AccountShareView) = mapViewAccountShare ctx s
