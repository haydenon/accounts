import React from "react";

type ChartType = 'Line';

declare interface IProps {
  data: any;
  type: ChartType;
  options?: any;
}

declare class ChartistGraph extends React.Component<IProps> { }

export default ChartistGraph;