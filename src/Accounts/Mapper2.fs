module Accounts.Mapper2

open Accounts
open Accounts.Data
open Accounts.Errors
open Accounts.Flow2
open Accounts.Flow2.AppResult
open Accounts.Models
open Accounts.ViewModels
open System
open System.Globalization

let withSource source result : AppResult<_> =
  match result with
  | Error err -> Error [ValidationFailure(err,source)]
  | Ok v -> Ok v

let dateFromString str =
  match DateTime.TryParseExact(str,"yyyy-MM-dd",CultureInfo.InvariantCulture,DateTimeStyles.None) with
  | (true,date) -> date
  | _ -> failwith "Invalid date string"

let dateToString(date : DateTime) = date.ToString("yyyy-MM-dd",CultureInfo.InvariantCulture)
let timeToString(time : DateTime) =
  time.ToString("yyyy-MM-dd_HH:mm:ss",CultureInfo.InvariantCulture)

module User =
  let mapUserToView(user : User) : UserView =
    {Email = WrappedString.value user.Email
     DisplayName = WrappedString.value user.DisplayName
     CommonCurrencies = Array.empty
     Timezone = ""}

  let mapFromModel(user : Models.User) : AppResult<User> =
    let commonCurrencies =
      user.CommonCurrencies.Split(';')
      |> Array.map(WrappedString.string3 >> withSource "currency")
      |> List.ofArray
    User.Create <!> (EmailAddress.create user.Email |> withSource "email")
    <*> (WrappedString.stringNonEmpty256 user.DisplayName |> withSource "display name")
    <*> (sequence commonCurrencies |> Result.map Array.ofList)
    <*> (WrappedString.string256 user.TimeZoneId |> (Option.fromResult >> Result.Ok))

module TransfersSummary =
  let mapFromModel(summary : Models.TransfersSummary) : AppResult<TransfersSummary> =
    TransfersSummary.Create (RecordId summary.AccountId) summary.StartDate summary.Amount
      summary.StartBalance |> Ok

  let mapToView(summary : TransfersSummary) : TransfersSummaryView =
    let (RecordId accountId) = summary.AccountId
    {AccountId = accountId
     StartDate = dateToString summary.StartDate
     EndDate = ""
     Amount = summary.Amount
     StartBalance = summary.StartBalance}
