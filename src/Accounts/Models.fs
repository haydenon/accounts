module Accounts.Models

open Accounts
open Accounts.CommonTypes.WrappedString
open System

type User =
  {Email : EmailAddress.T
   DisplayName : StringNonEmpty256
   CommonCurrencies : String3 []
   Timezone : String256 option}
  static member Create email displayName currencies timezone : User =
    {Email = email
     DisplayName = displayName
     CommonCurrencies = currencies
     Timezone = timezone}

type Account =
  {Id : RecordId<Account>}

type TransfersSummary =
  {AccountId : RecordId<Account>
   StartDate : DateTime
   Amount : decimal
   StartBalance : decimal}
  static member Create accountId startDate amount startBalance : TransfersSummary =
    {AccountId = accountId
     StartDate = startDate
     Amount = amount
     StartBalance = startBalance}
