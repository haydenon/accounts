declare class ColorHash {
  hex(value: string): string;
}

export default ColorHash;