namespace Accounts.Validation
    open System
    open System.Linq

    open Microsoft.FSharp.Quotations

    open Accounts.Errors

    module Validation =
        type Failure = ValidationError * string

        type 'e Test = Test of ('e -> Failure option) 

        let CreateValidator (f: 'e -> ('e Test list -> 'e Test list)) =  
            let entries = f Unchecked.defaultof<_> []
            fun entity -> List.choose (fun (Test test) -> test entity) entries

        let private add (propQ:'x Expr) (name : string option) (errorType : ValidationError) fx (xs: 'e Test list) = 
            let propName, eval =
                match propQ with
                | Patterns.PropertyGet (_,p,_) -> p.Name, fun x -> p.GetValue(x,[||])
                | Patterns.Value (_, ty) when ty = typeof<'e> -> "x", box 
                | _ -> failwith "Unsupported expression"
            let test entity =
                let propertyName =
                    match name with
                    | Some n -> n
                    | None   -> propName
                let value = eval entity
                if fx (unbox value) then None
                else  Some (errorType, propertyName)
            Test(test) :: xs

        let email propQ propName = 
            let regex = Text.RegularExpressions.Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*")
            add propQ propName (InvalidFormat "email address") regex.IsMatch
             
        let requiredString propQ propName = 
            add propQ propName NullValue (String.IsNullOrWhiteSpace >> not)

        let required propQ propName =
            add propQ propName NullValue (isNull >> not)

        let minLength propQ propName min =
            add propQ propName (MinLength min)
                (fun v -> String.length v >= min)

        let maxLength propQ propName max =
            add propQ propName (MaxLength max)
                (fun v -> String.length v <= max)

        let minValue propQ propName min =
            add propQ propName (MinValue min)
                (fun v -> v >= min)

        let maxValue propQ propName max =
            add propQ propName (MaxValue max)
                (fun v -> v <= max)

        let unique propQ propName (getQuery : 'a -> IQueryable<'b>) =
            add propQ propName Duplicate (getQuery >> Seq.isEmpty)

        let valueExists propQ propName (idToRelated : 'a -> IQueryable<'b>) =
            add propQ propName MustExist (idToRelated >> Seq.isEmpty >> not)

        let valueExistsOrNull propQ propName (idToRelated : 'a -> IQueryable<'b>) =
            add propQ propName MustExist (fun v ->
                if isNull v
                then true
                else v |> idToRelated |> Seq.isEmpty |> not)

        let valuesExist propQ propName (valueToRelated : 'a [] -> IQueryable<'b>) =
            add propQ propName MustExist (fun arr ->
                not (isNull arr)
                && (Array.isEmpty arr )
                || ((Array.length arr) = (valueToRelated arr |> List.ofSeq |> List.length)))
        
        let exists propQ propName (doesExist : int64 -> bool) =
            add propQ propName MustExist doesExist
        
        let routeMatches propQ (routeId : int64) =
            add propQ (Some "Id") MustMatchRoute (fun id -> id = routeId)
        
        let notEqual propQ propName (value : 'a) (eql : 'a -> 'a -> bool) =
            add propQ propName MustNotEqual  (fun itemVal -> not (eql value itemVal))
