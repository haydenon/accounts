namespace Accounts.Validation
    open System
    open System.Linq
    open Giraffe
    open Microsoft.AspNetCore.Http
    open Accounts.Services.Rates
    open Accounts.Validation.Validation
    open Accounts.ViewModels

    module SettingsValidation =
        let currencies (ctx : HttpContext) (names : string []) = 
            let rateClient = ctx.GetService<IRateClient>()
            let rateNames =
                rateClient.GetRateNames()
                |> Async.AwaitTask
                |> Async.RunSynchronously
            (query {
                for name in names do
                where (List.contains name rateNames)                
                select name
            }).AsQueryable()
        
        let validateCurrencies (ctx : HttpContext) =
            CreateValidator <| fun (x : SetCurrencyView) ->
                valuesExist <@ x.Currencies @> None (currencies ctx)

        let getTimezone timezoneId =
            TimeZoneInfo.GetSystemTimeZones()
            |> Seq.filter (fun tz -> tz.Id = timezoneId)
            |> Queryable.AsQueryable


        let validateTimezone _ =
            CreateValidator <| fun (x : SetTimezoneView) ->
                valueExists <@ x.Timezone @> None getTimezone

        let validateUser _ =
            CreateValidator <| fun (x : UserView) ->
                minLength <@ x.DisplayName @> (Some "Display name") 3 >>
                maxLength <@ x.DisplayName @> (Some "Display name") 25