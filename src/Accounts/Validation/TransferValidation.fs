namespace Accounts.Validation
    open Validation
    open Common
    open Accounts.Errors
    open Accounts.ViewModels
    open Microsoft.AspNetCore.Http
    
    module TransferValidation =
        let validateTransfer (ctx : HttpContext) = 
            CreateValidator <| fun (x : TransferView) ->
                requiredString <@ x.Date @> None >>
                valueExistsOrNull <@ x.OriginalCurrency @> (Some "Currency") (whereCurrencyExists ctx) >>
                valueExists <@ x.AccountId @> (Some "Related account") (relatedWritableAccount ctx)
        
        let validateTransferUpdate routeId (ctx : HttpContext) = 
            CreateValidator <| fun (x : TransferView) ->
                routeMatches <@ x.Id @> routeId >>
                requiredString <@ x.Date @> None >>
                exists <@ x.Id @> (Some "Transfer") (transferExists ctx true) >>
                valueExistsOrNull <@ x.OriginalCurrency @> (Some "Currency") (whereCurrencyExists ctx) >>
                valueExists <@ x.AccountId @> (Some "Related account") (relatedWritableAccount ctx)
        
        let validateDelete (ctx : HttpContext) (transferId : int64) = 
            match transferExists ctx true transferId with
            | true  -> []
            | false -> [MustExist, "Transfer"]

