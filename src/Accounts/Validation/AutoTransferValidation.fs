namespace Accounts.Validation
    open Accounts.Errors
    open Accounts.Validation.Validation
    open Accounts.ViewModels
    open Microsoft.AspNetCore.Http
    open Accounts.Validation.Common

    module AutoTransferValidation =
        let validateAutoTransfer (ctx : HttpContext) = 
            CreateValidator <| fun (x : AutoTransferView) ->
                requiredString <@ x.Date @> None >>
                required <@ x.Period @> None >>
                minValue <@ x.PeriodsPerOccurence @> (Some "Periods per occurence") 1 >>
                valueExistsOrNull <@ x.Currency @> (Some "Currency") (whereCurrencyExists ctx) >>
                valueExists <@ x.AccountId @> (Some "Related account") (relatedWritableAccount ctx)

        let validateAutoTransferUpdate routeId (ctx : HttpContext) = 
            CreateValidator <| fun (x : AutoTransferView) ->
                routeMatches <@ x.Id @> routeId >>
                requiredString <@ x.Date @> None >>
                required <@ x.Period @> None >>
                minValue <@ x.PeriodsPerOccurence @> (Some "Periods per occurence") 1 >>
                exists <@ x.Id @> (Some "Recurring transfer") (autoTransferExists ctx true) >>
                valueExistsOrNull <@ x.Currency @> (Some "Currency") (whereCurrencyExists ctx) >>
                valueExists <@ x.AccountId @> (Some "Related account") (relatedWritableAccount ctx)

        let validateDelete (ctx : HttpContext) (transferId : int64) = 
            match autoTransferExists ctx true transferId with
            | true  -> []
            | false -> [MustExist, "Recurring"]