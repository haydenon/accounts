namespace Accounts.Validation
    open Giraffe
    open System.Linq
    open Microsoft.Extensions.DependencyInjection
    open Microsoft.AspNetCore.Http
    open Microsoft.AspNetCore.Identity
    open Accounts.Errors
    open Accounts.Services.Rates
    open Accounts.Repository
    open Accounts.Validation.Validation
    open Accounts.Validation.Common
    open Accounts.ViewModels
    open Accounts.Data.Models
    open Accounts.Helpers.UserHelpers

    module AccountValidation =
        let private whereCriteria (ctx : HttpContext) (criteria : IQueryable<Account> -> string -> IQueryable<Account>) =
            let userManager = ctx.GetService<UserManager<User>>()
            let repo = ctx.GetService<IRepository<Account>>()
            let userId = userManager.GetUserId(ctx.User)
            criteria repo.Query userId

        let private userOwnedAccount (ctx : HttpContext) id =
            whereCriteria ctx (fun accs userId ->
                query {
                    for item in accs do
                    where (item.Id = id && item.UserId = userId)
                    select item
                })
                |> Seq.isEmpty |> not

        let private userOwnedShare (ctx : HttpContext) id =
            let userManager = ctx.GetService<UserManager<User>>()
            let repo = ctx.GetService<IRepository<AccountShare>>()
            let userId = userManager.GetUserId(ctx.User)
            query {
                for item in repo.Query do
                where (item.Id = id && item.UserId = userId)
                select item
            } |> Seq.isEmpty |> not

        let private whereNameEquals (ctx : HttpContext) (account : AccountView) =
            let currentId = account.Id
            let name = account.Name
            whereCriteria ctx (fun accs userId -> 
                query {
                    for item in accs do
                    where (item.Name = name && item.UserId = userId
                        && item.Id <> currentId) 
                    select item
                })
        
        let private whereCurrencyExists (ctx : HttpContext) currency =
            let rateClient = ctx.GetService<IRateClient>()
            let names = rateClient.GetRateNames()
                        |> Async.AwaitTask
                        |> Async.RunSynchronously
            (query {
                    for item in names do
                    where (item = currency)
                    select item
            }).AsQueryable()

        let validateAccount ctx = 
            CreateValidator <| fun (x : AccountView) ->
                requiredString <@ x.Name @> None >>
                minLength <@ x.Name @> None 1 >>
                maxLength <@ x.Name @> None 60 >>
                unique <@ x @> (Some "Name") (whereNameEquals ctx) >>
                valueExists <@ x.Currency @> None (whereCurrencyExists ctx)

        let validateAccountUpdate routeId ctx =
            CreateValidator <| fun (x : AccountView) ->
                routeMatches <@ x.Id @> routeId >>
                requiredString <@ x.Name @> None >>
                minLength <@ x.Name @> None 1 >>
                maxLength <@ x.Name @> None 60 >>
                exists <@ x.Id @> (Some "Account") (accountOwnedByUser ctx) >>
                unique <@ x @> (Some "Name") (whereNameEquals ctx) >>
                valueExists <@ x.Currency @> None (whereCurrencyExists ctx)

        let ignoreCaseEqual s1 s2 =
            System.String.Equals(s1, s2, System.StringComparison.CurrentCultureIgnoreCase)
        
        let validateAccountShare routeId ctx =
            let email = (userEmail ctx).Result
            CreateValidator <| fun (x : AccountShareView) ->
                routeMatches <@ x.AccountId @> routeId >>
                exists <@ x.AccountId @> (Some "Account") (accountOwnedByUser ctx) >>
                notEqual <@ x.ShareUser @> (Some "Shared user") email ignoreCaseEqual

        let validateAccountShareUpdate accId shareId ctx =
            let email = (userEmail ctx).Result
            CreateValidator <| fun (x : AccountShareView) ->
                routeMatches <@ x.Id @> shareId >>
                routeMatches <@ x.AccountId @> accId >>
                exists <@ x.Id @> (Some "Account share") (accountShareExists ctx) >>
                exists <@ x.AccountId @> (Some "Account") (accountOwnedByUser ctx) >>
                notEqual <@ x.ShareUser @> (Some "Shared user") email ignoreCaseEqual

        let validateDelete ctx id =
            match userOwnedAccount ctx id with
            | true  -> []
            | false -> [MustExist, "Account"]
        
        let validateAccountShareDelete ctx id =
            match userOwnedShare ctx id with
            | true  -> []
            | false -> [MustExist, "Account"]