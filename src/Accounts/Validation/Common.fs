namespace Accounts.Validation
    open System.Linq
    open Giraffe
    open Accounts.Data
    open Accounts.Services.Rates
    open Microsoft.AspNetCore.Http
    open Microsoft.AspNetCore.Identity
    open Accounts.Repository
    open Accounts.Data.Models

    module Common =
        let relatedAccount (ctx : HttpContext) accountId =
            let userManager = ctx.GetService<UserManager<Models.User>>()
            let repo = ctx.GetService<IRepository<Models.Account>>()
            let userId = userManager.GetUserId(ctx.User)
            query {
                for item in repo.Query do
                where (item.Id = accountId && item.UserId = userId)
                select item
            }

        let relatedWritableAccount (ctx : HttpContext) accountId =
            let userManager = ctx.GetService<UserManager<Models.User>>()
            let repo = ctx.GetService<IRepository<Models.Account>>()
            let userId = userManager.GetUserId(ctx.User)
            query {
                for item in repo.Query do
                where (item.Id = accountId
                    && (item.UserId = userId
                    || item.AccountShares.Any(fun s ->
                        s.SharedUserId = userId && not s.Readonly)))
                select item
            }

        let whereCurrencyExists (ctx : HttpContext) currency =
            let rateClient = ctx.GetService<IRateClient>()
            let names = rateClient.GetRateNames()
                        |> Async.AwaitTask
                        |> Async.RunSynchronously
            (query {
                    for item in names do
                    where (item = currency)
                    select item
            }).AsQueryable()

        let itemExists<'T when 'T :not struct and 'T :null and 'T :> IEntity>
            (ctx : HttpContext) (itemQuery : int64 -> string -> IQueryable<'T> -> IQueryable<'T>) id =
                let userManager = ctx.GetService<UserManager<Models.User>>()
                let repo = ctx.GetService<IRepository<'T>>()
                let userId = userManager.GetUserId(ctx.User)
                not <| (Seq.isEmpty <| itemQuery id userId repo.Query)

        let itemExistsIn<'T>
            (ctx : HttpContext) (itemQuery : int64 -> string -> IQueryable<'T>) id =
                let userManager = ctx.GetService<UserManager<Models.User>>()
                let userId = userManager.GetUserId(ctx.User)
                not <| (Seq.isEmpty <| itemQuery id userId)

        let accountOwnedByUser (ctx : HttpContext) =
            itemExists<Account> ctx (fun id userId items ->
                query {
                    for item in items do
                    where (item.Id = id && item.UserId = userId)
                    select item
                })

        let accountReadableByUser (ctx : HttpContext) =
            let owned = accountOwnedByUser ctx
            let shared =
                itemExists<AccountShare> ctx (fun id userId items ->
                    query {
                        for item in items do
                        where (item.AccountId = id && item.SharedUserId = userId)
                        select item
                    })
            (fun id -> (owned id) || (shared id))

        let accountWritableByUser (ctx : HttpContext) =
            let owned = accountOwnedByUser ctx
            let shared =
                itemExists<AccountShare> ctx (fun id userId items ->
                    query {
                        for item in items do
                        where (item.AccountId = id
                            && item.SharedUserId = userId
                            && not item.Readonly)
                        select item
                    })
            (fun id -> (owned id) || (shared id))

        let userForEmail (ctx : HttpContext) =
            fun email ->
                let userManager = ctx.GetService<UserManager<Models.User>>()
                let normalised = userManager.NormalizeKey email
                query {
                    for user in userManager.Users do
                    where (user.NormalizedEmail = normalised)
                    select user
                }


        let transferExists (ctx : HttpContext) update =
            let readable = not update
            let transferRepo = ctx.GetService<ITransferRepository>()
            let items = transferRepo.Query
            itemExistsIn<Transfer> ctx (fun id userId ->
                query {
                    for item in items do
                    where (item.Id = id
                        && (item.Account.UserId = userId
                        || item.Account.AccountShares.Any(fun s ->
                            s.SharedUserId = userId && (readable || not s.Readonly))))
                    select item
                })

        let autoTransferExists (ctx : HttpContext) update =
            let readable = not update
            itemExists<AutoTransfer> ctx (fun id userId items ->
                query {
                    for item in items do
                    where (item.Id = id
                        && (item.Account.UserId = userId
                        || item.Account.AccountShares.Any(fun s ->
                            s.SharedUserId = userId && (readable || not s.Readonly))))
                    select item
                })

        let accountShareExists (ctx : HttpContext) =
            itemExists<AccountShare> ctx (fun id userId items ->
                query {
                    for item in items do
                    where (item.Id = id && item.UserId = userId && item.Account.UserId = userId)
                    select item
                })
