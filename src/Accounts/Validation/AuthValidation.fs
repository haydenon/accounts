namespace Accounts.Validation
    module AuthValidation =
        open System
        open System.Linq
        open Accounts.Validation.Validation
        open Accounts.ViewModels

        let getTimezone timezoneId =
            TimeZoneInfo.GetSystemTimeZones()
            |> Seq.filter (fun tz -> tz.Id = timezoneId)
            |> Queryable.AsQueryable

        let validateRegister = 
            CreateValidator <| fun (x : RegisterView) ->
                requiredString <@ x.Email @> None >>
                email <@ x.Email @> None >>
                minLength <@ x.DisplayName @> (Some "Display name") 3 >>
                maxLength <@ x.DisplayName @> (Some "Display name") 25 >>
                requiredString <@ x.Password @> None >>
                requiredString <@ x.Token @> None >>
                valueExists <@ x.Timezone @> None getTimezone

        let validateSignIn =
            CreateValidator <| fun (x : SignInView) ->
                requiredString <@ x.Email @> None >>
                requiredString <@ x.Password @> None