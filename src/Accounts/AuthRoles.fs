module Accounts.AuthRoles

let [<Literal>] AdminRole = "Admin"

let [<Literal>] HangfireRole = "Hangfire"