module.exports = {
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.test.json',
        },
    },
    moduleFileExtensions: [
        'js',
        'ts',
    ],
    transform: {
        '^.+\\.(ts|tsx)$': 'ts-jest',
    },
    testMatch: [
        '**/?(*.)spec.(ts|tsx)',
    ],
    preset: 'ts-jest/presets/js-with-ts',
}
