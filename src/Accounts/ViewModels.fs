namespace Accounts.ViewModels

open Accounts.ReferenceData.Data
open System

[<CLIMutable>]
type UserView =
  {DisplayName : string
   Email : string
   CommonCurrencies : string []
   Timezone : string}

type AccountShareView =
  {Id : int64
   AccountId : int64
   ShareUser : string
   Readonly : bool}

type AccountView =
  {Id : int64
   CreatedAt : string
   Name : string
   Balance : decimal
   Currency : string
   Shares : AccountShareView list
   Owner : string
   Readonly : Nullable<bool>}

type TransferView =
  {Id : int64
   CreatedAt : string
   AccountId : int64
   Date : string
   Amount : decimal
   Currency : string
   OriginalAmount : Nullable<decimal>
   OriginalCurrency : string
   Description : string
   Recurring : bool}

type AutoTransferView =
  {Id : int64
   CreatedAt : string
   AccountId : int64
   Date : string
   Amount : decimal
   Currency : string
   Description : string
   Period : Period
   PeriodsPerOccurence : int
   NextDate : string}

type TransfersSummaryView =
  {AccountId : int64
   StartDate : string
   EndDate : string
   Amount : decimal
   StartBalance : decimal}

[<CLIMutable>]
type RatesView =
  {Base : string
   Date : string
   Rates : Map<string,decimal>}

[<CLIMutable>]
type ErrorView =
  {Code : int
   Error : string}

type TimeZoneView =
  {Display : string
   Offset : string
   TimeZone : string}

[<CLIMutable>]
type SignInView =
  {Email : string
   Password : string
   RememberMe : bool}

[<CLIMutable>]
type SetCurrencyView =
  {Currencies : string []}

[<CLIMutable>]
type SetTimezoneView =
  {Timezone : string}

[<CLIMutable>]
type TokenView =
  {Token : string}

[<CLIMutable>]
type RegisterView =
  {Email : string
   DisplayName : string
   Timezone : string
   Password : string
   Token : string}

type ForgotView =
  {Email : string}

type ResetView =
  {Email : string
   Token : string
   Password : string}
