module Accounts.App

open System
open System.IO
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting
open Microsoft.EntityFrameworkCore
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection
open Giraffe
open Giraffe.Middleware
open Microsoft.Extensions.Configuration
open Microsoft.AspNetCore.Http
open Accounts.Handlers.Helpers
open Accounts.Handlers.ApiHandler
open ReferenceData.Data
open Accounts.UserSecrets
open Accounts.Setup.Hangfire
open Accounts.Setup.Repositories
open Accounts.Setup.Services
open Accounts.Setup.Auth
open NLog.Extensions.Logging

// ---------------------------------
// Web app
// ---------------------------------

let webApp =
    choose [
        subRoute "/api" apiHandler
        fallback
    ]

// ---------------------------------
// Error handler
// ---------------------------------

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(EventId(), ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> internalServerError

// ---------------------------------
// Config and Main
// ---------------------------------


let configureCors (builder : CorsPolicyBuilder) =
    builder.WithOrigins("http://localhost:8080").AllowAnyMethod().AllowAnyHeader() |> ignore

let configureApp (app : IApplicationBuilder) =
    app.UseCors(configureCors)
       .UseAuthentication()
       .UseGiraffeErrorHandler(errorHandler) |> ignore

    configureHangfire app

    app.UseStaticFiles()
       .UseGiraffe(webApp)
    createMissingReferenceData app.ApplicationServices |> ignore

    addAuthRoles app.ApplicationServices
    createHangfireJobs app.ApplicationServices

let configureDb (config: IConfiguration) (options : DbContextOptionsBuilder) =
    options.UseNpgsql(config.GetSection("Data").Item("DatabaseConnection")) |> ignore

let configureServices (config: IConfiguration) (ctx : WebHostBuilderContext) (services : IServiceCollection) =
    let sp  = services.BuildServiceProvider()
    let _env = sp.GetService<IHostingEnvironment>()
    services.AddGiraffe() |> ignore
    services.AddSingleton<IConfiguration>(config) |> ignore
    services.AddSingleton<IHostingEnvironment>(ctx.HostingEnvironment) |> ignore
    services.AddCors() |> ignore
    services.AddEntityFrameworkNpgsql()
        .AddDbContext<Data.AccountsContext>(configureDb(config)) |> ignore
    services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>() |> ignore
    services.AddLogging() |> ignore

    configureAuthServices services
    configureHangfireServices services config
    configureRepositories services
    configureServices ctx services

let configureLogging (ctx : WebHostBuilderContext) (builder : ILoggingBuilder) =
    let filter (l : LogLevel) =
        (ctx.HostingEnvironment.IsDevelopment()
            || l.Equals LogLevel.Error)
            && not (l.Equals LogLevel.Debug)
    builder.AddFilter(filter).AddConsole().AddDebug() |> ignore
    if ctx.HostingEnvironment.IsProduction() then
        builder.AddNLog() |> ignore
        NLog.LogManager.LoadConfiguration("nlog.config") |> ignore

let config contentRoot (environment : string) =
    let mutable settings =
        ConfigurationBuilder()
            .SetBasePath(contentRoot)
            .AddJsonFile("appsettings.json", optional = true, reloadOnChange = true)
            .AddJsonFile((sprintf "appsettings.%s.json" (environment.ToLower())), optional = true)
            .AddEnvironmentVariables()
    if environment.ToLower() = "development"
    then settings <- settings.AddUserSecretsToSettings("aspnet-Accounts-15f16ab6-42cf-4324-9011-5830ae370523")
    settings.Build()

[<EntryPoint>]
let main _argv =
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "dist")
    let environment = Environment.GetEnvironmentVariable "ASPNETCORE_ENVIRONMENT"
    let config      = config contentRoot environment
    WebHostBuilder()
        .UseKestrel()
        .UseEnvironment(environment)
        .UseContentRoot(contentRoot)
        .UseWebRoot(webRoot)
        .ConfigureServices(configureServices(config))
        .Configure(Action<IApplicationBuilder>(configureApp))
        .ConfigureLogging(configureLogging)
        .Build()
        .Run()
    0