[<AutoOpen>]
module Accounts.Services.Helper

open Accounts.Repository

type Actions<'T> = {
  Create : 'T -> unit
  Update : 'T -> unit
  Delete : 'T -> unit
}

let createActions (repo : IRepository<'a>) = {
  Create = repo.Create
  Update = repo.Update
  Delete = repo.Delete
}

let getUserId (userService : UserService) =
  userService.CurrentUserId()