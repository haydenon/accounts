namespace Accounts.Services

open System
open System.Linq
open Accounts.Data.Models
open Hangfire
open Accounts.Repository.Helpers
open Accounts.Repository
open Accounts.Helpers.DateTimeHelpers
open Accounts.ReferenceData.Helpers
open Accounts.ReferenceData
open Rates
open Microsoft.AspNetCore.Http
open Accounts.ViewModels
open Microsoft.FSharp.Linq.RuntimeHelpers
open System.Linq.Expressions

module AutoTransfer =
    let jobId timeZoneId = sprintf "auto_transfer_create_tz%s" timeZoneId

    let lastTransfer (auto : AutoTransfer) =
        let period : Data.Period = (fromString typeof<Data.Period> auto.Period.Name).Value
        match Option.ofNullable auto.LastTransfer with
        | Some d -> d
        | None -> (addTimeframe auto.StartDate period auto.PeriodsPerOccurence -1)

    let newTransfer exchange (auto : AutoTransfer) (account : Account) date =
        let transfer = Transfer()
        transfer.AutoTransfer <- true
        transfer.AccountId <- account.Id
        transfer.Description  <-  auto.Description
        transfer.Date  <- date
        if account.Currency = auto.Currency then
            transfer.Amount <- auto.Amount
        else
            let amount = exchange auto.Currency account.Currency auto.Amount
            transfer.Amount <- amount
            transfer.OriginalAmount <- Nullable(auto.Amount)
            transfer.OriginalCurrency <- auto.Currency
        transfer

    let mapAutoToTransfers (rates : RatesView) (autoToAccount : AutoTransfer -> Account) (autoToDates : AutoTransfer * DateTime list) =
        let auto = fst autoToDates
        let account = autoToAccount auto
        let exchange source target amount =
            ExchangeService.Exchange rates (source, target, amount)
        let createTransfer = newTransfer exchange auto account
        List.map createTransfer (snd autoToDates)

    let accountsForAutoTransfers (accounts : IQueryable<Account>) (autos : AutoTransfer list) =
        let accountIds =
            (List.map (fun (auto : AutoTransfer) -> auto.AccountId) autos).ToList()
        query {
            for a in accounts do
            where (accountIds.Contains(a.Id))
            select a
        }
        |> Seq.map (fun a -> (a.Id, a))
        |> Map.ofSeq

    type AutoTransferGroup = AutoTransfer * Transfer list

    let getTransfersForAutoTransfers getMostRecentDays autoToAccount rates autos =
        let autoToDates = (List.map (fun (auto : AutoTransfer) ->
            let period : Data.Period = (fromString typeof<Data.Period> auto.Period.Name).Value
            let date = lastTransfer auto
            let dates : DateTime list =
                getMostRecentDays date period auto.PeriodsPerOccurence
            (auto, dates)) autos)
        let dueForTransfer = List.filter (snd >> List.isEmpty >> not) autoToDates
        let map = mapAutoToTransfers rates autoToAccount
        dueForTransfer
        |> List.map (fun (a, dates) -> a, (map (a, dates)))

    let createTransfersForAutoTransfers updateAuto createTransfers (autosToTransfers : (AutoTransferGroup) list) =
        let allTransfers = List.collect snd autosToTransfers
        let accountIdToTransfers =
                List.groupBy (fun (t : Transfer) -> t.AccountId) allTransfers
        for accountIdToTrans in accountIdToTransfers do
            let transfers = snd accountIdToTrans
            createTransfers transfers
        for autoTransfers in autosToTransfers do
            let auto = fst autoTransfers
            let dates =
                snd autoTransfers
                |> List.map (fun t -> t.Date)
            let latest = List.sortDescending dates |> List.head
            auto.LastTransfer <- Option.toNullable(Some latest)
            updateAuto auto

    type AutoTransferService(factory: RepositoryFactory, transferService: TransferService, ctxAccessor: IHttpContextAccessor, rateClient: IRateClient) =
        let ctx = ctxAccessor.HttpContext

        let autoTransfersForTimeZone (timeZone : TimeZoneInfo) =
            let repo = factory.Repository<AutoTransfer>()
            query {
                    for a in repo.Query do
                    where (a.Account.User.TimeZoneId = timeZone.Id)
                    select a
                }
                |> Query.IncludeRelation(fun a -> a.Period)
                |> List.ofSeq

        static let getAction (timeZoneId : string) : Expression<Action<AutoTransferService>> =
            <@ System.Action<AutoTransferService>(fun service -> service.CreateTransfers(timeZoneId)) @>
            |> LeafExpressionConverter.QuotationToExpression
            |> unbox<Expression<Action<AutoTransferService>>>

        static member GetAction = getAction

        member __.AddCreateJobIfAnyAutos timeZoneId =
            let timeZone = timeZoneFromId timeZoneId
            let repo = factory.Repository<AutoTransfer>()
            let any =
                query {
                    for a in repo.Query do
                    where (a.Account.User.TimeZoneId = timeZone.Id)
                    select a
                }
                |> Query.Any()
            if any then
                let id = jobId timeZone.Id
                RecurringJob.AddOrUpdate<AutoTransferService>(
                    id,
                    getAction(timeZone.Id),
                    Cron.Daily(0, 1),
                    timeZone)

        member _this.AddToCreateJob (auto : AutoTransfer) =
            let timeZone = timeZoneForAutoTransfer ctx auto
            let id = jobId timeZone.Id
            RecurringJob.AddOrUpdate<AutoTransferService>(
                id,
                getAction(timeZone.Id),
                Cron.Daily(0, 1),
                timeZone)

        member _this.CreateTransfers (timeZoneId : string) =
            let timeZone = timeZoneFromId timeZoneId
            let accountRepo = factory.Repository<Account>()
            let rates = (rateClient.GetRates()).Result
            let autoToAccount allAutos =
                let accIdToCurr = accountsForAutoTransfers accountRepo.Query allAutos
                fun (auto : AutoTransfer) -> accIdToCurr.[auto.AccountId]
            let autosToTransfers autos =
                let timeZoneDate = dateForTimeZone timeZone
                let mostRecent = fun start period numPeriods -> mostRecentDaysForPeriod start timeZoneDate period numPeriods
                getTransfersForAutoTransfers mostRecent (autoToAccount autos) rates autos
            let createTransfersForAutoTransfers =
                let autoRepo = factory.Repository<AutoTransfer>()
                let autoUpdate auto =
                    autoRepo.Update auto
                    autoRepo.SaveChangesAsync().Result |> ignore
                let createTransfers = fun transfers -> transferService.CreateTransfers transfers |> ignore
                createTransfersForAutoTransfers autoUpdate createTransfers
            let autoTransfers = autoTransfersForTimeZone timeZone
            let autosWithTransfers = autoTransfers |> autosToTransfers

            if List.isEmpty autoTransfers then
                RecurringJob.RemoveIfExists(jobId timeZoneId)

            if not <| List.isEmpty autosWithTransfers then
                createTransfersForAutoTransfers autosWithTransfers
