namespace Accounts.Services
    open Giraffe
    open FSharp.Control.Tasks.ContextInsensitive
    open Microsoft.AspNetCore.Http
    open Microsoft.Extensions.Configuration
    open MimeKit
    open Accounts.Data.Models
    open MailKit.Security
    open MailKit.Net.Smtp

    type Email = {
        User    : User
        Subject : string
        Body    : string
    }

    type EmailConfiguration = {
            FromAddress : string
            FromName    : string
            ApiUri      : string
            Port        : int
            User        : string
            Password    : string
        }

    type EmailService () =
        static let mailConfig (ctx : HttpContext) =
            let config = ctx.GetService<IConfiguration>()
            let mailSection = config.GetSection("Mail")
            if isNull mailSection
            then failwith "Missing mail configuration"

            let fromMailAddress = mailSection.GetValue<string>("FromMailAddress");
            let fromMailName = mailSection.GetValue<string>("FromMailName");
            let mailApiUri = mailSection.GetValue<string>("MailApiUri");
            let mailPort = mailSection.GetValue<int>("MailApiPort");
            let mailUser = mailSection.GetValue<string>("MailUser");
            let mailPass = mailSection.GetValue<string>("MailPassword");

            if isNull fromMailAddress
                || isNull fromMailName
                || isNull mailApiUri
                || isNull mailUser
                || isNull mailPass
            then failwith "Missing mail configuration"
            {
                FromAddress = fromMailAddress;
                FromName    = fromMailName;
                ApiUri      = mailApiUri;
                Port        = mailPort;
                User        = mailUser;
                Password    = mailPass
            }

        static let mailAddress name address = 
            MailboxAddress(name = name, address = address)

        static let mailMessage config (email : Email) = 
            let message = MimeMessage();
            message.From.Add(mailAddress config.FromName config.FromAddress)
            message.ReplyTo.Add(mailAddress config.FromName config.FromAddress);
            message.To.Add(mailAddress email.User.DisplayName email.User.Email)
            message.Subject <- email.Subject
            message.Body <- TextPart("html", Text = email.Body)
            message

        static member SendEmail (ctx : HttpContext) (email : Email) = task {
            let config = mailConfig ctx

            let message = mailMessage config email

            use client = new SmtpClient()
            try
                do! client.ConnectAsync(config.ApiUri, config.Port, SecureSocketOptions.Auto)
                do! client.AuthenticateAsync(config.User, config.Password)
                do! client.SendAsync(message)
                do! client.DisconnectAsync(true)
                return true
            with
            | _ -> return false
        }