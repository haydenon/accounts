namespace Accounts.Services

open System
open TimeZoneConverter
open Accounts.ViewModels

type TimeZoneService() =
    let mutable cached = []

    let getTimeZones () =
        if not (List.isEmpty cached)
        then
            cached
        else
            let knownTimeZones = TZConvert.KnownIanaTimeZoneNames |> List.ofSeq
            let timeZones =
                TimeZoneInfo.GetSystemTimeZones()
                |> List.ofSeq
                |> List.filter (fun tz -> List.contains tz.Id knownTimeZones)
                |> List.filter (fun tz -> List.contains tz.Id knownTimeZones)
                |> List.map (fun tz -> 
                    let offset = tz.BaseUtcOffset;
                    let offsetStr =
                        if tz = TimeZoneInfo.Utc
                        then String.Empty
                        else sprintf "%s:%s" (offset.Hours.ToString("+00;-#00")) (Math.Abs(offset.Minutes).ToString("00"))
                    { TimeZone = tz.Id; Display = TZConvert.WindowsToIana(TZConvert.IanaToWindows tz.Id) ;Offset = offsetStr})
            
            cached <- timeZones
            timeZones

    
    member _this.TimeZones = getTimeZones()
         