namespace Accounts.Services
    open System.Text
    open Giraffe
    open Microsoft.AspNetCore.WebUtilities
    open Microsoft.AspNetCore.Http
    open Microsoft.Extensions.Configuration
    open FSharp.Control.Tasks.ContextInsensitive
    open Microsoft.AspNetCore.Identity
    open Accounts.Data.Models
    open System.Threading.Tasks
    open Accounts.Errors

    type ResetPasswordService() =
        static let encode (token : string) =
            let bytes = Encoding.UTF8.GetBytes(token)
            WebEncoders.Base64UrlEncode(bytes)

        static let decode (encoded : string) =
            let bytes = WebEncoders.Base64UrlDecode(encoded)
            Encoding.UTF8.GetString(bytes)

        static let resetBody (user : User) resetUrl =
            sprintf """Hi %s<br>Someone recently requeseted a password reset for your account<br>
If this was you, to reset your password please click <a href="%s">here</a><br>
Otherwise, please ignore this email""" user.DisplayName resetUrl

        static let sendEmail (ctx : HttpContext) (user : User) = task {
            let config = ctx.GetService<IConfiguration>()
            let userManager = ctx.GetService<UserManager<User>>()
            let! token = userManager.GeneratePasswordResetTokenAsync(user)
            let encodedToken = encode token
            let encodedEmail = encode user.Email
            let baseUrl = config.GetValue("BaseUrl")
            let resetUrl = sprintf "%sresetPassword?user=%s&token=%s" baseUrl encodedEmail encodedToken
            return! EmailService.SendEmail ctx { User = user; Subject = "Reset Password"; Body = (resetBody user resetUrl)}
        }

        static let resetPassword (ctx : HttpContext) user token password = task {
            let userManager = ctx.GetService<UserManager<User>>()
            let! result = userManager.ResetPasswordAsync(user, token, password)
            let codes = Seq.map (fun (err : IdentityError) -> err.Code) result.Errors |> Seq.toList
            return
                match codes with
                | [ "InvalidToken" ] -> Some InvalidPasswordReset
                | _::_ -> Some PasswordMustContain
                | _ -> None
        }

        static member SendReset (ctx : HttpContext) email = task {
            let userManager = ctx.GetService<UserManager<User>>()
            let! user = userManager.FindByEmailAsync(email)
            let! succeeded =
                match isNull user with
                | true -> Task.FromResult false
                | false -> sendEmail ctx user

            return succeeded
        }

        static member ResetPassword (ctx : HttpContext) encodedEmail encodedToken password = task {
            let userManager = ctx.GetService<UserManager<User>>()
            let email = decode encodedEmail
            let token = decode encodedToken
            let! user = userManager.FindByEmailAsync(email)
            let! succeeded =
                match isNull user with
                | true -> Task.FromResult (Some InvalidPasswordReset)
                | false -> resetPassword ctx user token password

            return succeeded
        }