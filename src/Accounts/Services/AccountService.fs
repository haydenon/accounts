namespace Accounts.Services

open System.Linq

open FSharp.Control.Tasks.ContextInsensitive

open Accounts.Data.Models
open Accounts.Repository
open Accounts
open Rates
open Configuration

type ExchangeCurr = string * string * decimal -> decimal

module Accounts =
  let updateName update (original : Account) (updates : Account) =
    original.Name <- updates.Name
    update original

  let private updateAccountCurrency updateAccount updateTransfersForAccount (account : Account) newCurr =
    account.Currency <- newCurr
    updateAccount account
    updateTransfersForAccount account

  let updateCurrency updateAccount updateTransfersForAccount (current : Account) (updates : Account) =
    if updates.Currency <> current.Currency then
      updateAccountCurrency updateAccount updateTransfersForAccount current updates.Currency

  type AccountService(factory: RepositoryFactory, transferRepo : ITransferRepository, rates: IRateClient, config: ConfigurationService) =
    member __.UpdateAccount (acc : Account) = task {
      let accRepo = factory.Repository<Account>()
      let original = accRepo.FindById(acc.Id)
      let! rates = rates.GetRates()
      let exchangeForRates = ExchangeService.Exchange rates
      let currChangeEnabled = config.GetConfigBool "CurrencyChangeEnabled"
      return!
        match original with
        | None ->
          Task.value None
        | Some orig ->
          let originalCurr = orig.Currency
          let exchange amt = exchangeForRates(originalCurr, acc.Currency, amt)
          let updateTransfers = transferRepo.UpdateTransferCurrencyForAccount exchange
          do updateName accRepo.Update orig acc
          match currChangeEnabled with
          | true ->
            updateCurrency accRepo.Update updateTransfers orig acc
            accRepo.SaveChangesAsync()
            |> Task.map (fun _ -> Some orig)
          | false -> Task.value (Some orig)
}