namespace Accounts.Services

open System
open System.Security.Claims

open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Identity

open Accounts
open Accounts.Mapper2
open Accounts.Data.Models
open Accounts.Errors

type UserService(ctxAccessor : IHttpContextAccessor, usrMng: UserManager<User>, sgnMng: SignInManager<User>) =
  let ctx = ctxAccessor.HttpContext
  let authenticatedUser () =
    match isNull ctx with
    | true -> None
    | false ->
      let user = ctx.User
      if not(isNull user) && not(isNull user.Identity) && (user.Identity.IsAuthenticated)
      then Some user
      else None

  let errorForCreateResult email (error : IdentityError) =
    match error.Code with
    | "DuplicateUserName" -> DuplicateEmailAddress email
    | StartsWith "Password" -> PasswordMustContain
    | _ -> UnexpectedError

  member private __.User () =
    if isNull ctx then
      None
    else
      Some ctx.User

  member this.IsAuthenticated () =
    match authenticatedUser() with
    | None -> false
    | Some(_) -> true

  member this.CurrentUser () =
    match authenticatedUser() with
    | Some user  ->
      usrMng.GetUserAsync(user)
      |> Task.map (fun usr ->
        match usr with
        | NotNull -> User.mapFromModel usr |> Option.fromResult
        | _ -> None)
    | None -> Task.value None

  member this.CurrentUserId () =
    match authenticatedUser() with
    | Some user  -> Some(UserId(usrMng.GetUserId user))
    | None -> None

  member this.CurrentUserName () =
    match authenticatedUser() with
    | Some user -> user.Identity.Name
    | None -> String.Empty