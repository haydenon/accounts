module Accounts.Services.TransfersSummary

open Accounts
open Accounts.Errors
open Accounts.Mapper2
open Accounts.Models
open Accounts.Repository
open Accounts.Flow2

let private summariesForAccount (summaryRepo : ITransfersSummaryRepository) = summaryRepo.SummariesForAccount

let private summariesForAllAccounts (summaryRepo : ITransfersSummaryRepository) = summaryRepo.SummariesForAllAccounts

let getSummaries getUserId fetchSummaries period start finish : AppResult<TransfersSummary list> =
  match getUserId() with
  | Some userId ->
    fetchSummaries userId period start finish
    |> List.map TransfersSummary.mapFromModel
    |> AppResult.sequence
  | None -> Error [ UnexpectedError ]

type TransfersSummaryService(summaryRepo: ITransfersSummaryRepository, userService: UserService) =
  let getUserId () = getUserId userService
  member __.GetSummariesForAccount accountId period startDate endDate : AppResult<TransfersSummary list> =
    getSummaries getUserId (summariesForAccount summaryRepo accountId) period startDate endDate

  member __.GetSummariesForAllAccounts period startDate endDate : AppResult<TransfersSummary list> =
    getSummaries getUserId (summariesForAllAccounts summaryRepo) period startDate endDate