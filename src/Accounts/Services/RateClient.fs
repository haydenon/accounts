module Accounts.Services.Rates

open System
open System.Threading.Tasks
open System.Net.Http
open Giraffe
open Microsoft.Extensions.Configuration
open Newtonsoft.Json
open Accounts.ViewModels

let getRate rates currency= task {
    let! rates = rates
    return
        if rates.Base = currency
        then 1.0m
        else Map.find currency rates.Rates
}

let getRateNames rates () = task {
    let! rates = rates
    let names =
        rates.Rates
        |> Map.toList
        |> List.map fst
        |> List.append [rates.Base] 
    return names
}

type IRateClient =
    abstract member GetRates     : unit -> Task<RatesView>
    abstract member GetRate      : string -> Task<decimal>
    abstract member GetRateNames : unit -> Task<string list>

type RateClient(config : IConfiguration) = 
    let apiEndpoint = config.GetSection("CurrencyApi").GetValue("Endpoint")
    let token = config.GetSection("CurrencyApi").GetValue("Token")
    let url = sprintf "%slatest?access_key=%s" apiEndpoint token

    let mutable nextCheck = DateTime.UtcNow.Date.AddDays(-1.0)
    let mutable cached = { Date = ""; Base = ""; Rates = Map.empty<string, decimal> }

    let toCET (date : DateTime) =
        date.Add(TimeSpan.FromHours(1.0))

    let getFromProvider () = task {
        let client = new HttpClient();
        do client.Timeout <- TimeSpan.FromSeconds(10.0)
        let! result = client.GetStringAsync(url);
        let rates = JsonConvert.DeserializeObject<RatesView>(result)
        do cached <- rates
        let cet = toCET(DateTime.UtcNow)
        do nextCheck <-
            if cet.Hour >= 17 then
                cet.Date.AddDays(1.0).AddHours(17.00)
            else
                cet.Date.AddHours(17.00)    
        return rates
    } 

    member _this.GetRates () = task {
        let cet = toCET(DateTime.UtcNow)
        let! rates =
            if cet >= nextCheck then
                getFromProvider()
            else
                Task.FromResult cached
                
        return rates
    }

    interface IRateClient with
        member this.GetRates () = this.GetRates()
        member this.GetRate name = getRate (this.GetRates()) name
        member this.GetRateNames () = getRateNames (this.GetRates()) ()
    
type MockRateClient() =
    let file = IO.File.ReadAllText("currency_mock_data.json")
    let rates = Task.FromResult <| JsonConvert.DeserializeObject<RatesView>(file)

    interface IRateClient with
        member __.GetRates () =
            rates
        member __.GetRate name = getRate rates name
        member __.GetRateNames () = getRateNames rates ()