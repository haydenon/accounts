namespace Accounts.Services

open System
open System.Threading.Tasks
open Accounts
open Accounts.Data.Models
open Accounts.Repository
open FSharp.Control.Tasks.ContextInsensitive

type TransferUpdate = {
    Transfer             : Transfer
    BalanceChange : Decimal
}

type TransferService(repo : ITransferRepository, factory : RepositoryFactory) =
    let currencyForTransfer (transfer : Transfer) =
        query {
            for acc in factory.Repository<Account>().Query do
            where (acc.Id = transfer.AccountId)
            select acc.Currency
        } |> Seq.head

    let updateTransferValues (update : Transfer) (original : Transfer) round =
        update.DisplayAmount <- update.Amount
        let exchangeRate = original.Amount / original.DisplayAmount
        update.Amount <- round (update.Amount * exchangeRate)

        original.Description      <- update.Description
        original.Date             <- update.Date
        original.Amount           <- update.Amount
        original.DisplayAmount    <- update.DisplayAmount
        original.OriginalAmount   <- update.OriginalAmount
        original.OriginalCurrency <- update.OriginalCurrency

    let updateTransfer (update : Transfer) (original : Transfer) =
        let accountRepo = factory.Repository<Account>()
        let account = accountRepo.FindById original.AccountId
        let update (account : Account) =
            let round = ExchangeService.RoundForCurrency account.Currency
            updateTransferValues update original round
            repo.UpdateTransfer original

        match account with
        | Some account -> update account
        | None -> ()

        original


    member __.CreateTransfer (transfer : Transfer) =
        if String.IsNullOrWhiteSpace(transfer.Currency) then
            transfer.Currency <- currencyForTransfer transfer
        repo.CreateTransfer transfer

    member this.CreateTransfers (transfers : Transfer list) =
        let createTransfers transfers =
            let curr = currencyForTransfer (List.head transfers)
            List.iter (fun (t : Transfer) -> t.Currency <- curr) transfers
            List.map this.CreateTransfer transfers
        match transfers with
        | _::_ ->
            transfers
            |> List.groupBy (fun t -> t.AccountId)
            |> List.collect (snd >> createTransfers)
        | _ -> []

    member _this.UpdateTransfer (update : Transfer) =
        let original = repo.FindById update.Id
        match original with
        | Some orig ->
            let originalAmount = orig.Amount
            let updated = updateTransfer update orig
            Some{ Transfer = updated; BalanceChange = updated.Amount - originalAmount }
        | None -> None

    member _this.DeleteTransfer (id : int64) =
        let trans = repo.FindById(id)
        match trans with
        | Some transfer ->
            let amount = transfer.Amount
            repo.DeleteTransfer transfer
            Some amount
        | None -> None
