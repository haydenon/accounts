namespace Accounts.Services

open System
open Accounts.ViewModels

type ExchangeService() =
    static let (|Equals|_|) a v =
        if a = v then Some() else None

    static let precision curr =
        match curr with
        | "BIF" | "BYR" | "CLP" | "DJF"
        | "GNF" | "ILS" | "JPY" | "KMF"
        | "KRW" | "MGA" | "PYG" | "RWF"
        | "UGX" | "VND" | "VUV" | "XAF"
        | "XOF" | "XPF" ->
            0
        | "BHD" | "IQD" | "JOD" | "KWD"
        | "OMR" | "TND" ->
          3
        | _ ->
          2

    static member Exchange(rates: RatesView) (source, target, amount : decimal) =
        if source = target then
            amount
        else
            let sourceRate =
                match source with
                | Equals rates.Base -> 1.0m
                | _ -> rates.Rates.[source]

            let targetRate =
                match target with
                | Equals rates.Base -> 1.0m
                | _ -> rates.Rates.[target]

            let baseAmount = amount / sourceRate
            let precision = precision target
            Math.Round(baseAmount * targetRate, precision)

    static member RoundForCurrency currency (amount : decimal) =
        let precision = precision currency
        Math.Round(amount, precision)