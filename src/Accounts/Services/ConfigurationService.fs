module Accounts.Services.Configuration

open Microsoft.Extensions.Configuration

type ConfigurationService(configuration: IConfiguration) =
    let invalidMessage = sprintf "Configuration item '%s' of type '%s' doesn't exist"

    let config = configuration.GetSection("Configuration")

    let getItem (item : IConfigurationSection) : string * obj =
        match item.GetValue("type") with
        | "boolean" -> item.Key, box(item.GetValue<bool>("value"))
        | "string" -> item.Key, box(item.GetValue<string>("value"))
        | "number" -> item.Key, box(item.GetValue<float>("value"))
        | _ -> failwith (sprintf "Invalid configuration item %s" item.Key)

    member __.GetConfigBool name =
        let configSection = config.GetSection(name)
        match configSection.GetValue("type") with
        | "boolean" -> configSection.GetValue<bool>("value")
        | _ -> failwith (invalidMessage name "boolean")

    member __.GetConfigString name =
        let configSection = config.GetSection(name)
        match configSection.GetValue("type") with
        | "string" -> configSection.GetValue<string>("value")
        | _ -> failwith (invalidMessage name "string")

    member __.GetConfigNumber name =
        let configSection = config.GetSection(name)
        match configSection.GetValue("type") with
        | "number" -> configSection.GetValue<float>("value")
        | _ -> failwith (invalidMessage name "float")

    member __.GetFrontEndConfiguration() =
        config.GetChildren()
        |> Seq.filter (fun i -> i.GetValue<bool>("expose"))
        |> Seq.map getItem
        |> dict