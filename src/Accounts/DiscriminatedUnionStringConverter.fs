namespace Accounts

open System
open Accounts.ReferenceData.Helpers
open Newtonsoft.Json
open Newtonsoft.Json.Converters

type DiscriminatedUnionStringConverter() =
    inherit DiscriminatedUnionConverter()

    override _this.WriteJson(writer: JsonWriter, value: obj, _: JsonSerializer) : unit =
        writer.WriteValue(toString (unbox value));

    override _this.ReadJson(reader: JsonReader, objectType: Type, _: obj, _: JsonSerializer) : obj =
            if reader.TokenType = JsonToken.String
            then
                let caseName = reader.Value.ToString()
                match fromString objectType caseName with
                | Some v -> v
                | None -> null
            else
                null
                
