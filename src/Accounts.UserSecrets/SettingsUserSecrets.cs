﻿using System;
using Microsoft.Extensions.Configuration;

namespace Accounts.UserSecrets
{
    public static class SettingsUserSecrets
    {
        public static IConfigurationBuilder AddUserSecretsToSettings(this IConfigurationBuilder settings, string id)
        {
            return settings.AddUserSecrets(id);
        }
    }
}
