﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using System;
using System.Collections.Generic;

namespace Accounts.Data.Migrations
{
    public partial class MoveaccountbalanceandcurrencytoAccountBalancerecord : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AccountDetails",
                columns: table => new
                {
                    Id = table.Column<long>(type: "int8", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccountId = table.Column<long>(type: "int8", nullable: false),
                    CreatedBy = table.Column<string>(type: "text", nullable: true),
                    CreatedOn = table.Column<DateTime>(type: "timestamp", nullable: false),
                    Currency = table.Column<string>(type: "text", nullable: true),
                    CurrentBalance = table.Column<decimal>(type: "numeric", nullable: false),
                    ModifiedBy = table.Column<string>(type: "text", nullable: true),
                    ModifiedOn = table.Column<DateTime>(type: "timestamp", nullable: false),
                    StartBalance = table.Column<decimal>(type: "numeric", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDetails_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(@"
                INSERT INTO ""AccountDetails""
                (""AccountId"", ""Currency"", ""CurrentBalance"", ""StartBalance"", ""CreatedBy"", ""CreatedOn"", ""ModifiedBy"", ""ModifiedOn"")
                SELECT a.""Id"", a.""Currency"", a.""Balance"", 0, a.""CreatedBy"", now()::timestamp without time zone, a.""ModifiedBy"", now()::timestamp without time zone
                FROM ""Accounts"" AS a;
            ");

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "AutoTransfer",
                type: "text",
                nullable: true);

            migrationBuilder.Sql(@"
                UPDATE ""AutoTransfer"" AS at
                SET ""Currency"" =
                    (SELECT a.""Currency""
                    FROM ""Accounts"" AS a
                    WHERE a.""Id"" = at.""AccountId""
                    LIMIT 1)
                WHERE at.""OriginalCurrency"" IS NULL;

                UPDATE ""AutoTransfer"" AS at
                SET ""Currency"" = at.""OriginalCurrency"",
                ""Amount"" = at.""OriginalAmount""
                WHERE at.""OriginalCurrency"" IS NOT NULL;
            ");

            migrationBuilder.AlterColumn<string>(
                name: "Currency",
                table: "AutoTransfer",
                type: "text",
                nullable: false);

            migrationBuilder.DropColumn(
                name: "Balance",
                table: "Accounts");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Accounts");

            migrationBuilder.AddColumn<long>(
                name: "AccountDetailsId",
                table: "Transfer",
                type: "int8",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Transfer_AccountDetailsId",
                table: "Transfer",
                column: "AccountDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDetails_AccountId",
                table: "AccountDetails",
                column: "AccountId");

            migrationBuilder.Sql(@"
                UPDATE ""Transfer"" AS t
                SET ""AccountDetailsId"" =
                    (SELECT ad.""Id""
                    FROM ""AccountDetails"" AS ad
                    WHERE ad.""AccountId"" = t.""AccountId""
                    LIMIT 1);
            ");

            migrationBuilder.AddForeignKey(
                name: "FK_Transfer_AccountDetails_AccountDetailsId",
                table: "Transfer",
                column: "AccountDetailsId",
                principalTable: "AccountDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.DropForeignKey(
                name: "FK_Transfer_Accounts_AccountId",
                table: "Transfer");

            migrationBuilder.DropIndex(
                name: "IX_Transfer_AccountId",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "AccountId",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "OriginalAmount",
                table: "AutoTransfer");

            migrationBuilder.DropColumn(
                name: "OriginalCurrency",
                table: "AutoTransfer");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "AccountId",
                table: "Transfer",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<decimal>(
                name: "OriginalAmount",
                table: "AutoTransfer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OriginalCurrency",
                table: "AutoTransfer",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Balance",
                table: "Accounts",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Accounts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transfer_AccountId",
                table: "Transfer",
                column: "AccountId");


            migrationBuilder.Sql(@"
                UPDATE ""Transfer"" AS t
                SET ""AccountId"" =
                    (SELECT ad.""AccountId""
                    FROM ""AccountDetails"" AS ad
                    WHERE ad.""Id"" = t.""AccountDetailsId""
                    LIMIT 1);

                DELETE FROM ""Transfer"" AS t
                WHERE t.""AccountDetailsId"" <> (
                    SELECT ad.""Id""
                    FROM ""AccountDetails"" AS ad
                    WHERE ad.""AccountId"" = t.""AccountId""
                    ORDER BY ad.""CreatedOn""
                    LIMIT 1
                );
            ");

            migrationBuilder.AddForeignKey(
                name: "FK_Transfer_Accounts_AccountId",
                table: "Transfer",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.Sql(@"
                UPDATE ""Accounts"" AS a
                SET
                    ""Balance"" = (
                        SELECT ad.""CurrentBalance""
                        FROM ""AccountDetails"" AS ad
                        WHERE ad.""AccountId"" = a.""Id""
                        ORDER BY ad.""CreatedOn""
                        LIMIT 1 
                    ),
                    ""Currency"" = (
                        SELECT ad.""Currency""
                        FROM ""AccountDetails"" AS ad
                        WHERE ad.""AccountId"" = a.""Id""
                        ORDER BY ad.""CreatedOn""
                        LIMIT 1 
                    );
            ");

            migrationBuilder.Sql(@"
                UPDATE ""AutoTransfer"" AS at
                SET ""OriginalCurrency"" = at.""Currency"",
                ""OriginalAmount"" = at.""Amount"",
                ""Amount"" = 0
                WHERE at.""Currency"" <> (
                    SELECT a.""Currency""
                    FROM ""Accounts"" AS a
                    WHERE a.""Id"" = at.""AccountId""
                );
            ");

            migrationBuilder.DropForeignKey(
                name: "FK_Transfer_AccountDetails_AccountDetailsId",
                table: "Transfer");

            migrationBuilder.DropIndex(
                name: "IX_Transfer_AccountDetailsId",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "AccountDetailsId",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "AutoTransfer");

            migrationBuilder.DropTable(
                name: "AccountDetails");
        }
    }
}
