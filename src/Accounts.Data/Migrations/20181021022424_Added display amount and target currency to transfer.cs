﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounts.Data.Migrations
{
    public partial class Addeddisplayamountandtargetcurrencytotransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "OriginalCurrency",
                table: "Transfer",
                maxLength: 3,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Transfer",
                maxLength: 3,
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "DisplayAmount",
                table: "Transfer",
                nullable: true);

            migrationBuilder.Sql(@"
                UPDATE ""Transfer"" t
                SET
                    ""DisplayAmount"" = ""Amount"",
                    ""Currency"" = (
                        SELECT ad.""Currency""
                        FROM ""AccountDetails"" AS ad
                        WHERE ad.""Id"" = t.""AccountDetailsId""
                        LIMIT 1
                    );
            ");

            migrationBuilder.AlterColumn<decimal>(
                name: "DisplayAmount",
                table: "Transfer",
                nullable: false);

            migrationBuilder.AlterColumn<string>(
                name: "Currency",
                table: "AccountDetails",
                maxLength: 3,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "DisplayAmount",
                table: "Transfer");

            migrationBuilder.AlterColumn<string>(
                name: "OriginalCurrency",
                table: "Transfer",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 3,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Currency",
                table: "AccountDetails",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 3,
                oldNullable: true);
        }
    }
}
