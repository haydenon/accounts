﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Accounts.Data.Migrations
{
    public partial class Addedlasttransferdatetoautotransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "AutoTransfer",
                table: "Transfer",
                type: "bool",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastTransfer",
                table: "AutoTransfer",
                type: "timestamp",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AutoTransfer",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "LastTransfer",
                table: "AutoTransfer");
        }
    }
}
