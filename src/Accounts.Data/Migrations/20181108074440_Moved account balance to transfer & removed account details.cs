﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Accounts.Data.Migrations
{
    public partial class Movedaccountbalancetotransferremovedaccountdetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Currency",
                table: "Accounts",
                maxLength: 3,
                nullable: true);

            migrationBuilder.DropForeignKey(
                name: "FK_Transfer_AccountDetails_AccountDetailsId",
                table: "Transfer");

            migrationBuilder.Sql(@"
UPDATE ""Transfer"" t
SET ""AccountDetailsId"" = (
    SELECT ""AccountId""
    FROM ""AccountDetails""
    WHERE ""Id"" = t.""AccountDetailsId""
    LIMIT 1
);

UPDATE ""Accounts"" a
SET ""Currency"" = (
    SELECT ""Currency""
    FROM ""AccountDetails""
    WHERE ""AccountId"" = a.""Id""
    LIMIT 1
);
            ");

            migrationBuilder.AlterColumn<string>(
                name: "Currency",
                table: "Accounts",
                maxLength: 3,
                nullable: false);

            migrationBuilder.DropTable(
                name: "AccountDetails");

            migrationBuilder.RenameColumn(
                name: "AccountDetailsId",
                table: "Transfer",
                newName: "AccountId");

            migrationBuilder.RenameIndex(
                name: "IX_Transfer_AccountDetailsId",
                table: "Transfer",
                newName: "IX_Transfer_AccountId");

            migrationBuilder.AddColumn<decimal>(
                name: "AccountBalance",
                table: "Transfer",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.Sql(@"
UPDATE ""Transfer"" AS tup
SET ""AccountBalance"" = tbal.""AccountBalance""
FROM
(
    SELECT t.""Id"", coalesce(ttotal.""TotalAmount"", 0)+ t.""Amount"" AS ""AccountBalance""
    FROM ""Transfer"" t
    LEFT JOIN (
        SELECT t1.""Id"", SUM(t2.""Amount"") AS ""TotalAmount""
        FROM ""Transfer"" t1
        INNER JOIN ""Transfer"" t2
        ON (
            (t1.""Date"" > t2.""Date"" -- Transfers that are before t1
            OR (t1.""Date"" = t2.""Date"" AND t1.""CreatedOn"" > t2.""CreatedOn""))  -- Or on the same date, but created before
            AND t1.""AccountId"" = t2.""AccountId""
        )
        GROUP BY t1.""Id""
    ) AS ttotal ON ttotal.""Id"" = t.""Id""
) AS tbal
WHERE tup.""Id"" = tbal.""Id"";
            ");

            migrationBuilder.AddForeignKey(
                name: "FK_Transfer_Accounts_AccountId",
                table: "Transfer",
                column: "AccountId",
                principalTable: "Accounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION create_transfer(
 account_id bigint,
 amount numeric,
 currency character varying(3),
 original_amount numeric,
 original_currency character varying(3),
 transfer_date timestamp without time zone,
 description text,
 auto_transfer boolean,
 created_on timestamp without time zone,
 created_by text)
 RETURNS TABLE (
 ""TransferId"" bigint,
 ""AccountBalance"" numeric
)
AS $$
 DECLARE transfer_id bigint;
 DECLARE account_balance numeric;
BEGIN
 SELECT
 t.""AccountBalance"" + amount
 INTO account_balance
 FROM ""Transfer"" t
 WHERE t.""AccountId"" = account_id
 AND (t.""Date"" < transfer_date
 OR t.""Date"" = transfer_date AND t.""CreatedOn"" < created_on)
 ORDER BY t.""Date"" DESC, t.""CreatedOn"" DESC
 LIMIT 1;

 INSERT INTO ""Transfer""
 (
   ""AccountId"",
   ""Amount"",
   ""DisplayAmount"",
   ""Currency"",
   ""OriginalAmount"",
   ""OriginalCurrency"",
   ""Date"",
   ""Description"",
   ""AutoTransfer"",
   ""AccountBalance"",
   ""CreatedBy"",
   ""ModifiedBy"",
   ""CreatedOn"",
   ""ModifiedOn""
 )
 VALUES
 (
  account_id,
  amount,
  amount,
  currency,
  original_amount,
  original_currency,
  transfer_date,
  description,
  auto_transfer,
  account_balance,
  created_by,
  created_by,
  created_on,
  created_on
 )
 RETURNING ""Id"" INTO transfer_id;

 UPDATE ""Transfer"" t
 SET ""AccountBalance"" = t.""AccountBalance"" + amount
 WHERE t.""AccountId"" = account_id
 AND (t.""Date"" > transfer_date
 OR t.""Date"" = transfer_date AND t.""CreatedOn"" > created_on);

 RETURN QUERY SELECT transfer_id AS ""TransferId"", account_balance AS ""AccountBalance"";
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION update_transfer(
 transfer_id bigint,
 amount numeric,
 display_amount numeric,
 original_amount numeric,
 original_currency character varying(3),
 transfer_date timestamp without time zone,
 description text,
 modified_on timestamp without time zone,
 modified_by text)
 RETURNS void
AS $$
 DECLARE previous_amount numeric;
 DECLARE previous_date timestamp without time zone;
 DECLARE previous_created timestamp without time zone;
 DECLARE date_amount_changed boolean;
BEGIN
 SELECT ""Amount"", ""Date"", ""CreatedOn""
 INTO previous_amount, previous_date, previous_created
 FROM ""Transfer""
 WHERE ""Id"" = transfer_id
 LIMIT 1;

 SELECT 1
 INTO date_amount_changed
 WHERE previous_amount <> amount
 OR previous_date <> transfer_date;

 UPDATE ""Transfer""
 SET
    ""Amount"" = amount,
    ""DisplayAmount"" = display_amount,
    ""OriginalAmount"" = original_amount,
    ""OriginalCurrency"" = original_currency,
    ""Date"" = transfer_date,
    ""Description"" = description,
    ""ModifiedOn"" = modified_on,
    ""ModifiedBy"" = modified_by
 WHERE ""Id"" = transfer_id;

 IF date_amount_changed THEN
	 UPDATE ""Transfer""
	 SET ""AccountBalance"" = ""AccountBalance"" - previous_amount
	 WHERE ""Id"" <> transfer_id
	 AND (""Date"" > previous_date
	 OR (""Date"" = previous_date AND ""CreatedOn"" > previous_created));

	 UPDATE ""Transfer""
	 SET ""AccountBalance"" = ""AccountBalance"" + amount
	 WHERE ""Id"" <> transfer_id
	 AND (""Date"" > transfer_date
	 OR (""Date"" = transfer_date AND ""CreatedOn"" > previous_created));

     UPDATE ""Transfer"" t
     SET ""AccountBalance"" = t.""Amount"" + COALESCE((
        SELECT t1.""AccountBalance""
        FROM ""Transfer"" t1
        WHERE t1.""AccountId"" = t.""AccountId""
        AND (t1.""Date"" < t.""Date""
        OR (t1.""Date"" = t.""Date"" AND t1.""CreatedOn"" < t.""CreatedOn""))
        ORDER BY t1.""Date"" DESC, t1.""CreatedOn"" DESC
        LIMIT 1
     ), 0)
     WHERE ""Id"" = transfer_id;
 END IF;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION delete_transfer(transfer_id bigint)
 RETURNS void
AS $$
 DECLARE amount numeric;
 DECLARE transfer_date timestamp without time zone;
 DECLARE created_on timestamp without time zone;
BEGIN
 SELECT ""Amount"", ""Date"", ""CreatedOn""
 INTO amount, transfer_date, created_on
 FROM ""Transfer""
 WHERE ""Id"" = transfer_id
 LIMIT 1;

 DELETE FROM ""Transfer""
 WHERE ""Id"" = transfer_id;

 UPDATE ""Transfer""
 SET ""AccountBalance"" = ""AccountBalance"" - amount
 WHERE ""Date"" > transfer_date
 OR (""Date"" = transfer_date AND ""CreatedOn"" > created_on);
END; $$
LANGUAGE PLPGSQL;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION create_transfer(bigint,numeric,character varying,numeric,character varying,timestamp without time zone,text,boolean,timestamp without time zone,text);
            ");

            migrationBuilder.Sql(@"
DROP FUNCTION update_transfer(bigint, numeric, numeric, numeric, character varying, timestamp without time zone, text, timestamp without time zone, text);
            ");

            migrationBuilder.Sql(@"
DROP FUNCTION delete_transfer(bigint);
            ");

            migrationBuilder.DropForeignKey(
                name: "FK_Transfer_Accounts_AccountId",
                table: "Transfer");

            migrationBuilder.DropColumn(
                name: "AccountBalance",
                table: "Transfer");


            migrationBuilder.RenameIndex(
                name: "IX_Transfer_AccountId",
                table: "Transfer",
                newName: "IX_Transfer_AccountDetailsId");

            migrationBuilder.CreateTable(
                name: "AccountDetails",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    AccountId = table.Column<long>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    Currency = table.Column<string>(maxLength: 3, nullable: true),
                    CurrentBalance = table.Column<decimal>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedOn = table.Column<DateTime>(nullable: false),
                    StartBalance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccountDetails_Accounts_AccountId",
                        column: x => x.AccountId,
                        principalTable: "Accounts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.Sql(@"
                INSERT INTO ""AccountDetails""
                (""AccountId"", ""Currency"", ""CurrentBalance"", ""StartBalance"", ""CreatedBy"", ""CreatedOn"", ""ModifiedBy"", ""ModifiedOn"")
                SELECT
                    a.""Id"",
                    a.""Currency"",
                    (
                        SELECT COALESCE(SUM(""Amount""), 0)
                        FROM ""Transfer""
                        WHERE ""AccountId"" = a.""Id""
                        GROUP BY ""AccountId""
                    ),
                    0,
                    a.""CreatedBy"",
                    now()::timestamp without time zone,
                    a.""ModifiedBy"",
                    now()::timestamp without time zone
                FROM ""Accounts"" AS a;
            ");

            migrationBuilder.DropColumn(
                name: "Currency",
                table: "Accounts");

            migrationBuilder.RenameColumn(
                name: "AccountId",
                table: "Transfer",
                newName: "AccountDetailsId");

            migrationBuilder.Sql(@"
                UPDATE ""Transfer"" AS t
                SET ""AccountDetailsId"" =
                    (SELECT ad.""Id""
                    FROM ""AccountDetails"" AS ad
                    WHERE ad.""AccountId"" = t.""AccountDetailsId""
                    LIMIT 1);
            ");

            migrationBuilder.CreateIndex(
                name: "IX_AccountDetails_AccountId",
                table: "AccountDetails",
                column: "AccountId");

            migrationBuilder.AddForeignKey(
                name: "FK_Transfer_AccountDetails_AccountDetailsId",
                table: "Transfer",
                column: "AccountDetailsId",
                principalTable: "AccountDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
