﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounts.Data.Migrations
{
    public partial class Addedtransferfunctionfix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION create_transfer(
 account_id bigint,
 amount numeric,
 currency character varying(3),
 original_amount numeric,
 original_currency character varying(3),
 transfer_date timestamp without time zone,
 description text,
 auto_transfer boolean,
 created_on timestamp without time zone,
 created_by text)
 RETURNS TABLE (
 ""TransferId"" bigint,
 ""AccountBalance"" numeric
)
AS $$
 DECLARE transfer_id bigint;
 DECLARE account_balance numeric;
BEGIN
 SELECT COALESCE((
    SELECT t.""AccountBalance""
    FROM ""Transfer"" t
    WHERE t.""AccountId"" = account_id
    AND (t.""Date"" < transfer_date
    OR t.""Date"" = transfer_date AND t.""CreatedOn"" < created_on)
    ORDER BY t.""Date"" DESC, t.""CreatedOn"" DESC
    LIMIT 1
 ), 0) + amount
 INTO account_balance;

 INSERT INTO ""Transfer""
 (
   ""AccountId"",
   ""Amount"",
   ""DisplayAmount"",
   ""Currency"",
   ""OriginalAmount"",
   ""OriginalCurrency"",
   ""Date"",
   ""Description"",
   ""AutoTransfer"",
   ""AccountBalance"",
   ""CreatedBy"",
   ""ModifiedBy"",
   ""CreatedOn"",
   ""ModifiedOn""
 )
 VALUES
 (
  account_id,
  amount,
  amount,
  currency,
  original_amount,
  original_currency,
  transfer_date,
  description,
  auto_transfer,
  account_balance,
  created_by,
  created_by,
  created_on,
  created_on
 )
 RETURNING ""Id"" INTO transfer_id;

 UPDATE ""Transfer"" t
 SET ""AccountBalance"" = t.""AccountBalance"" + amount
 WHERE t.""AccountId"" = account_id
 AND (t.""Date"" > transfer_date
 OR t.""Date"" = transfer_date AND t.""CreatedOn"" > created_on);

 RETURN QUERY SELECT transfer_id AS ""TransferId"", account_balance AS ""AccountBalance"";
END; $$
LANGUAGE PLPGSQL;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION create_transfer(
 account_id bigint,
 amount numeric,
 currency character varying(3),
 original_amount numeric,
 original_currency character varying(3),
 transfer_date timestamp without time zone,
 description text,
 auto_transfer boolean,
 created_on timestamp without time zone,
 created_by text)
 RETURNS TABLE (
 ""TransferId"" bigint,
 ""AccountBalance"" numeric
)
AS $$
 DECLARE transfer_id bigint;
 DECLARE account_balance numeric;
BEGIN
 SELECT
 t.""AccountBalance"" + amount
 INTO account_balance
 FROM ""Transfer"" t
 WHERE t.""AccountId"" = account_id
 AND (t.""Date"" < transfer_date
 OR t.""Date"" = transfer_date AND t.""CreatedOn"" < created_on)
 ORDER BY t.""Date"" DESC, t.""CreatedOn"" DESC
 LIMIT 1;

 INSERT INTO ""Transfer""
 (
   ""AccountId"",
   ""Amount"",
   ""DisplayAmount"",
   ""Currency"",
   ""OriginalAmount"",
   ""OriginalCurrency"",
   ""Date"",
   ""Description"",
   ""AutoTransfer"",
   ""AccountBalance"",
   ""CreatedBy"",
   ""ModifiedBy"",
   ""CreatedOn"",
   ""ModifiedOn""
 )
 VALUES
 (
  account_id,
  amount,
  amount,
  currency,
  original_amount,
  original_currency,
  transfer_date,
  description,
  auto_transfer,
  account_balance,
  created_by,
  created_by,
  created_on,
  created_on
 )
 RETURNING ""Id"" INTO transfer_id;

 UPDATE ""Transfer"" t
 SET ""AccountBalance"" = t.""AccountBalance"" + amount
 WHERE t.""AccountId"" = account_id
 AND (t.""Date"" > transfer_date
 OR t.""Date"" = transfer_date AND t.""CreatedOn"" > created_on);

 RETURN QUERY SELECT transfer_id AS ""TransferId"", account_balance AS ""AccountBalance"";
END; $$
LANGUAGE PLPGSQL;
            ");
        }
    }
}
