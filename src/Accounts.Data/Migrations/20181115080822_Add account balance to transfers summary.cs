﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounts.Data.Migrations
{
    public partial class Addaccountbalancetotransferssummary : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION get_transfers_summary_by_month(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);

DROP FUNCTION get_transfers_summary_by_week(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);

DROP FUNCTION get_transfers_summary_by_day(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_week(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartBalance"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 CREATE TEMP TABLE IF NOT EXISTS transfer_by_week AS
    SELECT
		""Id"" AS transfer_id,
		to_char(""Date"", 'IYYY-IW') AS year_week,
		t.""AccountBalance"" - t.""Amount"" AS start_balance,
		""Date"" as transfer_date,
		""CreatedOn"" as created_on
    FROM ""Transfer"" AS t
	WHERE t.""AccountId"" IN (
		SELECT a2.""Id"" FROM ""Accounts"" AS a2
	   	LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
	   	WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
	   	OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
   	);

 RETURN QUERY SELECT
 sq.account_id AS ""AccountId"", sq.amount AS ""Amount"", sq.start_balance AS ""StartBalance"", to_date(sq.year_week, 'IYYY-IW')::timestamp AS ""StartDate""
 FROM (
  SELECT
    t.""AccountId"" AS account_id,
    SUM(t.""Amount"") AS amount,
	(
		SELECT start_balance
		FROM transfer_by_week AS tbw2
		WHERE tbw.year_week = tbw2.year_week
		ORDER BY transfer_date, created_on
		LIMIT 1
	) AS start_balance,
    tbw.year_week
  FROM ""Transfer"" AS t
  INNER JOIN transfer_by_week AS tbw ON t.""Id"" = tbw.transfer_id
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY tbw.year_week, t.""AccountId""
  ORDER BY tbw.year_week
 ) AS sq;

 DROP TABLE transfer_by_week;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_month(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartBalance"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 CREATE TEMP TABLE IF NOT EXISTS transfer_by_month AS
    SELECT
    ""Id"" AS transfer_id,
    to_char(""Date"", 'YYYY-MM') AS year_month,
    t.""AccountBalance"" - t.""Amount"" AS start_balance,
    ""Date"" as transfer_date,
    ""CreatedOn"" as created_on
    FROM ""Transfer"" AS t
  WHERE t.""AccountId"" IN (
    SELECT a2.""Id"" FROM ""Accounts"" AS a2
      LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
      WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
      OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
    );

 RETURN QUERY SELECT
 sq.account_id AS ""AccountId"", sq.amount AS ""Amount"", sq.start_balance AS ""StartBalance"", to_date(sq.year_month, 'YYYY-MM')::timestamp AS ""StartDate""
 FROM (
  SELECT
    t.""AccountId"" AS account_id,
    SUM(t.""Amount"") AS amount,
    (
    SELECT start_balance
    FROM transfer_by_month AS tbm2
    WHERE tbm.year_month = tbm2.year_month
    ORDER BY transfer_date, created_on
    LIMIT 1
  ) AS start_balance,
    tbm.year_month
  FROM ""Transfer"" AS t
  INNER JOIN transfer_by_month AS tbm ON t.""Id"" = tbm.transfer_id
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY tbm.year_month, t.""AccountId""
  ORDER BY tbm.year_month
 ) AS sq;

 DROP TABLE transfer_by_month;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_day(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartBalance"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 	a.""Id"" AS ""AccountId"",
	SUM(t.""Amount"") AS ""Amount"",
	(
		SELECT t2.""AccountBalance"" - t2.""Amount""
		FROM ""Transfer"" AS t2
		WHERE t.""Date"" = t2.""Date""
		ORDER BY t2.""Date"", t2.""CreatedOn""
		LIMIT 1
	) AS ""StartBalance"",
	t.""Date""
 FROM ""Transfer"" AS t
 INNER JOIN ""Accounts"" AS a ON t.""AccountId"" = a.""Id""
 WHERE a.""Id"" IN (
  SELECT a2.""Id"" FROM ""Accounts"" AS a2
  LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
  WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
 )
 AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
 AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
 GROUP BY t.""Date"", a.""Id""
 ORDER BY t.""Date"";
END; $$
LANGUAGE PLPGSQL;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION get_transfers_summary_by_month(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);

DROP FUNCTION get_transfers_summary_by_week(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);

DROP FUNCTION get_transfers_summary_by_day(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_week(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 sq.""AccountId"", sq.""Amount"", to_date(sq.""YearWeek"", 'IYYY-IW')::timestamp AS ""StartDate""
 FROM (
  SELECT a.""Id"" AS ""AccountId"", SUM(t.""Amount"") AS ""Amount"", to_char(t.""Date"", 'IYYY-IW') AS ""YearWeek"" FROM ""Transfer"" AS t
  INNER JOIN ""AccountDetails"" AS acs ON t.""AccountDetailsId"" = acs.""Id""
  INNER JOIN ""Accounts"" AS a ON acs.""AccountId"" = a.""Id""
  WHERE a.""Id"" IN (
   SELECT a2.""Id"" FROM ""Accounts"" AS a2
   LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
   WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
   OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  )
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY ""YearWeek"", a.""Id""
  ORDER BY ""YearWeek""
 ) AS sq;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_month(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 sq.""AccountId"", sq.""Amount"", to_date(sq.""YearMonth"", 'YYYY-MM')::timestamp AS ""StartDate""
 FROM (
  SELECT a.""Id"" AS ""AccountId"", SUM(t.""Amount"") AS ""Amount"", to_char(t.""Date"", 'YYYY-MM') AS ""YearMonth"" FROM ""Transfer"" AS t
  INNER JOIN ""AccountDetails"" AS acs ON t.""AccountDetailsId"" = acs.""Id""
  INNER JOIN ""Accounts"" AS a ON acs.""AccountId"" = a.""Id""
  WHERE a.""Id"" IN (
   SELECT a2.""Id"" FROM ""Accounts"" AS a2
   LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
   WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
   OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  )
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY ""YearMonth"", a.""Id""
  ORDER BY ""YearMonth""
 ) AS sq;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_day(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 a.""Id"" AS ""AccountId"", SUM(t.""Amount"") AS ""Amount"", t.""Date"" FROM ""Transfer"" AS t
 INNER JOIN ""AccountDetails"" AS acs ON t.""AccountDetailsId"" = acs.""Id""
 INNER JOIN ""Accounts"" AS a ON acs.""AccountId"" = a.""Id""
 WHERE a.""Id"" IN (
  SELECT a2.""Id"" FROM ""Accounts"" AS a2
  LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
  WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
 )
 AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
 AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
 GROUP BY t.""Date"", a.""Id""
 ORDER BY t.""Date"";
END; $$
LANGUAGE PLPGSQL;
            ");
        }
    }
}
