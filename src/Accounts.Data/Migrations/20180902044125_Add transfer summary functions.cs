﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Accounts.Data.Migrations
{
    public partial class Addtransfersummaryfunctions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_week(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 sq.""AccountId"", sq.""Amount"", to_date(sq.""YearWeek"", 'IYYY-IW')::timestamp AS ""StartDate""
 FROM (
  SELECT a.""Id"" AS ""AccountId"", SUM(t.""Amount"") AS ""Amount"", to_char(t.""Date"", 'IYYY-IW') AS ""YearWeek"" FROM ""Transfer"" AS t
  INNER JOIN ""AccountDetails"" AS acs ON t.""AccountDetailsId"" = acs.""Id""
  INNER JOIN ""Accounts"" AS a ON acs.""AccountId"" = a.""Id""
  WHERE a.""Id"" IN (
   SELECT a2.""Id"" FROM ""Accounts"" AS a2
   LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
   WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
   OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  )
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY ""YearWeek"", a.""Id""
  ORDER BY ""YearWeek""
 ) AS sq;
END; $$
LANGUAGE PLPGSQL;
            ");

            migrationBuilder.Sql(@"
CREATE OR REPLACE FUNCTION get_transfers_summary_by_month(
 user_id text,
 account_id bigint,
 min_datetime timestamp,
 max_datetime timestamp)
 RETURNS TABLE (
 ""AccountId"" bigint,
 ""Amount"" numeric,
 ""StartDate"" timestamp
 )
AS $$
BEGIN
 RETURN QUERY SELECT
 sq.""AccountId"", sq.""Amount"", to_date(sq.""YearMonth"", 'YYYY-MM')::timestamp AS ""StartDate""
 FROM (
  SELECT a.""Id"" AS ""AccountId"", SUM(t.""Amount"") AS ""Amount"", to_char(t.""Date"", 'YYYY-MM') AS ""YearMonth"" FROM ""Transfer"" AS t
  INNER JOIN ""AccountDetails"" AS acs ON t.""AccountDetailsId"" = acs.""Id""
  INNER JOIN ""Accounts"" AS a ON acs.""AccountId"" = a.""Id""
  WHERE a.""Id"" IN (
   SELECT a2.""Id"" FROM ""Accounts"" AS a2
   LEFT JOIN ""AccountShares"" sh ON sh.""AccountId"" = a2.""Id""
   WHERE (account_id IS NOT NULL AND a2.""Id"" = account_id AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
   OR (account_id IS NULL AND (a2.""UserId"" = user_id OR sh.""SharedUserId"" = user_id))
  )
  AND (min_datetime IS NULL OR t.""Date"" >= min_datetime)
  AND (max_datetime IS NULL OR t.""Date"" <= max_datetime)
  GROUP BY ""YearMonth"", a.""Id""
  ORDER BY ""YearMonth""
 ) AS sq;
END; $$
LANGUAGE PLPGSQL;
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
DROP FUNCTION get_transfers_summary_by_month(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);

DROP FUNCTION get_transfers_summary_by_week(user_id text, account_id bigint, min_datetime timestamp, max_datetime timestamp);
            ");
        }
    }
}
