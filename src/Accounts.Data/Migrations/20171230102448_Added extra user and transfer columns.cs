﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Accounts.Data.Migrations
{
    public partial class Addedextrauserandtransfercolumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "AutoTransfer",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "OriginalAmount",
                table: "AutoTransfer",
                type: "float8",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OriginalCurrency",
                table: "AutoTransfer",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TimeZoneId",
                table: "AspNetUsers",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "AutoTransfer");

            migrationBuilder.DropColumn(
                name: "OriginalAmount",
                table: "AutoTransfer");

            migrationBuilder.DropColumn(
                name: "OriginalCurrency",
                table: "AutoTransfer");

            migrationBuilder.DropColumn(
                name: "TimeZoneId",
                table: "AspNetUsers");
        }
    }
}
