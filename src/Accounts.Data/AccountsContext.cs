using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Accounts.Data.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Accounts.Data
{
    public class AccountsContext : IdentityDbContext<User, IdentityRole, string>
    {
        private readonly IHttpContextAccessor context;

        public AccountsContext(DbContextOptions options, IHttpContextAccessor context, IHostingEnvironment env)
            : base(options)
        {
            this.context = context;
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountShare> AccountShares { get; set; }
        public virtual DbSet<AutoTransfer> AutoTransfer { get; set; }
        public virtual DbSet<Period> Period { get; set; }
        public virtual DbSet<Transfer> Transfer { get; set; }
        public virtual DbSet<Token> Tokens { get; set; }
        public virtual DbSet<TokenType> TokenTypes { get; set; }

        public DbQuery<Void> VoidQuery { get; set; }

        public DbQuery<TransfersSummary> TransferSummaries { get; set; }

        public DbQuery<TransferCreate> TransferCreate { get; set; }

        public override int SaveChanges()
        {
            // fix trackable entities
            this.AddTimestamps();

            return base.SaveChanges();
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            this.AddTimestamps();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            this.AddTimestamps();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            this.AddTimestamps();
            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        private void AddTimestamps()
        {
            var trackables = this.ChangeTracker.Entries<IEntity>();

            var name = this.context?.HttpContext?.User?.Identity?.Name;

            if (trackables != null)
            {
                // added
                foreach (var item in trackables.Where(t => t.State == EntityState.Added))
                {
                    item.Entity.CreatedOn = System.DateTime.UtcNow;
                    item.Entity.CreatedBy = name;
                    item.Entity.ModifiedOn = System.DateTime.UtcNow;
                    item.Entity.ModifiedBy = name;
                }

                // modified
                foreach (var item in trackables.Where(t => t.State == EntityState.Modified))
                {
                    item.Entity.ModifiedOn = System.DateTime.UtcNow;
                    item.Entity.ModifiedBy = name;
                }
            }
        }
    }
}