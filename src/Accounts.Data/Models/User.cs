using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Accounts.Data.Models
{
    public class User : IdentityUser
    {
        [Required]
        [MaxLength(25)]
        public string DisplayName { get; set; }

        public string CommonCurrencies { get; set; }

        public string TimeZoneId { get; set; }
    }
}