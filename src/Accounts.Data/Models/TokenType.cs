using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounts.Data.Models
{
    public class TokenType : IReferenceEntity
    {
        public string Description { get; set; }
        public string Name { get; set; }
        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}