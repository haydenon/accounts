using System;

namespace Accounts.Data.Models
{
    public class AutoTransfer : IEntity
    {
        public Account Account { get; set; }
        public long AccountId { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public DateTime StartDate { get; set; }
        public Period Period { get; set; }
        public long PeriodId { get; set; }
        public int PeriodsPerOccurence { get; set; }
        public DateTime? LastTransfer { get; set; }

        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}