using System;

namespace Accounts.Data.Models
{
    public class Token : IEntity
    {
        public string TokenText { get; set; }
        public bool Used { get; set; }
        public string UserId { get; set; }
        public TokenType TokenType { get; set; }
        public long TokenTypeId { get; set; }

        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }

}