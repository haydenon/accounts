using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounts.Data.Models
{
    public class Account : IUserOwned
    {
        [Required]
        [MinLength(1)]
        [MaxLength(60)]
        public string Name { get; set; }

        public ICollection<AccountShare> AccountShares { get; set; }

        public ICollection<Transfer> Transfers { get; set; }

        [Required]
        [MaxLength(3)]
        public string Currency { get; set; }

        public User User { get; set; }
        public string UserId { get; set; }

        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}