using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounts.Data.Models
{
    public class TransfersSummary
    {
        public long AccountId { get; set; }

        public DateTime StartDate { get; set; }

        public decimal Amount { get; set; }

        public decimal StartBalance { get; set; }
    }
}