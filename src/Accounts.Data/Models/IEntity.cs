using System;

namespace Accounts.Data.Models
{
    public interface IEntity
    {
        long Id { get; set; }
        DateTime CreatedOn { get; set; }
        string CreatedBy { get; set; }
        DateTime ModifiedOn { get; set; }
        string ModifiedBy { get; set; }
    }
}