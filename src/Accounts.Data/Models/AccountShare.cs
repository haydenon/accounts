using System;
using System.ComponentModel.DataAnnotations;

namespace Accounts.Data.Models
{
    public class AccountShare : IUserOwned
    {
        public User User { get; set; }
        public string UserId { get; set; }

        public User SharedUser { get; set; }
        public string SharedUserId { get; set; }
        [EmailAddress]
        public string SharedEmail { get; set; }

        public Account Account { get; set; }
        public long AccountId { get; set; }

        public bool Readonly { get; set; }

        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}