namespace Accounts.Data.Models
{
    public interface IReferenceEntity : IEntity
    {
        string Description { get; set; }
        string Name { get; set; }
    }
}