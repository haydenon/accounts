using System;

namespace Accounts.Data.Models
{
    public class TransferCreate
    {
        public long TransferId { get; set; }

        public decimal AccountBalance { get; set; }

    }
}