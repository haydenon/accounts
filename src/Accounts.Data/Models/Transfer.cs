using System;
using System.ComponentModel.DataAnnotations;

namespace Accounts.Data.Models
{
    public class Transfer : IEntity
    {
        public Account Account { get; set; }
        public long AccountId { get; set; }
        public decimal Amount { get; set; }

        [MaxLength(3)]
        public string Currency { get; set; }
        public decimal DisplayAmount { get; set; }
        public decimal? OriginalAmount { get; set; }

        [MaxLength(3)]
        public string OriginalCurrency { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public bool AutoTransfer { get; set; }
        public decimal AccountBalance { get; set; }

        public long Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string ModifiedBy { get; set; }
    }
}