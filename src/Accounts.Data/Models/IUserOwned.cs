namespace Accounts.Data.Models
{
    public interface IUserOwned : IEntity
    {
        User User { get; set; }
        string UserId { get; set; }
    }
}